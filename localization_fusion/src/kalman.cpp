
#include "localization_fusion/kalman.h"
#define PI 3.14159265

using Eigen::MatrixXf;
using Eigen::VectorXf;

// Please note that the Eigen library does not initialize 
// VectorXf or MatrixXf objects with zeros upon creation.

KalmanFilter::KalmanFilter() {}

KalmanFilter::~KalmanFilter() {}

void KalmanFilter::Init(VectorXf &x_in, MatrixXf &P_in, MatrixXf &F_in,
                        MatrixXf &H_in, MatrixXf &R_in, MatrixXf &Q_in) {
  x_ = x_in;
  P_ = P_in;
  F_ = F_in;
  H_ = H_in;
  R_ = R_in;
  Q_ = Q_in;
}

void KalmanFilter::Predict() {
  /**
  TODO:
    * predict the state
  */
  x_ = F_ * x_;
  MatrixXf Ft = F_.transpose();
  P_ = F_ * P_ * Ft + Q_;

}

void KalmanFilter::Update(const VectorXf &z) {
  /**
  TODO:
    * update the state by using Kalman Filter equations
  */
  VectorXf y = z - H_ * x_;
  MatrixXf Ht = H_.transpose();
  MatrixXf S = H_ * P_ * Ht + R_;
  MatrixXf Si = S.inverse();
  MatrixXf K = P_ * Ht * Si;

  // New state
  x_ = x_ + ( K*y );
  long x_size = x_.size();
  MatrixXf I_ = MatrixXf::Identity(x_size, x_size);
  P_ = ( I_ - K*H_ )*P_;
}

