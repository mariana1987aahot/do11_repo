#!/usr/bin/env python
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
from numpy.linalg import inv
import cv2
import rospy 

from sensor_msgs.msg import Image,LaserScan
from geometry_msgs.msg import PoseStamped
import sys

class localization_fused:

    def __init__(self):
        self.msg = PoseStamped()
        self.scan_sub=rospy.Subscriber('/ndt_pose',PoseStamped,self.ndt_callback)
        self.spInv=np.matrix([[1, 0, 1, 0],[0, 1, 0, 1],[0, 0, 1, 0],[0, 0, 0, 1]])
        self.spInvt=np.transpose(self.spInv)
        self.St=np.matrix([[1,0,0.2,0],[0,1,0,0.2],[0,0,1,0],[0,0,0,1]])
        self.Q=np.matrix([[0,0,0,0],[0,0,0,0],[0,0,0.001,0],[0,0,0,0.001]]) #process noise
        self.M=np.matrix([[1,0,0,0],[0,1,0,0]])
        self.Mt=np.transpose(self.M)
        self.R=np.matrix([[0.9,0],[0,0.9]]) #measurement noise-default 0.1 diagonal 
        self.X=np.matrix([[14],[14],[0],[0]])
        self.x_filt = 0
        self.y_filt = 0
        ## initialize the publisher
        #self.pub=rospy.Publisher('/obstacle_pos',Point,queue_size=1)
        #self.pub1=rospy.Publisher('/obstacle_pos_ini',Point,queue_size=1)
        
        
    def visualize(x,y): ##Function to publish topic that goes to tf broadcaster
        br = tf.TransformBroadcaster()
        br.sendTransform((self.loc_x, self.loc_y, self.loc_z),
                     (0, 0, 0, 1),
                     rospy.Time.now(),
                     "kalman",
                     "world")


    def kalman(self, x,y):
        dT=1    
        Y=np.matrix([[0],[0]])
        I=np.identity(4)
        #x_filt=np.zeros(len(x))
        #y_filt=np.zeros(len(y))
        #Filtering equations
        self.X=self.spInv*self.X # state transition
        self.St=self.spInv*self.St*self.spInvt + self.Q # State covariance prediction
        self.K=self.St*self.Mt*inv(self.M*self.St*self.Mt + self.R)# Kalman gain
        Y=np.matrix([[x],[y]])# measurement step
        self.X=self.X + self.K*(Y - self.M*self.X)# update states based on kalman gain
        self.x_filt=self.X[0] 
        self.y_filt=self.X[1]
        St=(I - K*M)*St# update the state covariances
        return self.x_filt,self.y_filt
        
    def ndt_callback(self,ndt_data):
        #rospy.loginfo("Receiving Information")# Test the info inflow
        self.msg=ndt_data # initialize msg as the scan data
        x = self.msg.pose.position.x
        y = self.msg.pose.position.y
        self.x_filt, self.y_filt = self.kalman(x,y)
        visualize(self.x_filt, self.y_filt)

if __name__ == '__main__':
    rospy.init_node('fused_pose')
    C = localization_fused()  
    r = rospy.Rate(40)
    if rospy.is_shutdown():
        self.f.close()
    while not rospy.is_shutdown():
        r.sleep()
