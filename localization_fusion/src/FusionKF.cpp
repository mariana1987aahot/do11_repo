#include "localization_fusion/FusionKF.h"
#include "Eigen/Dense"
#include <iostream>
#include <tf/transform_listener.h>
#include "tf2_msgs/TFMessage.h"


using namespace std;
using Eigen::MatrixXf;
using Eigen::VectorXf;
using std::vector;

/*
 * Constructor.
 */
FusionKF::FusionKF(): nh_("") {
  is_initialized_ = false;

  // initializing matrices
  R_laser_ = MatrixXf(2, 2);
  H_laser_ = MatrixXf(2, 4);
  Hj_ = MatrixXf(3, 4);

  //measurement covariance matrix - laser
  R_laser_ << 0.0225, 0,
        0, 0.0225;
        
  /**
    * Finish initializing the FusionKF.
    * Set the process and measurement noises
  */

  //measurement matrix
  H_laser_ << 1, 0, 0, 0,
              0, 1, 0, 0;

  //the initial transition matrix F_ (state transition)
  kf_.F_ = MatrixXf(4, 4);
  kf_.F_ << 1, 0, 1, 0,
        0, 1, 0, 1,
        0, 0, 1, 0,
        0, 0, 0, 1;

  kf_.P_ = MatrixXf(4, 4);
  kf_.P_ << 1, 0, 0, 0,
               0, 1, 0, 0,
               0, 0, 1000, 0,
               0, 0, 0, 1000;

  filtered_pub_ =  nh_.advertise<geometry_msgs::PoseStamped>("/kalman_pose", 1);
  uwb_test_pub_ = nh_.advertise<std_msgs::Bool>("/testuwbcheck",1);
  kalman_stat_pub_ = nh_.advertise<std_msgs::Bool>("/kalman_pose_stat",1);
//  ndt_sub_ =  nh_.subscribe("ndt_pose", 1000, &FusionKF::ndtCallback,this);
  ndt_sub_ =  nh_.subscribe("ndt_pose_do11", 1000, &FusionKF::ndtCallback,this);
  ndt_check_sub_ =  nh_.subscribe("ndt_stat", 1000, &FusionKF::ndtCheckCallback, this);
  uwb_sub_ =  nh_.subscribe("uwb/pose", 1000, &FusionKF::uwbCallback, this);
  uwb_check_sub_ =  nh_.subscribe("uwb/fix", 1000, &FusionKF::uwbCheckCallback, this);
  gnss_sub_ =  nh_.subscribe("gps/rtkfix", 1000, &FusionKF::gnssCallback, this);
  gnss_check_sub_ =  nh_.subscribe("gps/fix", 1000, &FusionKF::gnssCheckCallback, this);
  
  gazebo_vehicle_sub_ = nh_.subscribe("gazebo_vehicle/pose", 1000, &FusionKF::gazeboCallback, this);
  initial_gazebo_pose_pub_ =  nh_.advertise<geometry_msgs::PoseWithCovarianceStamped>("initialpose", 1);
  
  
  ins_sub_ = nh_.subscribe("ins", 1000, &FusionKF::insCallback, this);
  

  initial_pose_ = nh_.subscribe("initialpose", 1000, &FusionKF::initialPoseCallback, this);
  timer1 = nh_.createTimer(ros::Duration(0.2), &FusionKF::checkNumberofPoses, this);
}

/**
* Destructor.
*/
FusionKF::~FusionKF() {}


void FusionKF::checkNumberofPoses(const ros::TimerEvent&)
{
  use_ndt = 0;
  int use_uwb = 0;
  int use_gnss = 0;
  use_gazebo = 0;
  Eigen::MatrixXf pose_trial, angle_matrix, pose_result;
  tf::TransformListener listener_;  
  geometry_msgs::Pose ndt_final;  
  geometry_msgs::Pose uwb_final;
  geometry_msgs::Pose gnss_final;
  
  std_msgs::Bool pose_available;
  pose_available.data = false;
  
  if(current_gazebo_pose_ != nullptr)
  {
    use_gazebo = 1;
    pose_available.data = true;
    px = current_gazebo_pose_->pose.position.x;
    py = current_gazebo_pose_->pose.position.y;
    raw_measurements_ << px, py;
    ProcessMeasurement();
    ROS_INFO("Using gazebo_vehicle/pose");
    kalman_stat_pub_.publish(pose_available);
  }
  else{
    if((current_ndt_pose_ && current_ndt_stat_) || (current_uwb_pose_ && current_uwb_stat_) || (current_gnss_pose_ && current_gnss_stat_))
    {
      if(current_ndt_pose_ && current_ndt_stat_)
      {
        if(current_ndt_stat_->score <= 15.0)
        {
          use_ndt = 1;
          pose_available.data = true;
          tf::Transform ndt_to_world;
          tf::poseMsgToTF(current_ndt_pose_->pose, ndt_to_world);
          tf::StampedTransform req_to_ndt;
          ros::Time now = ros::Time(0);
          listener_.waitForTransform("/world", now,"/map", now,"/world", ros::Duration(1));
          listener_.lookupTransform("/world", now,"/map", now,"/world", req_to_ndt); 
          tf::Transform req_to_world;
          req_to_world = req_to_ndt * ndt_to_world;
          tf::poseTFToMsg(req_to_world, ndt_final);
          
          px = ndt_final.position.x; 
          py = ndt_final.position.y;
          raw_measurements_ << px, py;
          ndt_orientation_ = current_ndt_pose_->pose.orientation;
          ProcessMeasurement();
        }else
          use_ndt = 0;
      }
      
      if(current_uwb_pose_ && current_uwb_stat_)
      {
        if(current_uwb_stat_->data)
        {
          use_uwb = 1; 
          pose_available.data = true;
          tf::Transform uwb_to_world;
          tf::poseMsgToTF(current_uwb_pose_->pose, uwb_to_world);
          tf::StampedTransform req_to_uwb;
          ros::Time now = ros::Time(0);
          listener_.waitForTransform("/world", now,"/uwb_frame", now,"/world", ros::Duration(1));
          listener_.lookupTransform("/world", now,"/uwb_frame", now,"/world", req_to_uwb); 
          tf::Transform req_to_world;
          req_to_world = req_to_uwb * uwb_to_world;
          tf::poseTFToMsg(req_to_world, uwb_final);
          
          px = uwb_final.position.x; 
          py = uwb_final.position.y;
          raw_measurements_ << px, py;
          ProcessMeasurement();
        }else
          use_uwb = 0;
      }
      
      if((current_gnss_pose_ != nullptr) && (current_gnss_stat_ != nullptr))
      {
        if(current_gnss_stat_->status.status == 0)
        {
          use_gnss = 1;
          pose_available.data = true;
          tf::Transform gnss_to_world;
          tf::poseMsgToTF(current_gnss_pose_->pose.pose, gnss_to_world);
          tf::StampedTransform req_to_gnss;
          ros::Time now = ros::Time(0);
          listener_.waitForTransform("/world", now,"/gps", now,"/world", ros::Duration(1));
          listener_.lookupTransform("/world", now,"/gps", now,"/world", req_to_gnss);
          tf::Transform req_to_world;
          req_to_world = req_to_gnss * gnss_to_world;
          tf::poseTFToMsg(req_to_world, gnss_final);

          px = gnss_final.position.x; 
          py = gnss_final.position.y;
          raw_measurements_ << px, py;
          ProcessMeasurement();
          current_gnss_pose_ = nullptr;
          current_gnss_stat_ = nullptr;
          
        }else
          use_gnss = 0;
      }
      ROS_INFO("NDT = [%d], UWB =  [%d], GNSS =  [%d]", use_ndt, use_uwb, use_gnss);
    }else
      ROS_INFO("No Reliable Poses");
      
    kalman_stat_pub_.publish(pose_available);
  }
}

std::vector<double> FusionKF::getOrientationOffset(geometry_msgs::Quaternion orig, geometry_msgs::Quaternion ref)
{
//  for original orientation of ins
  tf::Quaternion current_orientation_orig(orig.x, orig.y, orig.z, orig.w);
  tf::Matrix3x3 m_orig(current_orientation_orig);
  double roll_orig, pitch_orig, yaw_orig;
  m_orig.getRPY(roll_orig, pitch_orig, yaw_orig);

//  for reference orientation of initial pose in rivz
  tf::Quaternion current_orientation_ref(ref.x, ref.y, ref.z, ref.w);
  tf::Matrix3x3 m_ref(current_orientation_ref);
  double roll_ref, pitch_ref, yaw_ref;
  m_ref.getRPY(roll_ref, pitch_ref, yaw_ref);


  return std::vector<double>{roll_ref-roll_orig, pitch_ref-pitch_orig, yaw_ref-yaw_orig};
}


geometry_msgs::Quaternion FusionKF::applyOrientationOffset(geometry_msgs::Quaternion orig)
{
  std::cout << "inside function" << std::endl;
  tf2::Quaternion q_orig, q_rot, q_new;
  std::cout << "still inside function" << std::endl;
  // Get the original orientation of 'commanded_pose'
  tf2::convert(orig , q_orig);
  std::cout << "Roll = " << orientationOffset[0] << std::endl;
  double roll=orientationOffset[0], pitch=orientationOffset[1], yaw=orientationOffset[2];  // Rotate the previous pose by 180* about X
  q_rot.setRPY(roll, pitch, yaw);

  q_new = q_rot*q_orig;  // Calculate the new orientation
  q_new.normalize();

  // Stuff the new rotation back into the pose. This requires conversion into a msg type
  tf2::convert(q_new, orig);
  std::cout << "end of orienatation" << std::endl;
  return orig;
}


void FusionKF::ProcessMeasurement() {


  /*****************************************************************************
   *  Initialization
   ****************************************************************************/
  if (!is_initialized_) {
    /**
    TODO:
      * Initialize the state kf_.x_ with the first measurement.
      * Create the covariance matrix.
    */
    // first measurement
    cout << "kf: " << endl;
    kf_.x_ = VectorXf(4);
    kf_.x_ << 1, 1, 1, 1;
    // done initializing, no need to predict or update
    is_initialized_ = true;
    return;
  }

  /*****************************************************************************
   *  Prediction
   ****************************************************************************/

  /**
     * Update the state transition matrix F according to the new elapsed time.
      - Time is measured in seconds.
     * Update the process noise covariance matrix.
   */
  
  float dt = 0.1;
  float dt_2 = dt * dt;
  float dt_3 = dt_2 * dt;
  float dt_4 = dt_3 * dt;
  
  //Modify the F matrix so that the time is integrated
  kf_.F_(0, 2) = dt;
  kf_.F_(1, 3) = dt;
    
  //set the process covariance matrix Q
  kf_.Q_ = MatrixXf(4, 4);
  kf_.Q_ << dt_4/4, 0, dt_3/2, 0,
            0, dt_4/4, 0, dt_3/2,
            dt_3/2, 0, dt_2, 0,
            0, dt_3/2, 0, dt_2;   

  kf_.Predict();

  /*****************************************************************************
   *  Update
   ****************************************************************************/

  /**
     * Use the sensor type to perform the update step.
     * Update the state and covariance matrices.
   */
  
  kf_.H_ = H_laser_;
  kf_.R_ = R_laser_;
  kf_.Update(raw_measurements_);

  // print the output
  cout << "x_ = " << kf_.x_ << endl;
  cout << "P_ = " << kf_.P_ << endl;
  
  geometry_msgs::PoseStamped kalman_pose;
  kalman_pose.header.frame_id = "world";
  kalman_pose.pose.position.x = kf_.x_(0);
  kalman_pose.pose.position.y = kf_.x_(1);
  kalman_pose.pose.position.z = 0.0;

    //will subsribe to ins data here
  if(current_ins_pose_ != nullptr)
  {
    kalman_pose.pose.orientation = applyOrientationOffset(ins_orientation_);
    filtered_pub_.publish(kalman_pose);
    current_ins_pose_ = nullptr;
  }
  else if(use_ndt == 1)
  {
    kalman_pose.pose.orientation = ndt_orientation_;
    filtered_pub_.publish(kalman_pose);
  }else if(use_gazebo == 1)
  {
    kalman_pose.pose.orientation = current_gazebo_pose_->pose.orientation;
    filtered_pub_.publish(kalman_pose);
    
    geometry_msgs::PoseWithCovarianceStamped initial_pose;
    initial_pose.header.frame_id = current_gazebo_pose_->header.frame_id;
    initial_pose.pose.pose = current_gazebo_pose_->pose;
    initial_gazebo_pose_pub_.publish(initial_pose);
  }
}

void FusionKF::gazeboCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  current_gazebo_pose_ = std::make_shared<geometry_msgs::PoseStamped>(*msg);
}
void FusionKF::ndtCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  current_ndt_pose_ = std::make_shared<geometry_msgs::PoseStamped>(*msg);
}
void FusionKF::ndtCheckCallback(const autoware_msgs::NDTStat::ConstPtr& msg)
{
  current_ndt_stat_ = std::make_shared<autoware_msgs::NDTStat>(*msg);
}
void FusionKF::uwbCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  current_uwb_pose_ = std::make_shared<geometry_msgs::PoseStamped>(*msg);
}
void FusionKF::uwbCheckCallback(const std_msgs::Bool::ConstPtr& msg)
{
  current_uwb_stat_ = std::make_shared<std_msgs::Bool>(*msg);
}
void FusionKF::gnssCallback(const nav_msgs::Odometry::ConstPtr& msg)
{  
  current_gnss_pose_ = std::make_shared <nav_msgs::Odometry>(*msg);   
}
void FusionKF::gnssCheckCallback(const sensor_msgs::NavSatFix::ConstPtr& msg)
{
  current_gnss_stat_ = std::make_shared<sensor_msgs::NavSatFix>(*msg);
}
void FusionKF::insCallback(const nav_msgs::Odometry::ConstPtr& msg)
{  
  current_ins_pose_ = std::make_shared <nav_msgs::Odometry>(*msg);
  ins_orientation_ = current_ins_pose_->pose.pose.orientation;
}
void FusionKF::initialPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
  current_initial_pose_ = std::make_shared<geometry_msgs::PoseWithCovarianceStamped>(*msg);
  std::cout << "inside callback" << std::endl;
  orientationOffset = getOrientationOffset(ins_orientation_, current_initial_pose_->pose.pose.orientation);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "kf_localizer");
  FusionKF obj;
  //Using AsyncSpinner for asyncronous threading
  ros::AsyncSpinner spinner(0);
  spinner.start();
  ros::waitForShutdown();
  return 0;
};
