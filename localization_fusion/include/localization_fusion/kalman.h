#ifndef KALMAN_FILTER_H_
#define KALMAN_FILTER_H_
//#define PI 3.14159265
#include "Eigen/Dense"

class KalmanFilter {
public:

  // state vector
  Eigen::VectorXf x_;

  // state covariance matrix
  Eigen::MatrixXf P_;

  // state transition matrix
  Eigen::MatrixXf F_;

  // process covariance matrix
  Eigen::MatrixXf Q_;

  // measurement matrix
  Eigen::MatrixXf H_;

  // measurement covariance matrix
  Eigen::MatrixXf R_;



  /**
   * Constructor
   */
  KalmanFilter();

  /**
   * Destructor
   */
  virtual ~KalmanFilter();

  /**
   * Init Initializes Kalman filter
   * @param x_in Initial state
   * @param P_in Initial state covariance
   * @param F_in Transition matrix
   * @param H_in Measurement matrix
   * @param R_in Measurement covariance matrix
   * @param Q_in Process covariance matrix
   */
  void Init(Eigen::VectorXf &x_in, Eigen::MatrixXf &P_in, Eigen::MatrixXf &F_in,
      Eigen::MatrixXf &H_in, Eigen::MatrixXf &R_in, Eigen::MatrixXf &Q_in);

  /**
   * Prediction Predicts the state and the state covariance
   * using the process model
   * @param delta_T Time between k and k+1 in s
   */
  void Predict();

  /**
   * Updates the state by using standard Kalman Filter equations
   * @param z The measurement at k+1
   */
  void Update(const Eigen::VectorXf &z);

};

#endif /* KALMAN_FILTER_H_ */
