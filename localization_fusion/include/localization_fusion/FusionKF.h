#ifndef FusionKF_H_
#define FusionKF_H_

#include "Eigen/Dense"
#include <vector>
#include <string>
#include <fstream>
#include "localization_fusion/kalman.h"

#include <iostream>
#include <chrono>

#include <ros/ros.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Quaternion.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include <autoware_msgs/NDTStat.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2/utils.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/NavSatFix.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>

class FusionKF {
public:
  /**
  * Constructor.
  */
  FusionKF();

  /**
  * Destructor.
  */
  virtual ~FusionKF();

  /**
  * Run the whole flow of the Kalman Filter from here.
  */
  void ProcessMeasurement();

  /**
  * Kalman Filter update and prediction math lives in here.
  */
  KalmanFilter kf_;
  
  Eigen::Vector2f raw_measurements_;
  

  

private:
  // check whether the tracking toolbox was initialized or not (first measurement)
  bool is_initialized_;

  Eigen::MatrixXf R_laser_;
  Eigen::MatrixXf H_laser_;
  Eigen::MatrixXf Hj_;

  float noise_ax;
  float noise_ay;
  
  float px;
  float py;
  
  int use_ndt;
  int use_gazebo;
  
  geometry_msgs::Quaternion ndt_orientation_;
  geometry_msgs::Quaternion ins_orientation_;

  std::vector<double> orientationOffset;
  
  ros::Timer timer1;
  
  ros::NodeHandle nh_;
  ros::Publisher filtered_pub_;
  ros::Publisher uwb_test_pub_;
  ros::Publisher kalman_stat_pub_;
  ros::Subscriber ndt_sub_;
  ros::Subscriber ndt_check_sub_;
  ros::Subscriber uwb_sub_;
  ros::Subscriber uwb_check_sub_;
  ros::Subscriber gnss_sub_;
  ros::Subscriber gnss_check_sub_;
  ros::Subscriber ins_sub_;
  ros::Subscriber initial_pose_;
  
  //  For Simulation use only: Gazebo
  ros::Subscriber gazebo_vehicle_sub_;
  ros::Publisher initial_gazebo_pose_pub_;
  
    /* for model prediction */
  std::shared_ptr <geometry_msgs::PoseStamped> current_ndt_pose_;
  std::shared_ptr <autoware_msgs::NDTStat> current_ndt_stat_;
  std::shared_ptr <geometry_msgs::PoseStamped> current_uwb_pose_;
  std::shared_ptr <std_msgs::Bool> current_uwb_stat_;
  std::shared_ptr <nav_msgs::Odometry> current_gnss_pose_;
  std::shared_ptr <sensor_msgs::NavSatFix> current_gnss_stat_;
  std::shared_ptr <nav_msgs::Odometry> current_ins_pose_;
  std::shared_ptr <geometry_msgs::PoseWithCovarianceStamped> current_initial_pose_;
  
  //  For Simulation use only: Gazebo
  std::shared_ptr <geometry_msgs::PoseStamped> current_gazebo_pose_;
  
    /** KalmanFilter callbacks**/
  void ndtCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
  void ndtCheckCallback(const autoware_msgs::NDTStat::ConstPtr& msg);
  void uwbCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
  void uwbCheckCallback(const std_msgs::Bool::ConstPtr& msg);
  void gnssCallback(const nav_msgs::Odometry::ConstPtr& msg);
  void gnssCheckCallback(const sensor_msgs::NavSatFix::ConstPtr& msg);
  void insCallback(const nav_msgs::Odometry::ConstPtr& msg);
  void initialPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
  
  //  For Simulation use only: Gazebo
  void gazeboCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
  
  
  
  void checkNumberofPoses(const ros::TimerEvent&);
  geometry_msgs::Quaternion applyOrientationOffset(geometry_msgs::Quaternion orig);
  std::vector<double> getOrientationOffset (geometry_msgs::Quaternion orig, geometry_msgs::Quaternion ref);
};

#endif /* FusionKF_H_ */
