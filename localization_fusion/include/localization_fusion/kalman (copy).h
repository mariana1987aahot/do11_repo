
#include <iostream>
#include <vector>
#include <chrono>

#include <ros/ros.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include <autoware_msgs/NDTStat.h>
#include <tf2/utils.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/NavSatFix.h>

#include <Eigen/Dense>


//#include "amathutils_lib/kalman_filter.hpp"
//#include "amathutils_lib/time_delay_kalman_filter.hpp"

class KalmanFilter
{
  public:
    KalmanFilter();
    ~KalmanFilter();
    Eigen::Matrix4f St;
    Eigen::Vector4f X;
    Eigen::MatrixXf K, Ktemp;
    float x_filt,y_filt;
    int use_ndt;
    int use_uwb;
    int use_gnss;

  private:
    ros::NodeHandle nh_;
    ros::Publisher filtered_pub_;
    ros::Publisher uwb_test_pub_;
    ros::Subscriber ndt_sub_;
    ros::Subscriber ndt_check_sub_;
    ros::Subscriber uwb_sub_;
    ros::Subscriber uwb_check_sub_;
    ros::Subscriber gnss_sub_;
    ros::Subscriber gnss_check_sub_;
      
    /* parameters for KF */
    double kf_dt_;
    bool uwbchecker;
    

  /* parameters */
//  bool show_debug_info_;
//  double ekf_rate_;                 //!< @brief  EKF predict rate
//  double ekf_dt_;                   //!< @brief  = 1 / ekf_rate_
//  double tf_rate_;                  //!< @brief  tf publish rate
//  bool enable_yaw_bias_estimation_; //!< @brief  for LiDAR mount error. if true, publish /estimate_yaw_bias
//  std::string pose_frame_id_;

//  int dim_x_;             //!< @brief  dimension of EKF state
//  int extend_state_step_; //!< @brief  for time delay compensation
//  int dim_x_ex_;          //!< @brief  dimension of extended EKF state (dim_x_ * extended_state_step)

//  /* Pose */
//  double pose_additional_delay_;         //!< @brief  compensated pose delay time = (pose.header.stamp - now) + additional_delay [s]
//  double pose_measure_uncertainty_time_; //!< @brief  added for measurement covariance
//  double pose_rate_;                     //!< @brief  pose rate [s], used for covariance calculation
//  double pose_gate_dist_;                //!< @brief  pose measurement is ignored if the maharanobis distance is larger than this value.
//  double pose_stddev_x_;                 //!< @brief  standard deviation for pose position x [m]
//  double pose_stddev_y_;                 //!< @brief  standard deviation for pose position y [m]
//  double pose_stddev_yaw_;               //!< @brief  standard deviation for pose position yaw [rad]
//  bool use_pose_with_covariance_;        //!< @brief  use covariance in pose_with_covarianve message

//  /* twist */
//  double twist_additional_delay_; //!< @brief  compensated delay time = (twist.header.stamp - now) + additional_delay [s]
//  double twist_rate_;             //!< @brief  rate [s], used for covariance calculation
//  double twist_gate_dist_;        //!< @brief  measurement is ignored if the maharanobis distance is larger than this value.
//  double twist_stddev_vx_;        //!< @brief  standard deviation for linear vx
//  double twist_stddev_wz_;        //!< @brief  standard deviation for angular wx

//  /* process noise variance for discrete model */
//  double proc_cov_yaw_d_;      //!< @brief  discrete yaw process noise
//  double proc_cov_yaw_bias_d_; //!< @brief  discrete yaw bias process noise
//  double proc_cov_vx_d_;       //!< @brief  discrete process noise in d_vx=0
//  double proc_cov_wz_d_;       //!< @brief  discrete process noise in d_wz=0

//  enum IDX
//  {
//    X = 0,
//    Y = 1,
//    YAW = 2,
//    YAWB = 3,
//    VX = 4,
//    WZ = 5,
//  };

  /* for model prediction */
  std::shared_ptr <geometry_msgs::PoseStamped> current_ndt_pose_;
  std::shared_ptr <autoware_msgs::NDTStat> current_ndt_stat_;
  std::shared_ptr <geometry_msgs::PoseStamped> current_uwb_pose_;
  std::shared_ptr <std_msgs::Bool> current_uwb_stat_;
  std::shared_ptr <nav_msgs::Odometry> current_gnss_pose_;
  std::shared_ptr <sensor_msgs::NavSatFix> current_gnss_stat_;

  /** KalmanFilter callbacks**/
  void ndtCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
  void ndtCheckCallback(const autoware_msgs::NDTStat::ConstPtr& msg);
  void uwbCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
  void uwbCheckCallback(const std_msgs::Bool::ConstPtr& msg);
  void gnssCallback(const nav_msgs::Odometry::ConstPtr& msg);
  void gnssCheckCallback(const sensor_msgs::NavSatFix::ConstPtr& msg);
  
  /**Function to check how many reliable poses from NDT, UWB, and GNSS can be used to update measurement step**/
  void checkNumberofPoses();
  
  
  /** Functions to update measurements with reliable data from different poses**/
  void measurementNDTUpdatePose();
  void measurementUWBUpdatePose(const geometry_msgs::PoseStamped &pose);
  void measurementGNSSUpdatePose(const nav_msgs::Odometry &pose);
  void kalmanUpdate(float x,float y);

//  /**
//   * @brief computes update & prediction of EKF for each ekf_dt_[s] time
//   */
//  void timerCallback(const ros::TimerEvent &e);

//  /**
//   * @brief publish tf for tf_rate [Hz]
//   */
//  void timerTFCallback(const ros::TimerEvent &e);

//   /**
//   * @brief set pose measurement
//   */ 
//  void callbackPose(const geometry_msgs::PoseStamped::ConstPtr &msg);

//   /**
//   * @brief set twist measurement
//   */ 
//  void callbackTwist(const geometry_msgs::TwistStamped::ConstPtr &msg);

//   /**
//   * @brief set poseWithCovariance measurement
//   */ 
//  void callbackPoseWithCovariance(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &msg);

//   /**
//   * @brief set initial_pose to current EKF pose
//   */ 
//  void callbackInitialPose(const geometry_msgs::PoseWithCovarianceStamped &msg);

//  /**
//   * @brief initialization of EKF
//   */
//  void initEKF();

//  /**
//   * @brief compute EKF prediction 
//   */
//  void predictKinematicsModel();

//  /**
//   * @brief compute EKF update with pose measurement
//   * @param pose measurement value
//   */
//  void measurementUpdatePose(const geometry_msgs::PoseStamped &pose);

//  /**
//   * @brief compute EKF update with pose measurement
//   * @param twist measurement value
//   */
//  void measurementUpdateTwist(const geometry_msgs::TwistStamped &twist);

//  /**
//   * @brief check whether a measurement value falls within the mahalanobis distance threshold
//   * @param dist_max mahalanobis distance threshold
//   * @param estimated current estimated state
//   * @param measured measured state
//   * @param estimated_cov current estimation covariance
//   * @return whether it falls within the mahalanobis distance threshold
//   */
//  bool mahalanobisGate(const double &dist_max, const Eigen::MatrixXd &estimated, const Eigen::MatrixXd &measured, const Eigen::MatrixXd &estimated_cov);

//  /**
//   * @brief get transform from frame_id
//   */
//  bool getTransformFromTF(std::string parent_frame, std::string child_frame, geometry_msgs::TransformStamped &transform);

//  /**
//   * @brief normalize yaw angle
//   * @param yaw yaw angle
//   * @return normalized yaw
//   */
//  double normalizeYaw(const double &yaw);

//  /**
//   * @brief set current EKF estimation result to current_ekf_pose_ & current_ekf_twist_
//   */
//  void setCurrentResult();

//  /**
//   * @brief get transform from frame_id
//   */
//  void publishEstimatedPose();

//  /**
//   * @brief for debug
//   */
//  void showCurrentX();

//  friend class EKFLocalizerTestSuite; // for test code

};
