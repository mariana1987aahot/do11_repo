#include "lidar_pcd_merge/lidar_pcd_merge.h"

int main(int argc, char **argv)
{
	ros::init(argc, argv, __APP_NAME__);

	ROSLidarPCDMergeApp app;

	app.Run();

	return 0;
}
