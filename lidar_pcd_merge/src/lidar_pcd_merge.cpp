#include "lidar_pcd_merge/lidar_pcd_merge.h"

// Receives all PCDs, creates ,erged PCD and publishes it
void ROSLidarPCDMergeApp::PointsCallback(const sensor_msgs::PointCloud2::ConstPtr& parent_front_cloud_msg,
										const sensor_msgs::PointCloud2::ConstPtr& child_left_cloud_msg,
										const sensor_msgs::PointCloud2::ConstPtr& child_right_cloud_msg)
{
	pcl::PointCloud<PointT>::Ptr parent_front_cloud(new pcl::PointCloud<PointT>);
	pcl::PointCloud<PointT>::Ptr child_left_cloud(new pcl::PointCloud<PointT>);
	pcl::PointCloud<PointT>::Ptr child_right_cloud(new pcl::PointCloud<PointT>);
	pcl::PointCloud<PointT>::Ptr out_cloud(new pcl::PointCloud<PointT>);

	front_tf_ = FrameTransform("base_link",
																  parent_front_cloud_msg->header.frame_id);

	left_front_tf_ = FrameTransform("base_link",
																  child_left_cloud_msg->header.frame_id);
	right_front_tf_ = FrameTransform("base_link",
																 	 child_right_cloud_msg->header.frame_id);

	pcl::fromROSMsg(*parent_front_cloud_msg, *parent_front_cloud);
	pcl::fromROSMsg(*child_left_cloud_msg, *child_left_cloud);
	pcl::fromROSMsg(*child_right_cloud_msg, *child_right_cloud);

	std::vector<PointT> front_transformed_cloud(child_right_cloud->points.size());
	std::vector<PointT> left_transformed_cloud(child_left_cloud->points.size());
	std::vector<PointT> right_transformed_cloud(child_right_cloud->points.size());

	for (size_t i = 0; i < child_left_cloud->points.size(); i++)
	{
		pcl::PointXYZ transformed_point_left;
		left_transformed_cloud[i] = PointTransform(child_left_cloud->points[i], left_front_tf_);
		transformed_point_left.x = left_transformed_cloud[i].x;
		transformed_point_left.y = left_transformed_cloud[i].y;
		transformed_point_left.z = left_transformed_cloud[i].z;
		out_cloud->points.push_back(transformed_point_left);
	}

	for (size_t j = 0; j < child_right_cloud->points.size(); j++)
	{
		pcl::PointXYZ transformed_point_right;
		right_transformed_cloud[j] = PointTransform(child_right_cloud->points[j], right_front_tf_);
		transformed_point_right.x = right_transformed_cloud[j].x;
		transformed_point_right.y = right_transformed_cloud[j].y;
		transformed_point_right.z = right_transformed_cloud[j].z;
		out_cloud->points.push_back(transformed_point_right);
	}

	for (size_t k = 0; k < parent_front_cloud->points.size(); k++)
	{
		pcl::PointXYZ transformed_point_front;
		front_transformed_cloud[k] = PointTransform(parent_front_cloud->points[k], front_tf_);
		transformed_point_front.x = front_transformed_cloud[k].x;
		transformed_point_front.y = front_transformed_cloud[k].y;
		transformed_point_front.z = front_transformed_cloud[k].z;
		out_cloud->points.push_back(transformed_point_front);
	}

	// Publish merged PCD
	sensor_msgs::PointCloud2 merged_cloud_msg;
	pcl::toROSMsg(*out_cloud, merged_cloud_msg);
	merged_cloud_msg.header = parent_front_cloud_msg->header;
	merged_cloud_msg.header.frame_id = "base_link";
	merged_lidars_.publish(merged_cloud_msg);

	out_cloud->points.clear();
}

// Finds tranform between two frames (if it exists) based on the tfs being published
tf::StampedTransform
ROSLidarPCDMergeApp::FrameTransform(const std::string &in_target_frame, const std::string &in_source_frame)
{
	tf::StampedTransform lidar_transform;
	try
	{
		transform_listener_->lookupTransform(in_target_frame, in_source_frame, ros::Time(0), lidar_transform);
		return lidar_transform;
	}
	catch (tf::TransformException ex)
	{
		ROS_ERROR("[%s] %s", __APP_NAME__, ex.what());
	}
	return lidar_transform;
}

// Receives a point from the different child lidar PCDs and tranforms them to parent frame
pcl::PointXYZ
ROSLidarPCDMergeApp::PointTransform(const PointT &in_point, const tf::StampedTransform &in_transform)
{
	tf::Vector3 tf_point(in_point.x, in_point.y, in_point.z);
	tf::Vector3 tf_point_t = in_transform * tf_point;
	return PointT(tf_point_t.x(), tf_point_t.y(), tf_point_t.z());
}

//Initializes subscribers (synchronizes them) and publishers.
void ROSLidarPCDMergeApp::InitializeROSIo(ros::NodeHandle &in_private_handle)
{
	//get params
	std::string left_lidar_str,right_lidar_str, front_lidar_str, fused_lidar_str = "/fused_lidar";
	std::string name_space_str = ros::this_node::getNamespace();
	if (name_space_str != "/")
	{
		left_lidar_str = name_space_str + left_lidar_str;
		right_lidar_str = name_space_str + right_lidar_str;
		front_lidar_str = name_space_str + front_lidar_str;
		fused_lidar_str = name_space_str + fused_lidar_str;
	}

	in_private_handle.param<std::string>("left_lidar_points", left_lidar_str, "/lidar_left/velodyne_points");
	ROS_INFO("[%s] left_lidar_points: %s",__APP_NAME__, left_lidar_str.c_str());

	in_private_handle.param<std::string>("right_lidar_points", right_lidar_str, "/lidar_right/velodyne_points");
	ROS_INFO("[%s] right_lidar_points: %s",__APP_NAME__, right_lidar_str.c_str());

	in_private_handle.param<std::string>("front_lidar_points", front_lidar_str, "/lidar_front/velodyne_points");
	ROS_INFO("[%s] front_lidar_points: %s",__APP_NAME__, front_lidar_str.c_str());

	in_private_handle.param<bool>("is_cloud_colored", cloud_is_colored_, false);
	ROS_INFO("[%s] is_cloud_colored: %s",__APP_NAME__, cloud_is_colored_ ? "true" : "false");

	//generate subscribers and sychronizers
	cloud_left_subscriber_ = new message_filters::Subscriber<sensor_msgs::PointCloud2>(node_handle_,
	                                                                                     left_lidar_str, 10);
	ROS_INFO("[%s] Subscribing to... %s",__APP_NAME__, left_lidar_str.c_str());

	cloud_right_subscriber_ = new message_filters::Subscriber<sensor_msgs::PointCloud2>(node_handle_,
	                                                                                     right_lidar_str, 10);
	ROS_INFO("[%s] Subscribing to... %s",__APP_NAME__, right_lidar_str.c_str());

	cloud_front_subscriber_ = new message_filters::Subscriber<sensor_msgs::PointCloud2>(node_handle_,
	                                                                                     front_lidar_str, 10);
	ROS_INFO("[%s] Subscribing to... %s",__APP_NAME__, front_lidar_str.c_str());

	merged_lidars_ = node_handle_.advertise<sensor_msgs::PointCloud2>(fused_lidar_str, 1);
	ROS_INFO("[%s] Publishing merged PointCloud to... %s",__APP_NAME__, fused_lidar_str.c_str());

// Syncing all subscribers
	cloud_synchronizer_ =
			new message_filters::Synchronizer<SyncPolicyT>(SyncPolicyT(100),
																									 	 *cloud_front_subscriber_,
																										 *cloud_left_subscriber_,
																										 *cloud_right_subscriber_);
	cloud_synchronizer_->registerCallback(boost::bind(&ROSLidarPCDMergeApp::PointsCallback, this, _1, _2, _3));
}
void ROSLidarPCDMergeApp::Run()
{
	ros::NodeHandle private_node_handle("~");

	tf::TransformListener transform_listener;
	transform_listener_ = &transform_listener;

	InitializeROSIo(private_node_handle);

	ROS_INFO("[%s] Ready. Waiting for data...", __APP_NAME__);

	ros::spin();

	ROS_INFO("[%s] END", __APP_NAME__);
}
ROSLidarPCDMergeApp::ROSLidarPCDMergeApp()
{
	cloud_is_colored_ = false;
}
