#ifndef PROJECT_LIDAR_PCD_MERGE_H
#define PROJECT_LIDAR_PCD_MERGE_H

#define __APP_NAME__ "lidar_pcd_merge"

#include <string>
#include <vector>
#include <chrono>
#include <ros/ros.h>
#include <tf/tf.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl_ros/transforms.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>


#include <Eigen/Eigen>

namespace std {
	template <>
	class hash< cv::Point >{
	public :
		size_t operator()(const cv::Point &pixel_cloud ) const
		{
			return hash<std::string>()( std::to_string(pixel_cloud.x) + "|" + std::to_string(pixel_cloud.y) );
		}
	};
};

class ROSLidarPCDMergeApp
{
	ros::NodeHandle                     node_handle_;
	ros::Publisher                      merged_lidars_;
	ros::Subscriber                     left_lidar_subscriber_;
	ros::Subscriber                     right_lidar_subscriber_;
	ros::Subscriber                     front_lidar_subscriber_;

	tf::TransformListener*              transform_listener_;
	tf::StampedTransform                left_front_tf_;
	tf::StampedTransform                right_front_tf_;
	tf::StampedTransform                front_tf_;

//position of left lidar w.r.t front lidar
 double                              left_x_;
 double                              left_y_;
 double                              left_z_;
 double                              left_roll_;
 double                              left_pitch_;
 double                              left_yaw_;

//position of rightt lidar w.r.t front lidar
 double                              right_x_;
 double                              right_y_;
 double                              right_z_;
 double                              right_roll_;
 double                              right_pitch_;
 double                              right_yaw_;

 bool 															 cloud_is_colored_;

 std::string                         parent_front_frame_;
 std::string                         child_left_frame_;
 std::string                         child_right_frame_;

	pcl::PointCloud<pcl::PointXYZRGB>   colored_cloud_;
	//std::unordered_map<cv::Point, pcl::PointXYZ> projection_map_;

	typedef
	message_filters::sync_policies::ApproximateTime<sensor_msgs::PointCloud2,
			sensor_msgs::PointCloud2, sensor_msgs::PointCloud2>   SyncPolicyT;

	typedef pcl::PointXYZ              PointT;
	typedef pcl::PointXYZRGB            PointC;

	message_filters::Subscriber<sensor_msgs::PointCloud2>   *cloud_front_subscriber_, *cloud_left_subscriber_, *cloud_right_subscriber_;
	message_filters::Synchronizer<SyncPolicyT>              *cloud_synchronizer_;

	void InitializeROSIo(ros::NodeHandle& in_private_handle);

	void PointsCallback(const sensor_msgs::PointCloud2::ConstPtr& parent_front_cloud_msg,
											const sensor_msgs::PointCloud2::ConstPtr& child_left_cloud_msg,
	                    const sensor_msgs::PointCloud2::ConstPtr& child_right_cloud_msg);

	tf::StampedTransform
	FrameTransform(const std::string &in_target_frame, const std::string &in_source_frame);

	PointT PointTransform(const PointT &in_point, const tf::StampedTransform &in_transform);

	void PublishCloud(const ros::Publisher& in_publisher, pcl::PointCloud<PointT>::ConstPtr in_cloud_to_publish_ptr);

public:
	void Run();
	ROSLidarPCDMergeApp();
};

#endif //PROJECT_LIDAR_PCD_MERGE_H
