#!/bin/bash

#this shell file launches #all lidars #all cameras with rectification

#cmd1="sudo modprobe usbcore usbfs_memory_mb=1500"
cmd1="roslaunch mazda mazda_all_lidars.launch"
cmd2="roslaunch mazda mazda_all_cameras.launch"
cmd3="sleep 3"
cdm4="roslaunch lidar_pcd_merge lidar_pcd_merge.launch"

gnome-terminal --tab --command="bash -c '$cmd1; $SHELL '"\
	       --tab --command="bash -c '$cmd3; $cmd2; $SHELL '"\
	       --tab --command="bash -c '$cmd4; $SHELL '"\
