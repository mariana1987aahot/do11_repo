#!/usr/bin/env python
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import cv2
import rospy 
from sensor_msgs.msg import Image,LaserScan,Joy
from geometry_msgs.msg import Pose,Twist
import sys
from autoware_msgs.msg import VehicleCmd

def Subscriber():
     rospy.Subscriber('/vehicle_cmd',VehicleCmd,vehicle_callback)
     

def vehicle_callback(data):

    rospy.loginfo("Receiving Information")
    msg =Joy()
    msg.header.seq = 0
    
    msg.axes = [0.0, data.ctrl_cmd.linear_velocity, 0.0, data.ctrl_cmd.steering_angle, 0.0, 0.0, 0.0,0.0]  
 
    # put map conditions here
    vehicle_pub = rospy.Publisher('/joy',Joy,queue_size=10)
    vehicle_pub.publish(msg)



if __name__ == '__main__':
    rospy.init_node('converted')
    Subscriber() 
    r = rospy.Rate(100)
    while not rospy.is_shutdown():
        # r.sleep()
        rospy.spin()
