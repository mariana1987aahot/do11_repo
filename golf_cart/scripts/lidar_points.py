import cv2
import rospy
import numpy as np
import pandas as pd
import sensor_msgs.point_cloud2 as pc2
import pcl
import pcl.pcl_visualization
from sensor_msgs.msg import PointCloud2
#from sklearn.cluster import kmeans 
# make pointcloud object 
global visual 

def lidar_subscriber():
    rospy.Subscriber('/velodyne_points',PointCloud2,lidar_callback)


def ros_to_pcl(ros_cloud):

    """ Converts a ROS PointCloud2 message to a pcl PointXYZRGB
    
        Args:
            ros_cloud (PointCloud2): ROS PointCloud2 message
            
        Returns:
            pcl.PointCloud_PointXYZRGB: PCL XYZRGB point cloud
    """
    points_list = []

    for data in pc2.read_points(ros_cloud, skip_nans=True):
        points_list.append([data[0], data[1], data[2], data[3]])

    pcl_data = pcl.PointCloud_PointXYZRGB()
    pcl_data.from_list(points_list)

    return pcl_data    
visual = pcl.pcl_visualization.CloudViewing()
def lidar_callback(cloud):
    rospy.loginfo("received pointcloud")
    cloud_num = ros_to_pcl(cloud)
    rospy.loginfo(cloud_num)
    visual.ShowColorCloud(cloud_num,b'cloud')









if __name__ == '__main__':
    rospy.init_node('LIDAR')
    lidar_subscriber()
    r = rospy.Rate(40)
    while not rospy.is_shutdown():
        #r.sleep()
        rospy.spin()   