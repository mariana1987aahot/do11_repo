#!/usr/bin/env python
#THIS SCRIPT IS TO GIVE GOAL POSE


import rospy
from geometry_msgs.msg import PoseStamped


rospy.init_node('goal_pos')
pub1 = rospy.Publisher('move_base_simple/goal', PoseStamped, queue_size = 10)


rospy.sleep(1)
checkpoint = PoseStamped()
checkpoint.header.frame_id = 'world'

checkpoint.pose.position.x = 32.3970336914
checkpoint.pose.position.y = 5.22286748886
checkpoint.pose.position.z = 0.0

checkpoint.pose.orientation.x = 0.0
checkpoint.pose.orientation.y = 0.0
checkpoint.pose.orientation.z = 0.909382742201
checkpoint.pose.orientation.w = 0.415960368528

pub1.publish(checkpoint)

