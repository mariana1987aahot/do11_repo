#include "stanley/stanley.h"


Stanley::Stanley() : nh_(""), pnh_("~")
{
  /* ROSparameters */
  pnh_.param("pose_offset_x", pose_offset_x_, float(0.0));
  pnh_.param("pose_offset_y", pose_offset_y_, float(0.0));
  pnh_.param("is_const_velocity", is_const_velocity_, bool(true));
  pnh_.param("velocity", velocity_, float(0.0));
  pnh_.param("velocity_pot_field", velocity_pf_, float(0.0));
  pnh_.param("cte_threshold", cte_threshold_, float(0.07));
  pnh_.param("cte_threshold_pot_field", cte_threshold_pf_, float(0.07));
  pnh_.param("steering_limit", steering_limit_, float(1.0));
  pnh_.param("throttle_limit_min", throttle_limit_min_, float(0.0)); //TODO
  pnh_.param("throttle_limit_max", throttle_limit_max_, float(0.0)); //TODO
  pnh_.param("speed_limit", speed_limit_, float(0.0)); 
  
  //remember to set gazebo as param for it reverses steering


  steering_pub_ =  nh_.advertise<autoware_msgs::VehicleCmd>("/vehicle_cmd", 1);
  vis_pub_closest_ = nh_.advertise<visualization_msgs::Marker>( "closest_waypoint_marker", 1 );
  vis_pub_goal_  = nh_.advertise<visualization_msgs::Marker>( "goal_waypoint_marker", 1 );
  
//  closest_waypoint_stanley_  = nh_.advertise<geometry_msgs::PoseStamped>( "closest_waypoint_stanley", 1 );
  closest_waypoint_index_ = nh_.advertise<std_msgs::Int32>( "closest_waypoint_index", 1 );
  ndt_sub_ =  nh_.subscribe("ndt_pose", 1000, &Stanley::ndtCallback, this);
  gnss_sub_ =  nh_.subscribe("gps/rtkfix", 1000, &Stanley::gnssCallback, this);
  kalman_sub_ = nh_.subscribe("kalman_pose", 1000, &Stanley::kalmanCallback, this);
//  ndt_sub_ =  nh_.subscribe("gazebo_vehicle/pose", 1000, &Stanley::ndtCallback, this);
  waypoint_sub_ = nh_.subscribe("lane_waypoints_array1", 1000, &Stanley::waypointCallback, this);
  pot_field_replan_sub_ = nh_.subscribe("potential_field_replan_state", 1000, &Stanley::replanPotStateCallback, this);
  pot_field_path_sub_ = nh_.subscribe("potential_field_replan_path", 1000, &Stanley::replanPotPathCallback, this);
  is_path_feasible_sub_ = nh_.subscribe("is_path_feasible", 1000, &Stanley::isPathFeasibleCallback, this);
  
//  initGoalWaypointIndex();
};

Stanley::~Stanley(){};
//static volatile bool keep_running = true;

//static void* userInput_thread(void*)
//{

//       if (std::cin.get() == 'q')
//       {
//           //! desired user input 'q' received
//           keep_running = false;
//       }
//  
//}

int main(int argc, char **argv)
{
//  pthread_t tId;
//  (void) pthread_create(&tId, 0, userInput_thread, 0);

  ros::init(argc, argv, "stanley_node");
  Stanley obj;
  
//  while(keep_running){
    ros::spin();
//  }
  
//  (void) pthread_join(tId, NULL);

  return 0;
};

std::vector<float> Stanley::getCurrentPose()
{
  //Extract the current x, y position and orientation quaternions
  //Converting quaternios to RPY and extracting theta_current to calculate heading error
//  tf::Quaternion current_orientation(
//  current_ndt_pose_->pose.orientation.x, current_ndt_pose_->pose.orientation.y, 
//  current_ndt_pose_->pose.orientation.z, current_ndt_pose_->pose.orientation.w);
//  tf::Matrix3x3 m(current_orientation);
//  double roll, pitch, theta_current;
//  m.getRPY(roll, pitch, theta_current);
//  
  //when using ndt pose
//  std::vector<float> pose_info = {(float)current_ndt_pose_->pose.position.x,
//  (float)current_ndt_pose_->pose.position.y,
//  (float)theta_current};
  
  //when using gnss pose
//  std::vector<float> pose_info = {(float)current_gnss_pose_->pose.pose.position.x,
//  (float)current_gnss_pose_->pose.pose.position.y,
//  (float)theta_current};

  //When using gazebo simulation that has steering front link with kalman pose
  geometry_msgs::PoseStamped initial_pt;
  initial_pt.header.frame_id = "steering_front_link";
  initial_pt.pose.position.x = 0;
  initial_pt.pose.position.y = 0;
  initial_pt.pose.position.z = 0;
  initial_pt.pose.orientation.x = 0.0;
  initial_pt.pose.orientation.y = 0.0;
  initial_pt.pose.orientation.z = 0.0;
  initial_pt.pose.orientation.w = 1.0;
  
  geometry_msgs::Pose transformed_pose;
  transformed_pose = lookupTF(initial_pt, "world", "steering_front_link");
  
  std::cout << "Transformed front axle = "<<transformed_pose.position.x<<" "<< transformed_pose.position.y<<std::endl;
  
  tf::Quaternion current_orientation(
  transformed_pose.orientation.x, transformed_pose.orientation.y, 
  transformed_pose.orientation.z, transformed_pose.orientation.w);
  tf::Matrix3x3 m(current_orientation);
  double roll, pitch, theta_current;
  m.getRPY(roll, pitch, theta_current);

//  when using kalman pose
  std::vector<float> pose_info = {
  (float)transformed_pose.position.x + pose_offset_x_,
  (float)transformed_pose.position.y + pose_offset_y_,
  (float)theta_current};

//  tf::Quaternion current_orientation(
//  current_kalman_pose_->pose.orientation.x, current_kalman_pose_->pose.orientation.y, 
//  current_kalman_pose_->pose.orientation.z, current_kalman_pose_->pose.orientation.w);
//  tf::Matrix3x3 m(current_orientation);
//  double roll, pitch, theta_current;
//  m.getRPY(roll, pitch, theta_current);

//  std::vector<float> pose_info = {
//  (float)current_kalman_pose_->pose.position.x + pose_offset_x_,
//  (float)current_kalman_pose_->pose.position.y + pose_offset_y_,
//  (float)theta_current};
  //    goal_x += 3.48; //uncomment when using gazebo_vehicle/pose
//    goal_y += -11.974;
    
//    goal_x -= 3.48; //uncomment when using kalman pose 
//    goal_y -= -11.974;
  
  ROS_INFO("in funct [%f]", pose_info[0]);
  return pose_info;
}


geometry_msgs::Pose Stanley::lookupTF(geometry_msgs::PoseStamped initial_pt, std::string destination, std::string original)
{
//    std::cout <<"insideTF x = "<< initial_pt.pose.position.x <<" insideTF y = "<< initial_pt.pose.position.y << std::endl;
    
    geometry_msgs::Pose transformed_pose;  
    tf::Transform dest_to_orig;
    tf::poseMsgToTF(initial_pt.pose, dest_to_orig);
    tf::StampedTransform req_to_dest;
    try
    {
      listener_.lookupTransform(destination, original, ros::Time(0), req_to_dest);
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }
    tf::Transform req_to_orig;
    req_to_orig = req_to_dest * dest_to_orig;
    tf::poseTFToMsg(req_to_orig, transformed_pose);
    
    return transformed_pose;
}


void Stanley::initGoalWaypointIndex()
{
  goal_waypoint_index_ = 0;
  curv_wp_index_ = 0;
}

void Stanley::replanPotPathCallback(const geometry_msgs::PoseArray::ConstPtr& msg)
{
  current_pot_field_path_ = std::make_shared <geometry_msgs::PoseArray>(*msg);
}

void Stanley::replanPotStateCallback(const std_msgs::Bool::ConstPtr& msg)
{
  current_pot_field_state_ = std::make_shared <std_msgs::Bool>(*msg);
}

void Stanley::isPathFeasibleCallback(const std_msgs::Bool::ConstPtr& msg)
{
  current_path_feasibility_ = std::make_shared<std_msgs::Bool>(*msg);
}

void Stanley::kalmanCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{  
  current_kalman_pose_ = std::make_shared <geometry_msgs::PoseStamped>(*msg);   
}


void Stanley::gnssCallback(const nav_msgs::Odometry::ConstPtr& msg)
{  
  current_gnss_pose_ = std::make_shared <nav_msgs::Odometry>(*msg);   
}


void Stanley::ndtCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  current_ndt_pose_ = std::make_shared<geometry_msgs::PoseStamped>(*msg);
  
}

void Stanley::waypointCallback( const autoware_msgs::LaneArray::ConstPtr& msg)
{
  current_lanes_ = std::make_shared<autoware_msgs::LaneArray>(*msg);
  //when using ndt_pose
//  if(current_lanes_ != nullptr && current_ndt_pose_ != nullptr)
//  {
//    steeringController();
//  }
  //When using gnss pose
//  if(current_lanes_ != nullptr && current_gnss_pose_ != nullptr)
//  {
//    steeringController();
//  }
//  When using kalman
  if(current_lanes_ != nullptr && current_kalman_pose_ != nullptr)
  {
    steeringController();
  }
  else
  {
    ROS_INFO("No Kalman pose or no current lanes");
    autoware_msgs::VehicleCmd  desired_steering_angle_;
    desired_steering_angle_.ctrl_cmd.steering_angle = 0.0;
    desired_steering_angle_.twist_cmd.twist.angular.z = 0.0;
    desired_steering_angle_.ctrl_cmd.linear_velocity = 0.0;
    desired_steering_angle_.twist_cmd.twist.linear.x = 0.0;

    steering_pub_.publish(desired_steering_angle_);
  }
}

float Stanley::euclidianDistance(float x1, float y1, float x2, float y2)
{
  float dist;
  float x = x1 - x2;
  float y = y1 - y2;
  dist = sqrt(pow(x, 2) + pow (y, 2));
  return dist;
}

float Stanley::getCrossTrackError(float current_x, float current_y, float goal_x, float goal_y, float theta_goal)
{
  float m, c, a, b, d;
  m = tan (theta_goal);
  c = goal_y - (m * goal_x);
  a = -m;
  b = 1.0;
  d = std::abs(((a * current_x) + (b * current_y))) / std::sqrt((a*a) + (b*b));
  return d;
  
}

int Stanley::getClosestPoint(float current_x, float current_y)
{
  int closest_waypoint_index {goal_waypoint_index_};
  float temp1;
  float closest_dist {10000.0};
  float temp_x, temp_y {0.0};
  
  //Search for closest waypoints only within a defined range of the current goal_waypoint_index_
  if (goal_waypoint_index_ <=10) 
  {
    for(int i{0}; i<(current_lanes_->lanes[0].waypoints.size()); ++i)
    {
      temp_x = current_lanes_->lanes[0].waypoints[i].pose.pose.position.x;
      temp_y = current_lanes_->lanes[0].waypoints[i].pose.pose.position.y;
      temp1 = euclidianDistance(current_x, current_y, temp_x, temp_y);
      if(temp1 < closest_dist)
      {
        closest_dist = temp1;
        closest_waypoint_index = i;
      }
    }
  }
  else if(goal_waypoint_index_ >= (current_lanes_->lanes[0].waypoints.size()-11))
  {
    for(int i{goal_waypoint_index_ - 10}; i<(current_lanes_->lanes[0].waypoints.size()); ++i)
    {
      temp_x = current_lanes_->lanes[0].waypoints[i].pose.pose.position.x;
      temp_y = current_lanes_->lanes[0].waypoints[i].pose.pose.position.y;
      temp1 = euclidianDistance(current_x, current_y, temp_x, temp_y);
      if(temp1 < closest_dist)
      {
        closest_dist = temp1;


        closest_waypoint_index = i;
      }
    }
  }
  else if (goal_waypoint_index_ >= 10 && goal_waypoint_index_ < current_lanes_->lanes[0].waypoints.size()-11)
  {
    for(int i{goal_waypoint_index_ - 10}; i<(goal_waypoint_index_ + 10); ++i)
    {
      temp_x = current_lanes_->lanes[0].waypoints[i].pose.pose.position.x;
      temp_y = current_lanes_->lanes[0].waypoints[i].pose.pose.position.y;
      temp1 = euclidianDistance(current_x, current_y, temp_x, temp_y);
      if(temp1 < closest_dist)
      {
        closest_dist = temp1;
        closest_waypoint_index = i;
      }
    }
  }
  return closest_waypoint_index;
}

//function that decides turning direction for the cte term
float Stanley::getLorRFromClosestWaypoint(float current_x, float current_y, 
                          float wp1_x, float wp1_y, float wp2_x, float wp2_y)
{
  float var {0.0};
  var = (wp2_y - wp1_y)*(current_x - wp1_x) + (wp1_x - wp2_x)*(current_y - wp1_y);
  return var;//0 for on the line, 1 for right hand side of the line, -1 for left
}

std::vector<std::vector<float>>  Stanley::getXYCoordinatesArray(int curv_wp_index_, int closest_waypoint_index)
{
  std::vector<std::vector<float>> vec(2);//2 rows
  vec[0] = std::vector<float>(curv_wp_index_ - closest_waypoint_index);//size of each row
  vec[1] = std::vector<float>(curv_wp_index_ - closest_waypoint_index);
  for (int j{0}; j < curv_wp_index_ - closest_waypoint_index; j++)
  {
    vec[0][j] = current_lanes_->lanes[0].waypoints[j + closest_waypoint_index].pose.pose.position.x;
    vec[1][j] = current_lanes_->lanes[0].waypoints[j + closest_waypoint_index].pose.pose.position.y;
  }
  return vec;
}


//Function to find the curvature of the road ahead
float Stanley::getCurvature(int curv_wp_index_, int closest_waypoint_index)
{
  float curvature {0.0};
  int N {0};//number of points in polynomial
  N = (curv_wp_index_ - closest_waypoint_index) + 1;
  std::vector<std::vector<float>> vec(2);//vector having 2 rows
  vec[0] = std::vector<float>(N);//size of each row = number of wpts in polynomial
  vec[1] = std::vector<float>(N);
  vec = getXYCoordinatesArray(curv_wp_index_, closest_waypoint_index);
  
  int i,j,k,n;
  n = 2;// n is the degree of Polynomial 

  float X[2*n+1];                        //Array that will store the values of sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
  for (i=0;i<2*n+1;i++)
  {
      X[i]=0;
      for (j=0;j<N;j++)
          X[i]=X[i]+pow(vec[0][j],i);        //consecutive positions of the array will store N,sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
  }
  float B[n+1][n+2],a[n+1];            //B is the Normal matrix(augmented) that will store the equations, 'a' is for value of the final coefficients
  for (i=0;i<=n;i++)
      for (j=0;j<=n;j++)
          B[i][j]=X[i+j];            //Build the Normal matrix by storing the corresponding coefficients at the right positions except the last column of the matrix
  float Y[n+1];                    //Array to store the values of sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
  for (i=0;i<n+1;i++)
  {    
      Y[i]=0;
      for (j=0;j<N;j++)
      Y[i]=Y[i]+pow(vec[0][j],i)*vec[1][j];        //consecutive positions will store sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
  }
  for (i=0;i<=n;i++)
      B[i][n+1]=Y[i];                //load the values of Y as the last column of B(Normal Matrix but augmented)
  n=n+1;                //n is made n+1 because the Gaussian Elimination part below was for n equations, but here n is the degree of polynomial and for n degree we get n+1 equations

  for (i=0;i<n;i++)                    //From now Gaussian Elimination starts(can be ignored) to solve the set of linear equations (Pivotisation)
      for (k=i+1;k<n;k++)
          if (B[i][i]<B[k][i])
              for (j=0;j<=n;j++)
              {
                  float temp=B[i][j];
                  B[i][j]=B[k][j];
                  B[k][j]=temp;
              }
  
  for (i=0;i<n-1;i++)            //loop to perform the gauss elimination
      for (k=i+1;k<n;k++)
          {
              float t=B[k][i]/B[i][i];
              for (j=0;j<=n;j++)
                  B[k][j]=B[k][j]-t*B[i][j];    //make the elements below the pivot elements equal to zero or elimnate the variables
          }
  for (i=n-1;i>=0;i--)                //back-substitution
  {                        //x is an array whose values correspond to the values of x,y,z..
      a[i]=B[i][n];                //make the variable to be calculated equal to the rhs of the last equation
      for (j=0;j<n;j++)
          if (j!=i)            //then subtract all the lhs values except the coefficient of the variable whose value                                   is being calculated
              a[i]=a[i]-B[i][j]*a[j];
      a[i]=a[i]/B[i][i];            //now finally divide the rhs by the coefficient of the variable to be calculated
  }

  float x {0.0};
  x = current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.position.x;
  
  curvature = (pow(pow((1.0 + (2.0*a[2]*x + a[1])),2.0),1.5)) / std::abs(2.0*a[2]);
  return curvature;
}

float Stanley::getAverageCurvature(int curv_wp_index_, int closest_waypoint_index)
{
  float curvature {0.0};
  int N {0};//number of points in polynomial
  N = (curv_wp_index_ - closest_waypoint_index) + 1;
  std::vector<std::vector<float>> vec(2);//vector having 2 rows
  vec[0] = std::vector<float>(N);//size of each row = number of wpts in polynomial
  vec[1] = std::vector<float>(N);
  vec = getXYCoordinatesArray(curv_wp_index_, closest_waypoint_index);
  
  int i,j,k,n;
  n = 2;// n is the degree of Polynomial 

  float X[2*n+1];                        //Array that will store the values of sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
  for (i=0;i<2*n+1;i++)
  {
      X[i]=0;
      for (j=0;j<N;j++)
          X[i]=X[i]+pow(vec[0][j],i);        //consecutive positions of the array will store N,sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
  }
  float B[n+1][n+2],a[n+1];            //B is the Normal matrix(augmented) that will store the equations, 'a' is for value of the final coefficients
  for (i=0;i<=n;i++)
      for (j=0;j<=n;j++)
          B[i][j]=X[i+j];            //Build the Normal matrix by storing the corresponding coefficients at the right positions except the last column of the matrix
  float Y[n+1];                    //Array to store the values of sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
  for (i=0;i<n+1;i++)
  {    
      Y[i]=0;
      for (j=0;j<N;j++)
      Y[i]=Y[i]+pow(vec[0][j],i)*vec[1][j];        //consecutive positions will store sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
  }
  for (i=0;i<=n;i++)
      B[i][n+1]=Y[i];                //load the values of Y as the last column of B(Normal Matrix but augmented)
  n=n+1;                //n is made n+1 because the Gaussian Elimination part below was for n equations, but here n is the degree of polynomial and for n degree we get n+1 equations

  for (i=0;i<n;i++)                    //From now Gaussian Elimination starts(can be ignored) to solve the set of linear equations (Pivotisation)
      for (k=i+1;k<n;k++)
          if (B[i][i]<B[k][i])
              for (j=0;j<=n;j++)
              {
                  float temp=B[i][j];
                  B[i][j]=B[k][j];
                  B[k][j]=temp;
              }
  
  for (i=0;i<n-1;i++)            //loop to perform the gauss elimination
      for (k=i+1;k<n;k++)
          {
              float t=B[k][i]/B[i][i];
              for (j=0;j<=n;j++)
                  B[k][j]=B[k][j]-t*B[i][j];    //make the elements below the pivot elements equal to zero or elimnate the variables
          }
  for (i=n-1;i>=0;i--)                //back-substitution
  {                        //x is an array whose values correspond to the values of x,y,z..
      a[i]=B[i][n];                //make the variable to be calculated equal to the rhs of the last equation
      for (j=0;j<n;j++)
          if (j!=i)            //then subtract all the lhs values except the coefficient of the variable whose value                                   is being calculated
              a[i]=a[i]-B[i][j]*a[j];
      a[i]=a[i]/B[i][i];            //now finally divide the rhs by the coefficient of the variable to be calculated
  }

  std::vector<double> x = {current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.position.x,
                current_lanes_->lanes[0].waypoints[goal_waypoint_index_ + 3].pose.pose.position.x,
                current_lanes_->lanes[0].waypoints[goal_waypoint_index_ + 9].pose.pose.position.x};

  for (auto i: x){
    curvature += (pow(pow((1.0 + (2.0*a[2]*i + a[1])),2.0),1.5)) / std::abs(2.0*a[2]);
  }
  curvature /= x.size();
  
  return curvature;
}

void Stanley::displayVisMarkers(const float goal_x, const float goal_y, const float closest_x, const float closest_y)
{
  visualization_msgs::Marker closest;
  closest.header.frame_id = "map";
  closest.header.stamp = ros::Time();
  closest.ns = "global_lane_array_marker";
  closest.id = 0;
  closest.type = visualization_msgs::Marker::SPHERE;
  closest.action = visualization_msgs::Marker::ADD;
//  closest.action = 2;
//  closest.pose.position.x = goal_x;
//  closest.pose.position.y = goal_y;
  closest.pose.position.x = closest_x;
  closest.pose.position.y = closest_y;
  closest.pose.position.z = 0.0;
  closest.pose.orientation.x = 0.0;
  closest.pose.orientation.y = 0.0;
  closest.pose.orientation.z = 0.0;
  closest.pose.orientation.w = 1.0;
  closest.scale.x = 1;
  closest.scale.y = 1;
  closest.scale.z = 1;
  closest.color.a = 1.0; // Don't forget to set the alpha!
  closest.color.r = 0.0;
  closest.color.g = 1.0;
  closest.color.b = 0.0;
  vis_pub_closest_.publish( closest );
  
  visualization_msgs::Marker goal;
  goal.header.frame_id = "map";
  goal.header.stamp = ros::Time();
  goal.ns = "global_lane_array_marker";
  goal.id = 0;
  goal.type = visualization_msgs::Marker::SPHERE;
  goal.action = visualization_msgs::Marker::ADD;
//  closest.action = 2;
//  closest.pose.position.x = goal_x;
//  closest.pose.position.y = goal_y;
  goal.pose.position.x = goal_x;
  goal.pose.position.y = goal_y;
  goal.pose.position.z = 0.0;
  goal.pose.orientation.x = 0.0;
  goal.pose.orientation.y = 0.0;
  goal.pose.orientation.z = 0.0;
  goal.pose.orientation.w = 1.0;
  goal.scale.x = 1;
  goal.scale.y = 1;
  goal.scale.z = 1;
  goal.color.a = 1.0; // Don't forget to set the alpha!
  goal.color.r = 1.0;
  goal.color.g = 0.0;
  goal.color.b = 0.0;
  vis_pub_goal_.publish( goal );
}

//void Stanley::publishClosestWaypoint(const float closest_x, const float closest_y)
//{
//  geometry_msgs::PoseStamped closest_waypoint;
//  closest_waypoint.header.frame_id = "map"
//  closest_waypoint.pose.position.x = closest_x;
//  closest_waypoint.pose.position.y = closest_y;
//  closest_waypoint.pose.position.z = 0;
//  closest_waypoint.pose.orientation.x = 0.0;
//  closest_waypoint.pose.orientation.y = 0.0;
//  closest_waypoint.pose.orientation.z = 0.0;
//  closest_waypoint.pose.orientation.w = 1.0;
//  closest_waypoint_stanley_.publish(closest_waypoint);
//}

//float Stanley::updateGoalWaypoint(float current_x, float current_y)
//{
////  return goal_waypoint_index_;
//}
float Stanley::speedControl(int curv_wp_index_, int closest_waypoint_index)
{
  float curvature = getAverageCurvature(curv_wp_index_, closest_waypoint_index);
  float v = std::sqrt(lat_acc_limit_ * curvature);
  if(v>speed_limit_)
    return speed_limit_;
  else if(v<2.5)
    return 2.5;
  else return v;
  // return (v>speed_limit_) ? speed_limit_ : v;
}

void Stanley::steeringController()
{
//  auto start = high_resolution_clock::now(); 
  time_t start, end; 
  time(&start);
  std::ios_base::sync_with_stdio(false);
  
  float current_x, current_y, theta_current;
//  current_x, current_y= getCurrentPose();
  
  std::vector<float> pose_info = {0.0, 0.0, 0.0};
  pose_info = getCurrentPose();
  current_x = pose_info[0];
  current_y = pose_info[1];
  theta_current = pose_info[2];
//  ROS_INFO("in steer controller [%f], [%f]", current_x, current_y);
  if (current_pot_field_state_ != nullptr && current_pot_field_path_ != nullptr)
  {
    if (current_pot_field_state_->data == true){
    //follow the 1st waypoint in replanned path
      float goal_x, goal_y, cte, cte_stanley;
      float wp1_x, wp1_y, wp2_x, wp2_y, var, vel, k_cte;
      goal_x = current_pot_field_path_->poses[5].position.x;
      goal_y = current_pot_field_path_->poses[5].position.y;
      cte = euclidianDistance(current_x, current_y, goal_x, goal_y);
      wp1_x = goal_x;
      wp1_y = goal_y;
      //potential segfault,,,,beware
      wp2_x = current_pot_field_path_->poses[6].position.x;
      wp2_y = current_pot_field_path_->poses[6].position.y;
      var = getLorRFromClosestWaypoint(current_x, current_y, 
                               wp1_x, wp1_y, wp2_x, wp2_y);
      vel = 1.0; //m/s
      k_cte = 0.25; //Controller gain for cross track error 0.25
      cte_stanley = 0.5*atan(k_cte * cte / vel);
  //    ROS_INFO("var = [%f], -ve left || +ve right", var);
      if (var > 0.0 && cte > cte_threshold_pf_)//if car is on the right of wp and has a high deviation
        cte_stanley = std::abs(cte_stanley);//turn left
      else if (var < 0.0 && cte > cte_threshold_pf_) //if car is on the left of wp and has a high deviation
        cte_stanley = -(std::abs(cte_stanley));//turn right
      else if (cte <=cte_threshold_pf_)
        cte_stanley = 0.0;
      
      
      float theta_err, theta_stanley, k_theta;
      //if they are in the lower quadrants and they have different signs 
      float theta_goal;
      theta_goal = atan2((wp2_y-wp1_y),(wp2_x-wp1_x));
      if((theta_goal > M_PI/2 || theta_goal < M_PI/2)  && (theta_current < M_PI/2 || theta_current <M_PI/2) && (std::abs(theta_goal-theta_current)>=M_PI))
      {
        theta_err = (theta_current-theta_goal)/(2*M_PI);
      }else{
        theta_err = theta_goal - theta_current; //normal condition
      }

      k_theta = 1.2; //0.8
      //k_theta = 1.0;
      //k_theta = 10.0;

      theta_stanley = k_theta * theta_err; //-ve == right turn
      delta_pf_ = theta_stanley + cte_stanley;
  //    ROS_INFO("Delta PF = [%f]", delta);
  //    if(delta < 0)
  //      ROS_INFO("TURNING RIGHT");
  //    else if(delta > 0)
  //      ROS_INFO("TURNING LEFT");
  //    else
  //      ROS_INFO("GOING STRAIGHT");
    }
  }
  
  
  waypoint_array_size_ = current_lanes_->lanes[0].waypoints.size();
  ROS_INFO("Waypoint Array Size = [%d]", waypoint_array_size_);
  if (waypoint_array_size_ != prev_waypoint_array_size_){
  
    goal_waypoint_index_ = 0;
    prev_waypoint_array_size_ = waypoint_array_size_;
  }
  
  
  //Function to update goal_waypoint_index_
  float goal_x, goal_y, cte;//cte is the cross track error
  goal_x = current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.position.x;
  goal_y = current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.position.y;
  //tf to world coordinates
//  goal_x += 3.48; //uncomment when using gazebo_vehicle/pose
//  goal_y += -11.974;
  
//  goal_x -= 3.48; //uncomment when using kalman pose
//  goal_y -= -11.974;
  
//  Converting quaternions to RPY and extracting theta_goal to calculate heading error
  tf::Quaternion goal_orientation(
  current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.orientation.x,
  current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.orientation.y,
  current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.orientation.z,
  current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.orientation.w);
  tf::Matrix3x3 n(goal_orientation);
  double goal_roll, goal_pitch, theta_goal;
  n.getRPY(goal_roll, goal_pitch, theta_goal);

//  //Set goal waypoint x and y
//  ROS_INFO("goal waypoint no= [%d]", goal_waypoint_index_);


  if (goal_waypoint_index_ < current_lanes_->lanes[0].waypoints.size() && euclidianDistance(current_x, current_y, goal_x, goal_y) <= 2.0) //try 8
  {
    goal_waypoint_index_ += 1;
    
//    ROS_INFO("goal waypoint no= [%d]", goal_waypoint_index_);
    goal_x = current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.position.x;
    goal_y = current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.position.y;
    //tf to world coordinates
//    goal_x += 3.48; //uncomment when using gazebo_vehicle/pose
//    goal_y += -11.974;
    
//    goal_x -= 3.48; //uncomment when using kalman pose
//    goal_y -= -11.974;
    
    tf::Quaternion goal_orientation(
    current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.orientation.x,
    current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.orientation.y,
    current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.orientation.z,
    current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.orientation.w);
    tf::Matrix3x3 n(goal_orientation);
    n.getRPY(goal_roll, goal_pitch, theta_goal);
  }
  
//  //Calculate steering error
  float theta_err, theta_stanley, k_theta;
  //if they are in the lower quadrants and they have different signs 
  if((theta_goal > M_PI/2 || theta_goal < M_PI/2)  && (theta_current < M_PI/2 || theta_current <M_PI/2) && (std::abs(theta_goal-theta_current)>=M_PI)){
    theta_err = (theta_current-theta_goal)/(2*M_PI);
  }else{
    theta_err = theta_goal - theta_current; //normal condition
  }

  k_theta = 0.8; //0.4
//  k_theta = 1.0;
//  k_theta = 10.0;

  theta_stanley = k_theta * theta_err; //-ve == right turn
  

//  
//  goal_x = 15.0;
//  goal_y = 15.0;
//  float cte {0.0};
//  cte = euclidianDistance(goal_x, goal_y, current_x, current_y);

  //Finding closest point, used to calculate cte and polynomial start for curvature
  int closest_waypoint_index {0};
  closest_waypoint_index = getClosestPoint(current_x, current_y);
  float closest_x, closest_y;
  closest_x = current_lanes_->lanes[0].waypoints[closest_waypoint_index].pose.pose.position.x;
  closest_y = current_lanes_->lanes[0].waypoints[closest_waypoint_index].pose.pose.position.y;
  
  //Visualize the closest and goal waypoint
  displayVisMarkers(goal_x, goal_y, closest_x, closest_y);
  
  
  std_msgs::Int32 CWI;
  CWI.data = closest_waypoint_index;
  closest_waypoint_index_.publish(CWI);
  
//  publishClosestWaypoint(closest_x,closest_y);
  
  if(goal_waypoint_index_ < closest_waypoint_index)
  {
    goal_waypoint_index_ = closest_waypoint_index;
    
    goal_x = current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.position.x;
    goal_y = current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.position.y;
    //tf to world coordinates
//    goal_x += 3.48; //uncomment when using gazebo_vehicle/pose
//    goal_y += -11.974;
    
//    goal_x -= 3.48; //uncomment when using kalman pose
//    goal_y -= -11.974;
    
    tf::Quaternion goal_orientation(
    current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.orientation.x,
    current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.orientation.y,
    current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.orientation.z,
    current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.orientation.w);
    tf::Matrix3x3 n(goal_orientation);
    n.getRPY(goal_roll, goal_pitch, theta_goal);
  }
  //Calculate curvature at goal_waypoint_index_
  float curvature {0.0};
  curv_wp_index_ = closest_waypoint_index + 10;
  curvature = getCurvature(curv_wp_index_, closest_waypoint_index);
  ROS_INFO("curvature at [%d] waypt is [%f]", goal_waypoint_index_, curvature);
  
  
  //  //Calculate the cross-track error
  cte = getCrossTrackError(current_x, current_y, closest_x, closest_y, theta_goal);
//  ROS_INFO("closest_wp = [%d] || cte = [%f]", closest_waypoint_index, cte);
//  ROS_INFO("dist = [%f]", euclidianDistance(current_x, current_y, closest_x, closest_y));
  
  //something wrong with cte, take cte=dist
  cte = euclidianDistance(current_x, current_y, closest_x, closest_y);
  float k_cte, vel, cte_stanley;
  vel = 1.0; //m/s
  k_cte = 0.5; //Controller gain for cross track error 0.25
  cte_stanley = 0.5*atan(k_cte * cte / vel);
  
  
  //Find where the car is wrt the waypoints ahead
  float var, wp1_x, wp1_y, wp2_x, wp2_y = {0.0};
//  wp1_x = current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.position.x;
//  wp1_y = current_lanes_->lanes[0].waypoints[goal_waypoint_index_].pose.pose.position.y;
//  wp2_x = current_lanes_->lanes[0].waypoints[goal_waypoint_index_ + 1].pose.pose.position.x;
//  wp2_y = current_lanes_->lanes[0].waypoints[goal_waypoint_index_ + 1].pose.pose.position.y;
//problem where the car should be -ve but = 0
  wp1_x = current_lanes_->lanes[0].waypoints[closest_waypoint_index].pose.pose.position.x;
  wp1_y = current_lanes_->lanes[0].waypoints[closest_waypoint_index].pose.pose.position.y;
  wp2_x = current_lanes_->lanes[0].waypoints[closest_waypoint_index + 1].pose.pose.position.x;
  wp2_y = current_lanes_->lanes[0].waypoints[closest_waypoint_index + 1].pose.pose.position.y;
  var = getLorRFromClosestWaypoint(current_x, current_y, 
                           wp1_x, wp1_y, wp2_x, wp2_y);
  ROS_INFO("var = [%f], -ve left || +ve right", var);
  
  if (var > 0.0 && cte > cte_threshold_)//if car is on the right of wp and has a high deviation
    cte_stanley = std::abs(cte_stanley);//turn left
  else if (var < 0.0 && cte > cte_threshold_) //if car is on the left of wp and has a high deviation
    cte_stanley = -(std::abs(cte_stanley));//turn right
  else if (cte <=cte_threshold_)
    cte_stanley = 0.0;
  
  ROS_INFO("error dist = [%f], cte_stanley = [%f]", cte, cte_stanley );
//  if(cte_stanley < 0)
//    ROS_INFO("TURNING RIGHT");
//  else if(cte_stanley > 0)
//    ROS_INFO("TURNING LEFT");
//  else
//    ROS_INFO("GOING STRAIGHT");
  
  ROS_INFO("theta_stanley = [%f], theta_err = [%f]", theta_stanley, theta_err);
  ROS_INFO("theta current = [%f], theta_goal = [%f]", theta_current, theta_goal);
  float delta {0.0};
//  theta_stanley = 0.0;
  delta = theta_stanley + cte_stanley;
  ROS_INFO("Delta = [%f]", delta);
  if(delta < 0)
    ROS_INFO("TURNING RIGHT");
  else if(delta > 0)
    ROS_INFO("TURNING LEFT");
  else
    ROS_INFO("GOING STRAIGHT");
    
  int size = current_lanes_->lanes[0].waypoints.size();
  ROS_INFO("no of waypoints = [%d]",size);
  ROS_INFO("closest wp = [%d]; goal wp = [%d]", closest_waypoint_index, goal_waypoint_index_);
  
//  //Publish the steering command only if replan state is false
  if (current_pot_field_state_ != nullptr)
  {
    if (current_pot_field_state_->data==false)
    {

      autoware_msgs::VehicleCmd  desired_steering_angle_;
      //Limits for GC=0.8+-
      //Limits for Gazebo = 5+-
      //Limits for Mazda = 1+-
      if (delta < -steering_limit_)
      {
        desired_steering_angle_.ctrl_cmd.steering_angle = -steering_limit_;
        desired_steering_angle_.twist_cmd.twist.angular.z = -steering_limit_; //Adjust the new limits here. Simulation takes -.1
      }
      else if (delta > steering_limit_)
      {
        desired_steering_angle_.ctrl_cmd.steering_angle = steering_limit_;
        desired_steering_angle_.twist_cmd.twist.angular.z = steering_limit_; //Adjust the new limits here. Simulation takes .1
      }
      else if(delta >= -steering_limit_ && delta <= steering_limit_)
      {
    //    desired_steering_angle_.twist_cmd.twist.angular.z = theta_stanley + cte_stanley;
        desired_steering_angle_.ctrl_cmd.steering_angle = delta;
        desired_steering_angle_.twist_cmd.twist.angular.z = delta;
    //    desired_steering_angle_.twist_cmd.twist.angular.z = cte_stanley;
    //    desired_steering_angle_.twist_cmd.twist.angular.z = theta_stanley;
      }
      if (goal_waypoint_index_ == current_lanes_->lanes[0].waypoints.size())
      {
      
      desired_steering_angle_.ctrl_cmd.linear_velocity = 0.0;
      desired_steering_angle_.twist_cmd.twist.linear.x = 0.0;
      desired_steering_angle_.ctrl_cmd.steering_angle = 0.0;
      desired_steering_angle_.twist_cmd.twist.angular.z = 0.0;
      ROS_INFO("GOAL REACHED");
      }
      else
      {
      //Constant Velocity GC =0.6, Gazebo = 1->3
        if(is_const_velocity_ == true)
        {
          desired_steering_angle_.ctrl_cmd.linear_velocity = velocity_;
          desired_steering_angle_.twist_cmd.twist.linear.x = velocity_;//
        }
        else //dynamic velocity control
        {
          desired_steering_angle_.ctrl_cmd.linear_velocity = speedControl(curv_wp_index_, closest_waypoint_index);
          desired_steering_angle_.twist_cmd.twist.linear.x = speedControl(curv_wp_index_, closest_waypoint_index);
           if (curvature<50)
             desired_steering_angle_.twist_cmd.twist.linear.x = 2.5; //1.5
            
           else
             desired_steering_angle_.twist_cmd.twist.linear.x = speed_limit_;//2.5
        }
        
      }
      //Gazebo reverses the turning command
//      desired_steering_angle_.ctrl_cmd.steering_angle = - desired_steering_angle_.ctrl_cmd.steering_angle;
      steering_pub_.publish(desired_steering_angle_);
      ROS_INFO("velocity = [%f]",desired_steering_angle_.twist_cmd.twist.linear.x);
    }
    else if (current_path_feasibility_->data == true)
    {
      ROS_INFO("In PF path");
      //publish the pot field delta and a const vel
      autoware_msgs::VehicleCmd  desired_steering_angle_;
      //Limits for GC=0.8+-
      //Limits for Gazebo = 5+-
      if (delta < -steering_limit_)
      {
        desired_steering_angle_.ctrl_cmd.steering_angle = -steering_limit_;
        desired_steering_angle_.twist_cmd.twist.angular.z = -steering_limit_; //Adjust the new limits here. Simulation takes -.1
      }
      else if (delta > steering_limit_)
      {
        desired_steering_angle_.ctrl_cmd.steering_angle = steering_limit_;
        desired_steering_angle_.twist_cmd.twist.angular.z = steering_limit_; //Adjust the new limits here. Simulation takes .1
      }
      else if(delta >= -steering_limit_ && delta <= steering_limit_)
      {
    //    desired_steering_angle_.twist_cmd.twist.angular.z = theta_stanley + cte_stanley;
        desired_steering_angle_.ctrl_cmd.steering_angle = delta_pf_;
        desired_steering_angle_.twist_cmd.twist.angular.z = delta_pf_;
    //    desired_steering_angle_.twist_cmd.twist.angular.z = cte_stanley;
    //    desired_steering_angle_.twist_cmd.twist.angular.z = theta_stanley;
      }
      
//      if(is_const_velocity_ == true)
//        {
      desired_steering_angle_.ctrl_cmd.linear_velocity = velocity_pf_;
      desired_steering_angle_.twist_cmd.twist.linear.x = velocity_pf_;//
//        }
      
      //Gazebo reverses the turning command
//      desired_steering_angle_.ctrl_cmd.steering_angle = -desired_steering_angle_.ctrl_cmd.steering_angle;
      steering_pub_.publish(desired_steering_angle_);
      ROS_INFO("velocity = [%f]",desired_steering_angle_.twist_cmd.twist.linear.x);
    }
    else
    {
      ROS_INFO("No feasible path");
      autoware_msgs::VehicleCmd  desired_steering_angle_;
      desired_steering_angle_.ctrl_cmd.linear_velocity = 0;
      desired_steering_angle_.twist_cmd.twist.linear.x = 0;//
      desired_steering_angle_.ctrl_cmd.steering_angle = 0;
      desired_steering_angle_.twist_cmd.twist.angular.z = 0;
      steering_pub_.publish(desired_steering_angle_);
      ROS_INFO("velocity = [%f]",desired_steering_angle_.twist_cmd.twist.linear.x);
    }
  }
  
//  auto stop = high_resolution_clock::now(); 
//  auto duration = duration_cast<microseconds>(stop - start);
  time(&end); 
  double duration = double(end - start); 
  ROS_INFO("duration = [%f]", duration);
  ROS_INFO("--------------------------");
  
}


