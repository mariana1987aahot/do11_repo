#include <iostream>
#include <vector>
#include <chrono>
#include <cmath>
#include <string>

#include <ros/ros.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float64MultiArray.h>
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include <autoware_msgs/NDTStat.h>
#include <autoware_msgs/LaneArray.h>
#include <autoware_msgs/VehicleCmd.h>
#include <tf/transform_datatypes.h>
#include <tf2/utils.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/NavSatFix.h>
#include <visualization_msgs/Marker.h>
#include <tf/transform_listener.h>
# define M_PI           3.14159265358979323846  /* pi */
using namespace std::chrono;


class Stanley
{
  public:
    Stanley();
    ~Stanley();

  private:
    ros::NodeHandle nh_, pnh_;
    
      /* ROSparameters */
    float pose_offset_x_;
    float pose_offset_y_; 
    bool is_const_velocity_;
    float velocity_;
    float velocity_pf_;
    float cte_threshold_;
    float cte_threshold_pf_;
    float steering_limit_;
    float throttle_limit_min_;
    float throttle_limit_max_;
    float speed_limit_;
    
    // Add Publishers here
    ros::Publisher steering_pub_;
    ros::Publisher vis_pub_closest_ ;
    ros::Publisher vis_pub_goal_ ; 
    ros::Publisher closest_waypoint_index_;
    //    ros::Publisher closest_waypoint_stanley_;

    // Add Subscribers here
    ros::Subscriber ndt_sub_;
    ros::Subscriber gnss_sub_;
    ros::Subscriber kalman_sub_;
    ros::Subscriber waypoint_sub_;
    ros::Subscriber pot_field_replan_sub_;
    ros::Subscriber pot_field_path_sub_;
    ros::Subscriber is_path_feasible_sub_;
      
    //Add private member variables here
    int goal_waypoint_index_{0};
    int waypoint_array_size_;
    int prev_waypoint_array_size_ {0};
    int curv_wp_index_;
    float lat_acc_limit_{0.5};
    float delta_pf_{0.0};

    //Add shared pointers here
    std::shared_ptr <geometry_msgs::PoseStamped> current_ndt_pose_;
    std::shared_ptr <autoware_msgs::LaneArray> current_lanes_;
    std::shared_ptr <nav_msgs::Odometry> current_gnss_pose_;
    std::shared_ptr <geometry_msgs::PoseStamped> current_kalman_pose_;
    std::shared_ptr <std_msgs::Bool> current_pot_field_state_;
    std::shared_ptr <geometry_msgs::PoseArray> current_pot_field_path_;
    std::shared_ptr <std_msgs::Bool> current_path_feasibility_;
    // std::shared_ptr <autoware_msgs::VehicleCmd> desired_steering_angle_;
    // std::shared_ptr <autoware_msgs::LaneArray> waypoints_array_;
    // std::shared_ptr <std_msgs::Int32> goal_waypoint_index_; //count of the current waypoint

    //Add callbacks here
    void ndtCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
    void waypointCallback(const autoware_msgs::LaneArray::ConstPtr& msg);
    void gnssCallback(const nav_msgs::Odometry::ConstPtr& msg);
    void kalmanCallback(const geometry_msgs::PoseStamped::ConstPtr& msg);
    void replanPotStateCallback(const std_msgs::Bool::ConstPtr& msg);
    void replanPotPathCallback(const geometry_msgs::PoseArray::ConstPtr& msg);
    void isPathFeasibleCallback(const std_msgs::Bool::ConstPtr& msg);
    
    //Add private member functions here
    
    int getClosestPoint(float current_x, float current_y);
    
    float euclidianDistance(float x1, float y1, float x2, float y2);
    float getCrossTrackError(float current_x, float current_y, float goal_x, float goal_y, float theta_goal);
    float getCurvature(int curv_wp_index_, int closest_waypoint_index);
    float getAverageCurvature(int curv_wp_index_, int closest_waypoint_index);
    float speedControl(int curv_wp_index_, int closest_waypoint_index);
    float getLorRFromClosestWaypoint(float current_x, float current_y, 
                          float wp1_x, float wp1_y, float wp2_x, float wp2_y);

    void steeringController();
    void initGoalWaypointIndex();
    void displayVisMarkers(const float goal_x, const float goal_y, const float closest_x, const float closest_y);

    std::vector<float> getCurrentPose();
    std::vector<std::vector<float>> getXYCoordinatesArray(int curv_wp_index_, int closest_waypoint_index);
    
    
//    void publishClosestWaypoint(const float closest_x, const float closest_y);
    
//    float updateGoalWaypoint(float current_x, float current_y);

    tf::TransformListener listener_;
    geometry_msgs::Pose lookupTF(geometry_msgs::PoseStamped initial_pt, std::string destination, std::string original);
};
   /**
   * @brief set twist measurement
   */
