#!/bin/bash
# Shell script to setup can0
# Only run this once the can convertor is connected

sudo modprobe can
sudo modprobe can_raw
sudo modprobe can_dev
sudo modprobe kvaser_usb

sudo ip link set can0 type can bitrate 500000
sudo ip link set up can0
