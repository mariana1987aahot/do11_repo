#!/usr/bin/env python
# coding: utf-8
##todo:count messages in 1 sec and print freq
##read and reciprocate (heartbeat), later put a timeout
##HB: recieve a message with id 11, check and send hb wit0x2
"""
This example shows how sending a single message works.
"""

#$ modprobe can_dev

#$ modprobe can
#$ modprobe can_raw
#  sudo ip link add dev vcan0 type vcan
#  sudo ip link set vcan0 type vcan
#  sudo ip link set up vcan0

# sudo ip link set can0 type can bitrate 500000
# sudo ip link set up can0

from __future__ import print_function
import can
import cantools
import sys
import rospy

db = cantools.db.load_file('/home/deeporange/do11_workspace/src/do11_repo/can/data/Display_Test1.dbc')
#v3 = len(db.messages)
#msg = db.messages[0]
example_msg = db.get_message_by_name('Display_Test')
count = 0

#bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=500000) #can
bus = can.interface.Bus(bustype='socketcan', channel='vcan0', bitrate=500000)#vcan


def send_one():
	MsgId = example_msg.frame_id
	data = example_msg.encode({'Battery_SOC':255, 'Battery_Temp':1,
	 'Inverter_Temperature':1, 'Pump_RPM':1,
	 'Mode':1, 'Fault_Status':1})

	msg1 = can.Message(arbitration_id=MsgId,
	                  data=data,
	                  is_extended_id=False)
	                  
	msg12 = can.Message(arbitration_id=0x14,
	                  data=[0, 1, 2, 3, 4, 5, 6, 7],
	                  is_extended_id=False)
	try:
		bus.send(msg1)
#		bus.send(msg12)
		print("Message sent on {}".format(bus.channel_info))
		global count
		count += 1
		print(count)
	except can.CanError:
		print("Message NOT sent")


if __name__ == "__main__":
	rospy.init_node('can_sender')
	r = rospy.Rate(20)
	while not rospy.is_shutdown():
		send_one()
		r.sleep()

