#!/usr/bin/env python

"""
Take in put from the state machine and send constant commands to 
-Kartech linear actuator for full braking and
-EPAS DCE steering motor for 0 steering
on the // CAN line

Used PEP8 style guide
"""

import rospy
import can
import cantools
import sys
from sensor_msgs.msg import Joy
from std_msgs.msg import Int32


# Load the dbc files adn find the messages we need to send

db_brake = cantools.db.load_file('dbc_files/brakebywire.dbc')
brake_msg = db_brake.get_message_by_name('Position_Command_Default')
db_steer = cantools.db.load_file('dbc_files/Steering_dbc.dbc')
steering_msg = db_steer.get_message_by_name('Command_Primary_Motor_Angle')

#TODO: Implement // can line
bus = can.interface.Bus(bustype='socketcan',
                                channel='vcan0', bitrate=500000)  # can

class emergency_cmd:
    def __init__(self):
        self.msg = Int32()
        self.do11_state_sub = rospy.Subscriber('/do11_state', Int32, self.do11_state_callback)
    
    def send_cmd(self, pressure, angle):
        global pos_msg
        global steering_msg
        
        MsgId_brake = brake_msg.frame_id
        data_brake = brake_msg.encode({'message_type':15, 'confirmation_flag':0,
                                'auto_reply_flag':1, 'data_type':10,
                                'dpos':pressure, 'motor_enable':1, 'clutch_enable':1})
        msg_brake = can.Message(arbitration_id=MsgId_brake,
                                  data=data_brake,
                                  is_extended_id=True)

        MsgId_steer = steering_msg.frame_id
        data_steer = steering_msg.encode({'Steering_Map_Tx':4, 
                                    'Steering_Angle_Demand':angle})
        msg_steer = can.Message(arbitration_id=MsgId_steer,
                                  data=data_steer,
                                  is_extended_id=False)
        try:
            bus.send(msg_brake)
            print("Emergency Braking Message sent on {}".format(bus.channel_info))
        except can.CanError:
            print("Emergency Braking Message NOT sent")
        try:
            bus.send(msg_steer)
            print("Emergency Steering Message sent on {}".format(bus.channel_info))
        except can.CanError:
            print("Emergency Steering Message NOT sent")


    def do11_state_callback(self, do11_state_data):
        rospy.loginfo("Receiving Information")  # Test the info inflow
        self.msg = do11_state_data  # initialize msg as joy data
        #TODO: adjust to the respective value
        if (do11_state_data.data == 100): #value for emergency state

            # Steering from xbox left joystick (1 = full left, -1 = full right)
            self.steer = 0 #self.msg.axes[0]
            angle = int(128 - (self.steer * 100)) #conversion for actuator
            
            # Braking from xbox RT, +1 unactuated to -1 fully depressed
            self.brake = -1#self.msg.axes[5] # right trigger / R2
            self.brake = ((-self.brake) + 1)/2
            pressure = int(550 + (self.brake * 2900)) #conversion for actuator
            self.send_cmd(pressure, angle)

if __name__ == '__main__':
    rospy.init_node('emergency_steer_and_brake')
    C = emergency_cmd()
    r = rospy.Rate(100)
    while not rospy.is_shutdown():
        rospy.spin()
