#!/usr/bin/env python
# coding: utf-8

"""""
Heartbeat: Random number generator sends and checks if the recieved value is the same as sent in the previous message.
Timeouts added if nothing recieved for a predetermined duartion.
"""""

import rospy
import can
import time
from time import sleep
from random import randrange


count = 1
fault_count = 0
fault_threshold = 10
start_flag = 0

def send_initial():
#	bus = can.interface.Bus(bustype='socketcan', channel='vcan0', bitrate=500000)#vcan
	bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=500000) #can
	global sent_num 
	sent_num = 5
	MsgId = 0x11
	msg_beat = can.Message(arbitration_id=MsgId,
	                  data=[5, sent_num, 2, 3, 4, 5, 6, 7], #initialize heartbeat once this message is recieved by the MAB
	                  is_extended_id=False)
	try:
		bus.send(msg_beat)
		print("Initial Message sent on {}".format(bus.channel_info))
		global start_flag
		start_flag = 1
	except can.CanError:
		print("Initial Message NOT sent")
		

	
def random_num():
	return randrange(10)

def send_beat(sent_num, new_rand):
#	bus = can.interface.Bus(bustype='socketcan', channel='vcan0', bitrate=500000)#vcan
	bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=500000) #can
	MsgId = 0x11
	msg_beat = can.Message(arbitration_id=MsgId,
	                  data=[sent_num, new_rand, 2, 3, 4, 5, 6, 7],
	                  is_extended_id=False)
	try:
		bus.send(msg_beat)
		print("Heartbeat Message sent on {}".format(bus.channel_info))
	except can.CanError:
		print("Heartbeat Message NOT sent")

def read():
	global new_rand
	global recieved_num
	bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=500000) #can
#try:
	#msg = bus.recv(None)
	try:
		print("in reading function")
		msg1 = bus.recv(None)
		if msg1.arbitration_id == 0x12: 
#				time_now=time.time()
#				diff_time = start_time - time_now
			new_rand = msg1.data[0]
			recieved_num = msg1.data[1]
			print("read msg")
	except AttributeError:
		print("Nothing received this time")
#except KeyboardInterrupt:
	print("Program Exited")
#	except can.CanError:
#		print("Message NOT sent")
#	return new_rand, old_rand_recieved
	return new_rand, recieved_num

def fault():
#	bus = can.interface.Bus(bustype='socketcan', channel='vcan0', bitrate=500000)#vcan
	bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=500000) #can
	MsgId = 0xFF
	
	msg_fault = can.Message(arbitration_id=MsgId,
	                  data=[255,],
	                  is_extended_id=False)
	try:
		bus.send(msg_fault)
		print("Fault Message sent on {}".format(bus.channel_info))
	except can.CanError:
		print("Fault Message NOT sent")


def nominal():
#	bus = can.interface.Bus(bustype='socketcan', channel='vcan0', bitrate=500000) #vcan
	bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=500000) #can
	start_time = time.time()
	global fault_count
	global sent_num
	global new_rand
#	global recieved_num
	
	try:
#		while fault_count < 30:
		if fault_count < 10:
			print("reading")
			#msg1 = bus.recv(None)
			try:
				#if state == 0 #recieving state
				print("1")
				#new_rand, recieved_num = read() #get message
#				read(msg1)
				msg = bus.recv()
				time_now=time.time()
				diff_time = start_time - time_now
				print("1.5")
				try:
					print("in reading function")
					
					if msg.arbitration_id == 0x12: 
						
						new_rand = msg.data[0]
						recieved_num = msg.data[1]
						print("read msg")
				except AttributeError:
					print("Nothing received this time")
				msg = None
				print("2")
#					send_beat(new_rand)
				if recieved_num != sent_num: #check if message is = previous sent message, else increase the fault count
					fault_count += 1 
				sent_num = random_num() #set and send a random number
				send_beat(sent_num, new_rand) 
				print("fault count", fault_count)
						
			except AttributeError:
				print("Nothing received this time")
	except can.CanError:
		prisnt("Message NOT sent")
		
		
		

if __name__ == '__main__':
	rospy.init_node("heart_beat")
	start_time = time.time()
	r=rospy.Rate(100)
	while not rospy.is_shutdown():
		if start_flag == 1:
			if fault_count >= 30:
				print("fault")
				fault()
			else:
				print("nominal")
				nominal()
		else: #Initiate the MAB heartbeat
			send_initial()
		r.sleep()

