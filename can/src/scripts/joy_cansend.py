#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Joy
from nav_msgs.msg    import Odometry
from gc_control.msg  import VEH
from std_msgs.msg    import Int32, String

import socket
import sys
import struct
import time
import tf
import random

vr=32767
dr=32767
# flag=0
j1=32767
j2=32767
j3=0
j4=0
j5=0
x=0
y=0
z=0
xd=0
yd=0
zd=0
wx=0
wy=0
wz=0

# Maximum and minimum position of the stepper motor (nanotech)
Steer_max =  18000
Steer_min = -18000

# Aurix IP address
host = '10.42.0.146'
port = 50000
source_port = 0xEBEB
curr_state = None
# Published topics
pub_odom  = rospy.Publisher('/curtis/odometry',   Odometry, queue_size=10)
pub_steer = rospy.Publisher('/nanotech/steerPos', Int32,    queue_size=10) # -18000 left to +18000 right

# Socket creation
try:
    # AF_INIT:    specifies the socket family
    # SOCK_DGRAM: specifies the UDP protocol used, in this case datagram-based protocol
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    
    s.bind(('0.0.0.0', source_port))
    
    print 'socket created'
    
except socket.error:
    print 'Failed to create socket'
    sys.exit()

def send_msg(dst, src, app_selector, msg_id, payload):
    # length of the header + length of the payload
    length = 16 + len(payload)

    # time stamp gets ignored at the moment 
    timestamp = 0   

    # https://docs.python.org/2/library/struct.html
    as_header = struct.pack('>II', 33, length)
    msg_header = struct.pack('BBHIII', dst, src, length, timestamp, app_selector, msg_id)

    # combine message header and payload
    msg = as_header + msg_header + payload

    #send the message
    bt = s.sendto(msg, (host, port))


def callback(data):
    global j1
    global j2
    global j3  
    global j4
    global j5

    # Receiving data from joystick
    j1=(data.axes[3]+1)*32767
    j2=(data.axes[1]+1)*32767
    j3=data.buttons[1]
    j4=data.buttons[2]
    j5=data.buttons[3]

def receive_callback(event):
    try :
        # receive data from Aurix
	# packet dimension of 2^10 bytes (rec contains the data received, while addr is the socket address)
        rec, addr = s.recvfrom(1024)

        AsPduId, AsPduLen = struct.unpack('>II', rec[:8])
        Dst, Src, Len, Timestamp, AppSelector, MsgId = struct.unpack('<BBHIII', rec[8:24])
        
        #print("Aurix MsgId: ", hex(MsgId))
			
	# Message id from Aurix
        if(MsgId == 0x103):
            for i in xrange(24, len(rec), 20):   
		CanId, CrtlId, CanLen, TimeStamp = struct.unpack('>IBBQ', rec[i:i+14])
		#print ("can ID :",hex(CanId))
		if hex(CanId)=='0x227':
                    Speed,Current, Regen_Flag, Scrap = struct.unpack('<hhBB', rec[i+14:i+20])
		    odom=Odometry()
                    odom.header.stamp = rospy.Time.now()
                    odom.header.frame_id = 'base_link'
		    #odom.twist.twist.linear.x=Speed*(2*3.14/60)*(1/12.3)*(0.22) 
		    odom.twist.twist.linear.x=Speed*(2*3.14/60)*(1/12.3)*(0.2075)
		    
		    # Added the covariance on longitudinal speed. Computed experimentally 
		    odom.twist.covariance[0] = 0.001
                    pub_odom.publish(odom)
                    
                if hex(CanId) == '0x400':
                    # var has been added so that the whole 6 bytes can be read, without changing the index
                    SteerPos, var = struct.unpack('<ih', rec[i+14:i+20])
                    
                    # Saturations on the steering position
                    if(SteerPos > Steer_max):
                    	SteerPos = Steer_max
            	    if(SteerPos < Steer_min):
            	    	SteerPos = Steer_min
            	    
            	    pub_steer.publish(SteerPos)               
        
    except socket.error, msg:
        print 'Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()

def send_A_callback(event):
    
    str_msg=struct.pack('<HHBBB',j1,j2,j3,j4,j5)
    send_msg(4, 1, 0x14, 0x681, str_msg)
    #print(1)

def send_B_callback(event):

    str_msg=struct.pack('<HHH',random.randrange(0,10,1),y,z)
    send_msg(4, 1, 0x14, 0x666, str_msg)
    #print(2)

def send_C_callback(event):
    
    str_msg=struct.pack('<HHH',xd,yd,zd)
    send_msg(4, 1, 0x14, 0x667, str_msg)
    #print(3)

def send_D_callback(event):

    str_msg=struct.pack('<HHH',wx,wy,wz)
    send_msg(4, 1, 0x14, 0x668, str_msg)
    #print(4)

def path_tracking_callback(data):
    global vr, dr, j1
    
    vr=int((data.speed+1)*32767)
    dr=int((data.steering+1)*32767)

    #print(data.flag,data.speed,"Stanley steer :",dr," joy steer :",j1)

def path_tracking_timer(event):
#     global flag
    if(curr_state == "RemoteControlManager"):
        flag = 0
    else:
        flag = 1

    print("steer ",dr/32767.0-1," speed " ,vr/32767.0-1," auto? ",flag, " curr state ", curr_state)

    str_msg=struct.pack('<HHB',dr,vr,flag)
    send_msg(4, 1, 0x14, 0x669, str_msg)
    
def set_curr_state(msg):
    global curr_state
    curr_state = msg.data

def listener():

    rospy.init_node('joy_cansend') #, anonymous=True)

    rospy.Timer(rospy.Duration(0.005),receive_callback)
    time.sleep(0.01)
    rospy.Timer(rospy.Duration(0.05),send_A_callback)
    time.sleep(0.01)
    rospy.Timer(rospy.Duration(0.05),path_tracking_timer)


    #time.sleep(0.008)
    #rospy.Timer(rospy.Duration(0.04),send_C_callback)
    #time.sleep(0.008)
    #rospy.Timer(rospy.Duration(0.04),send_D_callback)

    rospy.Subscriber("joy", Joy, callback)

    rospy.Subscriber('veh_control_ref', VEH, path_tracking_callback) #data has speed, steering and flag, all always 0
    
    behavior_top = '/behavior/behavior_info'
    behavior_sub = rospy.Subscriber(behavior_top, String, set_curr_state, queue_size=1)

    rospy.spin()

if __name__ == '__main__':
    listener()

