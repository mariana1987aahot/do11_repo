#!/usr/bin/env python
# coding: utf-8
##todo:count messages in 1 sec and print freq
##read and reciprocate (heartbeat), later put a timeout
##HB: recieve a message with id 11, check and send hb wit0x2
"""
This example shows how sending a single message works.
"""

#$ modprobe can_dev
#$ modprobe can
#$ modprobe can_raw
#  sudo ip link add dev vcan0 type vcan
#  sudo ip link set vcan0 type vcan
#  sudo ip link set up vcan0

# sudo ip link set can0 type can bitrate 500000
# sudo ip link set up can0

from __future__ import print_function
import can
import cantools
import sys
import rospy
from std_msgs.msg import Int32MultiArray


#load dbc and read message
db = cantools.db.load_file('/home/ardashir/do11_workspace/src/do11_repo/can/data/Display_Test1.dbc')
message = db.messages[0]

#setup the publishers
pub_ept_stat = rospy.Publisher('ept_stats', Int32MultiArray, queue_size=10)

#bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=500000) #can
bus = can.interface.Bus(bustype='socketcan', channel='vcan0', bitrate=500000)#vcan


def read_one():
	message = bus.recv()
	dict = {}
	dict = db.decode_message(message.arbitration_id, message.data)
	print (dict['Battery_Temp'])
	data = Int32MultiArray()
	temp_list = [dict['Battery_SOC'], dict['Battery_Temp'], 
	dict['Inverter_Temperature'], dict['Pump_RPM'], dict['Mode'], dict['Fault_Status']]
	data.data = temp_list

	pub_ept_stat.publish(data)


if __name__ == "__main__":
	rospy.init_node('can_reader')
	r = rospy.Rate(20)
	while not rospy.is_shutdown():
		read_one()
		r.sleep()

