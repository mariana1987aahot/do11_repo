#!/usr/bin/env python

"""
ROS to CAN conversion of messages for showboat functions
Input from do11_state
Output to MAB via CAN
"""

import rospy
import can
import cantools
import sys
import numpy as np
#import message_filters
from message_filters import ApproximateTimeSynchronizer
from std_msgs.msg import Int32, Int32MultiArray
from autoware_msgs.msg import ControlCommand

# Virtual can setup
# sudo ip link add dev vcan0 type vcan
# sudo ip link set vcan0 type vcan
# sudo ip link set up vcan0

# Setup CAN with hardware (Kvaser leaf light USB CAN converter)
# sudo ip link set can0 type can bitrate 500000
# sudo ip link set up can0

# Load the dbc files and find the messages we need to send
db_ept = cantools.db.load_file('/home/deeporange/do11_workspace/src/do11_repo/can/src/scripts/dbc_files/Nuevo_MAB.dbc')

showboat_msg = db_ept.get_message_by_name('Showboat')
state_msg = db_ept.get_message_by_name('Control_Commands')#TODO
mab_nuvo_msg = db_ept.get_message_by_name('Autonomy_Feedback')
display_msg =  db_ept.get_message_by_name('Display_Test')

# Setup the bus
bus = can.interface.Bus(bustype='socketcan',
                                channel='can0', bitrate=500000)
# Setup the publishers
pub_showboat_feedback = rospy.Publisher('ept_in/showboat_feedback', Int32, queue_size=10)
pub_sys_state = rospy.Publisher('ept_in/sys_state', Int32, queue_size=10)
pub_ept_stat = rospy.Publisher('ept_stats', Int32MultiArray, queue_size=10)

# Declare the global variables and
state_message = Int32()
ctrl_cmd_message = ControlCommand()

class conversion:
    def __init__(self):
        self.sub_do11_state = rospy.Subscriber('/do11_state', Int32, self.callback_state)
        self.sub_final_cmd = rospy.Subscriber('/final_vehicle_cmd', ControlCommand, self.callback_cmd)
        self.timer = rospy.Timer(rospy.Duration(0.1), self.demo_callback)

    def read_one(self):
#        try:
        message = bus.recv(0.1)#TODO reduce to 0.01
        if message is not None and message.arbitration_id==0x26:
#          print("here")
            dict = {}
            dict = db_ept.decode_message(message.arbitration_id, message.data)
#            print("here")
            # Initialize datatypes and define them as the incoming signals
            feedback_signal = Int32()
            feedback_signal.data = dict['SB_Feedback']
            sys_state_signal = Int32()
            sys_state_signal.data = dict['System_State']
#            print("here")
            # Publish the data over ros
            pub_showboat_feedback.publish(feedback_signal)
            pub_sys_state.publish(sys_state_signal)
#          print("here")
#        if message is None:
#            print("Main message not recieved this time")

#----------------------------------------------------

        if message is not None and message.arbitration_id==0x7ab:
#          print("here")
            dict = {}
            dict = db_ept.decode_message(message.arbitration_id, message.data)
            print (dict['Battery_SOC'])
            print (dict['Battery_Temp'])
            print (dict['Inverter_Temperature'])
            print (dict['Pump_RPM'])
            print (dict['Mode'])
            print (dict['Fault_Status'])
            display_data = Int32MultiArray()
            signal_list = [dict['Battery_SOC'], 
                         dict['Battery_Temp'], 
                         dict['Inverter_Temperature'], 
                         dict['Pump_RPM'], 
                         dict['Mode'], 
                         dict['Fault_Status']]
            display_data.data = signal_list
            pub_ept_stat.publish(display_data)
        if message is None:
            print("Display message not received this time")
        message = None
        dict={}

    def send_msg(self, signals):
        MsgId = showboat_msg.frame_id
        #Convert numpy int64 to int
        data = showboat_msg.encode({ 'Five_Seat': signals[0].item(),
                                     'Six_Seat':  signals[1].item(),
                                     'Ramp_In':   signals[2].item(),
                                     'Ramp_Out':  signals[3].item(),
                                     'Door_Close':signals[4].item(),
                                     'Door_Open': signals[5].item(),
                                     'WC_Fold':   signals[6].item(),
                                     'WC_Unfold': signals[7].item()
                                     })
        msg_showboat = can.Message(arbitration_id=MsgId,
                                  data=data,
                                  is_extended_id=False)
        try:
            bus.send(msg_showboat)
            print("Showboat Message sent on {}".format(bus.channel_info))
        except can.CanError:
            print("Showboat Message NOT sent")

    def send_ctrl_cmds(self, state_value, ctrl_cmds):
        brake = ctrl_cmds.linear_acceleration
        brake1 = ((-brake) + 1)/2
        pressure = int(550 + (brake1 * 2900))
        print("Brake = ",pressure)
        
        steer = ctrl_cmds.steering_angle
        steer = int(128 - (steer * 100))
        print("Steering = ",steer)
        
        vel = ctrl_cmds.linear_velocity
        vel = ((vel) + 1)/2
        vel = vel *250
        print("Velocity = ",vel)
        MsgId = state_msg.frame_id
        data = state_msg.encode({ 'Required_Brake': pressure,
                                  'Required_Steering': steer,
                                  'Required_Velocity': int(vel),
                                  'System_Mode':  state_value
                                })
        msg_state = can.Message(arbitration_id=MsgId,
                                  data=data,
                                  is_extended_id=False)
        try:
            bus.send(msg_state)
            print("State Message sent on {}".format(bus.channel_info))
        except can.CanError:
            print("State Message NOT sent")

    def process_info_showroom(self,state):
    # Vector for sending can msg, message as given below
    # [five seat, six seat, ramp in, ramp out, door close, door_open, wc fold, wc unfold]
        if state == 50: #showroom mode
            self.send_msg(np.array([0,0,0,0,0,0,0,0]))
        elif state == 51: #five seat
            self.send_msg(np.array([1,0,0,0,0,0,0,0]))
        elif state == 52: #six seat
            self.send_msg(np.array([0,1,0,0,0,0,0,0]))
        elif state == 53: #ramp in
            self.send_msg(np.array([0,0,1,0,0,0,0,0]))
        elif state == 54: #ramp out
            self.send_msg(np.array([0,0,0,1,0,0,0,0]))
        elif state == 55: #door close
            self.send_msg(np.array([0,0,0,0,1,0,0,0]))
        elif state == 56: #door open
            self.send_msg(np.array([0,0,0,0,0,1,0,0]))
        elif state == 57: #wheelchair fold
            self.send_msg(np.array([0,0,0,0,0,0,1,0]))
        elif state == 58: #wheelchair unfold
            self.send_msg(np.array([0,0,0,0,0,0,0,1]))
        elif state == 70: #door open
            self.send_msg(np.array([0,0,0,0,0,0,0,0]))
        elif state == 71: #wheelchair fold
            self.send_msg(np.array([0,0,0,0,0,0,0,0]))
        elif state == 72: #wheelchair unfold
            self.send_msg(np.array([0,0,0,0,0,0,0,0]))
        else:return

    def callback_state(self,state_data):
        global state_message
        state_message = state_data
    def callback_cmd(self,ctrl_cmd_data):
        global ctrl_cmd_message
        ctrl_cmd_message = ctrl_cmd_data

    def demo_callback(self, timer):
        global state_message
        global ctrl_cmd_message
        if state_message != None and ctrl_cmd_message != None:
            rospy.loginfo("Receiving Topics")  # Test the info inflow
            state = state_message.data
            self.process_info_showroom(state)
            self.send_ctrl_cmds(state, ctrl_cmd_message)
            self.read_one()
#            print("here")
#        clear global vars
            state_message = None
            ctrl_cmd_message = None
        else:
            print("Topics not synced")

#        print("here2")

if __name__ == '__main__':
    rospy.init_node('ros_to_can')
    C = conversion()
    r = rospy.Rate(10)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
