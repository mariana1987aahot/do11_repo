#!/usr/bin/env python

"""
Convert joy msgs to can for steering DCE motor
Used PEP8 style guide
"""

import rospy
import can
from sensor_msgs.msg import Joy


class joy_can_steer:
    def __init__(self):
        self.msg = Joy()
        self.steer_sub = rospy.Subscriber('/joy_orig', Joy, self.joy_callback)
    
    def send_steer(self, angle):
        bus = can.interface.Bus(bustype='socketcan',
                                channel='can0', bitrate=500000)  # can
        MsgId = 0x298
        msg_beat = can.Message(arbitration_id=MsgId,
                                  data=[4, angle, 0, 0, 0, 0, 0, 0],
                                  is_extended_id=False)
        try:
            bus.send(msg_beat)
            print("Steering Message sent on {}".format(bus.channel_info))
        except can.CanError:
            print("Steering Message NOT sent")


    def joy_callback(self, joy_data):
        rospy.loginfo("Receiving Information")  # Test the info inflow
        self.msg = joy_data  # initialize msg as joy data
        # Steering from xbox left joystick (1 = full left, -1 = full right)
        self.steer = self.msg.axes[0]
        angle = int(133 - (self.steer * 90))
        self.send_steer(angle)
        rospy.loginfo("Publishing Information")  # Test the info inflow

    

if __name__ == '__main__':
    rospy.init_node('joy_can_dce_steer')
    C = joy_can_steer()
    r = rospy.Rate(100)
    while not rospy.is_shutdown():
        rospy.spin()
