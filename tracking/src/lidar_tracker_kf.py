import rospy
import numpy as np
from visualization_msgs.msg import Marker
from KF_lidar import KalmanFilter
from autoware_msgs.msg import Centroids
from threading import Lock

class Tracker(object):   

     def __init__(self):

        self.kf = KalmanFilter(0.1)
        self.lidar_sub = rospy.Subscriber('/cluster_centroids',Centroids,self.lidar_tracker)
        


   #   def data_association(self,data):
        
   #      prev_id = {}
   #      current_id = {}
   #      for j in range(len(data.points)):

   #          current_id[i] = (data.points[i].x,data.points[i].y)
             
        
     def lidar_tracker(self,data):

         marker_pub = rospy.Publisher('/tracked_object',Marker,queue_size=10)
         self.kf.predict()
         self.kf.update(np.array([data.points[0].x, data.points[0].y]))
         state = self.kf.getx()
         # rospy.loginfo(state)
         rospy.loginfo((data.points[1].x,data.points[1].y,state))
         msg = Marker()
         msg.header.frame_id = 'velodyne'
         msg.header.stamp = rospy.Time()
         msg.type = msg.SPHERE
         msg.ns = "tracked_objects"
         msg.id = 1
         msg.action = msg.ADD
         msg.pose.position.x = state[0] 
         msg.pose.position.y = state[1] 
         msg.pose.position.z = 0
         msg.pose.orientation.x = 0.0
         msg.pose.orientation.y = 0.0
         msg.pose.orientation.z = 0.0
         msg.pose.orientation.w = 1.0
         msg.scale.x = 0.5
         msg.scale.y = 0.5
         msg.scale.z = 0.5
         msg.color.a = 1.0 
         msg.color.r = 1.0
         msg.color.g = 0.0
         msg.color.b = 0.0
         marker_pub.publish(msg)
        
       
         # first define time interval 
         # then define the 

if __name__ =='__main__':
    rospy.init_node('Tracker',anonymous=True)
    Tracker()
    rospy.loginfo('starting node')
    rospy.Rate(10)
    rospy.spin()   
    
        

        



        
        
        









    
      

      
       
         

     






    





    

    