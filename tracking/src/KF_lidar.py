import numpy as np
#from threading import Lock

class KalmanFilter:
  """
    A class that predicts the next state of the system given sensor measurements 
    using the Kalman Filter algorithm 
  """

  def __init__(self,dt):
    #self.lock = Lock()
    self.dt = dt
    self.Lxd = 0.1
    self.Lyd = 0.1
    self.I = np.eye(4)
    self.x = np.array([-2,-2,1,1])
    self.P = 0.0001*np.eye(4)
    self.F = np.eye(4)
    self.F[0][2] = self.dt 
    self.F[1][3] = self.dt 
    self.dt4 = pow(self.dt,4)
    self.dt3 = pow(self.dt,3)
    self.dt2 = pow(self.dt,2) 
    self.Q = np.eye(4)
    self.Q[0][0] = (self.dt4*self.Lxd)/4
    self.Q[0][2] = (self.dt3*self.Lxd)/2
    self.Q[1][1] = (self.dt4*self.Lyd)/4
    self.Q[1][3] = (self.dt3*self.Lyd)/2
    self.Q[2][0] = (self.dt3*self.Lxd)/2
    self.Q[2][2] = (self.dt2*self.Lxd)
    self.Q[3][1] = (self.dt3*self.Lyd)/2
    self.Q[3][3] = (self.dt2*self.Lyd)
    self.R = np.eye(2)
    self.R[0][0] = 0.15 #process noise for lidar x
    self.R[1][1] = 0.15 # process noise for lidar y
    self.H = np.zeros((2,4))
    self.H[0][0] = 1
    self.H[1][1] = 1 
        
  def setQ(self, Q):
    self.Q = Q

  def updateF(self, dt):
    self.F[0, 2], self.F[1, 3]  = dt, dt

 
  def getx(self):
    #print(self.x)
    return self.x

  def predict(self):
    #self.lock.acquire()
    self.x = np.dot(self.F,self.x)
    self.P = np.dot(self.F,np.dot(self.P , (self.F).T)) + self.Q
    #self.lock.release()

  def update(self, z):
    #self.lock.acquire()
    y = z - np.dot(self.H,self.x)
    PHt = np.dot(self.P ,self.H.T)
    S = np.dot(self.H ,PHt) + self.R    
    K = np.dot(PHt,np.linalg.inv(S))
    self.x = self.x + np.dot(K,y)
    self.P = np.dot((self.I - np.dot(K,self.H)),self.P)
    #self.lock.release()
    #print(self.x)
  
# if __name__ == '__main__':
    
#     kf = KalmanFilter(1)
      
#     for i in range(0,100):
        
#         kf.predict()
#         kf.update(np.array([i,2*i]))


    
    
