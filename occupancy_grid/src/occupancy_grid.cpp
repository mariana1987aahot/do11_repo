#include "occupancy_grid/occupancy_grid.h"

OccupancyGrid::OccupancyGrid() : nh_(""), pnh_("~")
{
  // Add rosparamters here
  pnh_.param("laser_scan_topic", laser_scan_topic_, std::string("/scan"));
  pnh_.param("vector_map_topic", vector_map_topic_, std::string("/vector_map"));
  pnh_.param("occupancy_frame", occupancy_frame_, std::string("/velodyne"));
  pnh_.param("vector_map_frame", vector_map_frame_, std::string("/map"));
  pnh_.param("map_width", map_width_, int(56));
  pnh_.param("map_height", map_height_, int(56));
  pnh_.param("resolution", resolution_, double(0.5));
  
  //  Add subscribers here
  laser_scan_sub_ =  nh_.subscribe(laser_scan_topic_, 1000, &OccupancyGrid::laserscanCallback, this);
  vector_map_sub_ =  nh_.subscribe(vector_map_topic_, 1000, &OccupancyGrid::vectormapCallback,this);

  //  Add publishers here
  occupancy_map_pub_ = nh_.advertise<nav_msgs::OccupancyGrid>("/occupancy_map", 10);
  
  //  Add TimerEvent
  timer = nh_.createTimer(ros::Duration(0.05), &OccupancyGrid::createOccupancyGrid, this);
  ROS_INFO("Creating Occupancy Grid");
};

OccupancyGrid::~OccupancyGrid(){};


void OccupancyGrid::createOccupancyGrid(const ros::TimerEvent&)
{
  if(current_laserscan_ && current_vector_map_)
  {
    nav_msgs::OccupancyGrid Occupancy;
    Occupancy.header.frame_id = occupancy_frame_;
    Occupancy.info.resolution = resolution_;
    Occupancy.info.width = int(map_width_/resolution_);
    Occupancy.info.height = int(map_height_/resolution_);
    Occupancy.info.origin.position.x = -map_width_/2;
    Occupancy.info.origin.position.y = -map_height_/2;
    Occupancy.info.origin.position.z = 0;
    Occupancy.info.origin.orientation.x = 0;
    Occupancy.info.origin.orientation.y = 0;
    Occupancy.info.origin.orientation.z = 0;
    Occupancy.info.origin.orientation.w = 1;
    
    int n = Occupancy.info.width *Occupancy.info.height;
    array = new int[n];
    for(int i = 0; i<n; i++)
    {
      array[i] = 0;
    }
    
    laserscanToGrid();
    curbsToGrid();
  
    for(int x = 0; x< n; x++)
    {
      occupancy_array.data.push_back(array[x]);
    }

    Occupancy.data = occupancy_array.data;
    occupancy_map_pub_.publish(Occupancy);
    ROS_INFO("Published OccupancyGrid");
    occupancy_array.data.clear();
    delete [] array;
    array = NULL;
  }
  else
    ROS_INFO("Waiting for LaserScan or VectorMap Data");

}

void OccupancyGrid::laserscanToGrid()
{
  float polar_angle, x, y;
  int count{0};
  double inf = std::numeric_limits<double>::infinity();
  for(auto range: current_laserscan_->ranges)
  {
    polar_angle = current_laserscan_->angle_min + (count*current_laserscan_->angle_increment);
    x = range*cos(polar_angle);
    y = range*sin(polar_angle);
    if(insideBoundaries(x,y))
    {
      int x1 = (int)(x/resolution_)+map_width_;
      int y1 = (int)(y/resolution_)+map_height_;
      int position = x1+map_width_/resolution_*y1;
      array[position] = 50;
    }
    count++;
  }
}

void OccupancyGrid::curbsToGrid()
{
  for(auto curb: current_vector_map_->markers)
    {
      for(auto point: curb.points)
      {
        auto current_x = point.x;
        auto current_y = point.y;
        geometry_msgs::PoseStamped waypoint = createNewWaypoint(current_x, current_y, vector_map_frame_); 
        geometry_msgs::Pose transformed_pose;
        //we now have transformed pose in velodyne frames for the curb point
        transformed_pose = lookupTF(waypoint, occupancy_frame_, vector_map_frame_);
        auto x = transformed_pose.position.x;
        auto y = transformed_pose.position.y;
        if(insideBoundaries(x,y))
        {
          int x1 = int(x/resolution_)+map_width_;
          int y1 = int(y/resolution_)+map_height_;
          int position = x1+map_width_/resolution_*y1;
          array[position] = 50;
        }
      }
    }
}


bool OccupancyGrid::insideBoundaries(float x, float y)
{
  double inf = std::numeric_limits<double>::infinity();
  if(x >= -map_width_/2 && x <= map_width_/2 && x != inf && x!= -inf)
  {
    if(y >= -map_height_/2 && y <= map_height_/2 && y != inf && y != -inf)
      return true;
    else
      return false;
  }
  else
    return false;
}

geometry_msgs::PoseStamped OccupancyGrid::createNewWaypoint(double current_x, double current_y, std::string frame)
{
    geometry_msgs::PoseStamped waypoint;
    waypoint.header.frame_id = frame; //"map"
    waypoint.pose.position.x = current_x;
    waypoint.pose.position.y = current_y;
    waypoint.pose.position.z = 0;
    waypoint.pose.orientation.x = 0.0;
    waypoint.pose.orientation.y = 0.0;
    waypoint.pose.orientation.z = 0.0;
    waypoint.pose.orientation.w = 1.0;
    return waypoint;
}

//Generic lookup transform function
geometry_msgs::Pose OccupancyGrid::lookupTF(geometry_msgs::PoseStamped initial_pt, std::string destination, std::string original)
{
    geometry_msgs::Pose transformed_pose;  
    tf::Transform dest_to_orig;
    tf::poseMsgToTF(initial_pt.pose, dest_to_orig);
    tf::StampedTransform req_to_dest;
    try
    {
      listener_.lookupTransform(destination, original, ros::Time(0), req_to_dest);
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }
    tf::Transform req_to_orig;
    req_to_orig = req_to_dest * dest_to_orig;
    tf::poseTFToMsg(req_to_orig, transformed_pose);
    
    return transformed_pose;
}

//Add Callbacks here
void OccupancyGrid::laserscanCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
  current_laserscan_ = std::make_shared<sensor_msgs::LaserScan>(*msg);
}
void OccupancyGrid::vectormapCallback( const visualization_msgs::MarkerArray::ConstPtr& msg)
{
  current_vector_map_ = std::make_shared<visualization_msgs::MarkerArray>(*msg);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "occupancy_grid");
  OccupancyGrid obj;
  ros::spin();
  return 0;
};
