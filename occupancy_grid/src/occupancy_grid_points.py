#!/usr/bin/env python
import math
import rospy
import numpy as np
from nav_msgs.msg import OccupancyGrid
from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2 as pc2

class Costmap(object):
    # initializing subscribers and callbacks
    def __init__(self):
        
       self.lidar_sub = rospy.Subscriber('/velodyne_points',PointCloud2,self.occupancy_callback)


    def pointcloud_to_Grid(self,ros_cloud):
     
     points_list = []

     for data in pc2.read_points(ros_cloud, skip_nans=True):
        #if (data[2]>-5 and data[2]<0):
        points_list.append((data[0], data[1]))

     return points_list 


    def occupancy_callback(self,data):

       #get occupancy area from bounding box in cloudclusterarray
       msg = OccupancyGrid()
       msg.header.frame_id = 'velodyne'
       msg.info.resolution = 1
       msg.info.width = int(56/msg.info.resolution)
       msg.info.height =int(56/msg.info.resolution)
       msg.info.origin.position.x = -msg.info.width/2
       msg.info.origin.position.y = -msg.info.height/2
       msg.info.origin.position.z = 0
       msg.info.origin.orientation.x = 0
       msg.info.origin.orientation.y = 0
       msg.info.origin.orientation.z = 0
       msg.info.origin.orientation.w = 1
       grid_array = self.pointcloud_to_Grid(data)
       array = [0 for k in range(msg.info.width*msg.info.height)]
       for i in range(len(grid_array)): # loop thorugh the points
          if((grid_array[i][0])>=-msg.info.width/2 and (grid_array[i][0])<=msg.info.width/2 and grid_array[i][0]!= float('inf') and grid_array[i][0]!= float('-inf')):
             if((grid_array[i][1])>=-msg.info.height/2 and (grid_array[i][1])<=msg.info.height/2 and grid_array[i][1]!= float('inf') and grid_array[i][1]!= float('-inf')):# map constraints
                x_index = int(grid_array[i][0]) + msg.info.width/2
                y_index = int(grid_array[i][1]) + msg.info.height/2
                array[x_index + msg.info.width*y_index] = 50
       msg.data = array
       rospy.loginfo('Occupancy grid populated')
       map_pub = rospy.Publisher('/occupancy_map',OccupancyGrid,queue_size=10)
       map_pub.publish(msg)

if __name__ == '__main__':
    rospy.init_node('occupancy_grid', anonymous=True)
    rospy.loginfo('Occupancy grid running')
    Costmap()
    rospy.spin()
