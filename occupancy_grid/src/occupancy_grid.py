#!/usr/bin/env python

import cv2
import math
import rospy
import numpy as np
from nav_msgs.msg import OccupancyGrid,GridCells
from geometry_msgs.msg import Point,Pose,PoseStamped
import sensor_msgs.point_cloud2 as pc2
#import pcl
#import pcl.pcl_visualization
from visualization_msgs.msg import MarkerArray,Marker
from sensor_msgs.msg import PointCloud2,Image,LaserScan
from autoware_msgs.msg import PointsImage,DetectedObjectArray,Centroids,CloudClusterArray
from tf import TransformListener

# testing git
# construct based on OOP
class Costmap(object):
    # initializing subscribers and callbacks
    # spceify map width and height in metres
    def __init__(self,width,height,resolution):
       self.tf = TransformListener();
       #self.lidar_sub = rospy.Subscriber('/points_cluster',PointCloud2,self.cluster_occupancy_callback)
       #self.cluster_grid_sub = rospy.Subscriber('/detection/lidar_detector/cloud_clusters',CloudClusterArray,self.cluster_occupancy_callback)
       #self.marker_sub = rospy.Subscriber('/detection/lidar_detector/cloud_clusters',CloudClusterArray,self.bounding_box_callback)
       #self.visualizer = pcl.pcl_visualization.CloudViewing()
       self.grid_sub = rospy.Subscriber('/scan',LaserScan,self.laserscan_occupancy_callback)
       self.map_width = width # in metres
       self.map_height = height # in metres
       self.map_resolution = resolution # in metres/cell
    # function to convert pointcloud in ros to pcl object
#    def ros_to_pcl(self,ros_cloud):
#
#     points_list = []
#
#     for data in pc2.read_points(ros_cloud, skip_nans=True):
#        points_list.append([data[0], data[1], data[2], data[3]])

#     pcl_data = pcl.PointCloud_PointXYZRGB()
#     pcl_data.from_list(points_list)

#     return pcl_data
    # need to shorten the data first - all clusters hamper performance and is not even necessary
    # function to convert 2d scan data to 2d grid
    def LaserScan_to_Grid(self,scan_data):
      XY_data = []
      count = 0
      for i in range(len(scan_data.ranges)):
         polar_angle = scan_data.angle_min + (i*scan_data.angle_increment)
         x_cord = scan_data.ranges[i]*math.cos(polar_angle)
         y_cord = scan_data.ranges[i]*math.sin(polar_angle)
         count = count + 1
         XY_data.append((x_cord,y_cord))

#      rospy.loginfo("count = %d",count )
#      count = 0

      p1 = PoseStamped()
      p1.header.frame_id = "map"
      p1.pose.orientation.w = 1.0
      for curb in curb_data.markers:
        for point in curb.points:
          p1.pose.position.x = point.x
          p1.pose.position.y = point.y
          p_in_velodyne = self.tf.transformPose("/velodyne",p1)
          XY_data.append((p_in_velodyne.pose.position.x,p_in_velodyne.pose.position.y))
#          rospy.loginfo("p_in_velodyne = %f",p_in_velodyne.pose.position.x )
          count = count + 1          
      rospy.loginfo("count = %d",count )
      rospy.loginfo("length = %d", len(XY_data))
      return XY_data


#    def Cluster_to_Grid(self,data):
#      XY_data = []
#      for i in range(len(scan_data.ranges)):
#         polar_angle = scan_data.angle_min + (i*scan_data.angle_increment)
#         x_cord = scan_data.ranges[i]*math.cos(polar_angle)
#         y_cord = scan_data.ranges[i]*math.sin(polar_angle)
#         XY_data.append((x_cord,y_cord))
#      return XY_data



    #function to visualize bounding box
#    def bounding_box_callback(self,data):
#      markers = []
#      for i in range(len(data.clusters)):
#        msg = Marker()
#        msg.header.frame_id = 'velodyne'
#        msg.header.stamp = rospy.Time()
#        msg.type = msg.CUBE
#        msg.ns = "objects"
#        msg.id = i
#        msg.action = msg.ADD
#        msg.pose.position.x = data.clusters[i].bounding_box.pose.position.x
#        msg.pose.position.y = data.clusters[i].bounding_box.pose.position.y
#        msg.pose.position.z = data.clusters[i].bounding_box.pose.position.z
#        msg.pose.orientation.x = 0.0
#        msg.pose.orientation.y = 0.0
#        msg.pose.orientation.z = 0.0
#        msg.pose.orientation.w = 1.0
#        msg.scale.x = data.clusters[i].bounding_box.dimensions.x
#        msg.scale.y = data.clusters[i].bounding_box.dimensions.y
#        msg.scale.z = data.clusters[i].bounding_box.dimensions.z
#        msg.color.a = 0.5
#        msg.color.r = 1.0
#        msg.color.g = 0.0
#        msg.color.b = 0.0
#        markers.append(msg)
#      obj_pub = rospy.Publisher('/cube_marker',MarkerArray,queue_size=10)
#      obj_pub.publish(markers)

    # function to plot occupancy grid
    def laserscan_occupancy_callback(self,data):

       #get occupancy area from bounding box in cloudclusterarray
       msg = OccupancyGrid()
       msg.header.frame_id = 'velodyne'
       msg.info.resolution = self.map_resolution
       msg.info.width = int(self.map_width/msg.info.resolution)
       msg.info.height =int(self.map_height/msg.info.resolution)
       msg.info.origin.position.x = -self.map_width/2#-msg.info.width/2 has to be in metres
       msg.info.origin.position.y = -self.map_height/2#-msg.info.height/2
       msg.info.origin.position.z = 0
       msg.info.origin.orientation.x = 0
       msg.info.origin.orientation.y = 0
       msg.info.origin.orientation.z = 0
       msg.info.origin.orientation.w = 1
#       curb_data = rospy.wait_for_message('/vector_map',MarkerArray)
#       rospy.loginfo("curb_data = %d ",curb_data.markers[0].points[0].x )
       grid_array = self.LaserScan_to_Grid(data)
       array = [0 for k in range(msg.info.width*msg.info.height)]
       for i in range(len(grid_array)): # loop thorugh the points
          if((grid_array[i][0])>=-self.map_width/2 and (grid_array[i][0])<=self.map_width/2 and grid_array[i][0]!= float('inf') and grid_array[i][0]!= float('-inf')):
             if((grid_array[i][1])>=-self.map_height/2 and (grid_array[i][1])<=self.map_height/2 and grid_array[i][1]!= float('inf') and grid_array[i][1]!= float('-inf')):# map constraints
                x_index = int(grid_array[i][0]/msg.info.resolution) + msg.info.width/2 # this is in cell index 
                y_index = int(grid_array[i][1]/msg.info.resolution) + msg.info.height/2  
                array[x_index + msg.info.width*y_index] = 50
       msg.data = array
       rospy.loginfo('Occupancy grid populated')
       map_pub = rospy.Publisher('/occupancy_map',OccupancyGrid,queue_size=10)
       map_pub.publish(msg)


    # def cluster_occupancy_callback(self,data):

    #     #get occupancy area from bounding box in cloudclusterarray
    #     msg = OccupancyGrid()
    #     msg.header.frame_id = 'velodyne'
    #     msg.info.resolution = 1
    #     msg.info.width = 50
    #     msg.info.height =  50
    #     msg.info.origin.position.x = -msg.info.width/2
    #     msg.info.origin.position.y = -msg.info.height/2
    #     msg.info.origin.position.z = 0
    #     msg.info.origin.orientation.x = 0
    #     msg.info.origin.orientation.y = 0
    #     msg.info.origin.orientation.z = 0
    #     msg.info.origin.orientation.w = 1
    #     grid_array = self.LaserScan_to_Grid(data)
    #     array = [0 for k in range(msg.info.width*msg.info.height)]
    #     for i in range(len(grid_array)): # loop thorugh the points
    #        if((grid_array[i][0])>=-msg.info.width/2 and (grid_array[i][0]<msg.info.width/2) and grid_array[i][0]!= float('inf') and grid_array[i][0]!= float('-inf')):
    #           if((grid_array[i][1])>=-msg.info.width/2 and (grid_array[i][1])<msg.info.width/2 and grid_array[i][1]!= float('inf') and grid_array[i][1]!= float('-inf')):# map constraints
    #              x_index = int(grid_array[i][0])
    #              y_index = int(grid_array[i][1])
    #              array[x_index + msg.info.width*y_index] = 100
    #     msg.data = array
    #     map_pub = rospy.Publisher('/occupancy_map',OccupancyGrid,queue_size=10)
    #     map_pub.publish(msg)

    # lidar visualizer in pcl

#    def lidar_callback(self,data):
#        rospy.loginfo("received pointcloud")
#        cloud_num = self.ros_to_pcl(data)
#        rospy.loginfo(cloud_num)
#        self.visualizer.ShowColorCloud(cloud_num,b'cloud')

if __name__ == '__main__':
    rospy.init_node('occupancy_grid_python', anonymous=True)
    rospy.loginfo('Occupancy grid running')
    Costmap(56,56,0.5)
    rospy.spin()
