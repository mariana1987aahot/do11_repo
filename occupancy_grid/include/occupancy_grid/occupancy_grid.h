#include <iostream>
#include <vector>
#include <cmath>
#include <ros/ros.h>
#include <string>
#include <tuple>
#include <math.h>
#include <array>
#include <iomanip>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/GridCells.h>
#include <std_msgs/Int8.h> 
#include <std_msgs/Int32.h> 
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Bool.h>
#include <autoware_msgs/LaneArray.h>
#include <autoware_msgs/Waypoint.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Point.h>
#include <tf/transform_listener.h>
#include <visualization_msgs/MarkerArray.h>
#include <sensor_msgs/LaserScan.h>

class OccupancyGrid{
  public:
    OccupancyGrid();
    ~OccupancyGrid();

  private:
    //  Add node handle here
    ros::NodeHandle nh_, pnh_;;
    
    //  Add ROS Parameters here
    std::string laser_scan_topic_;
    std::string vector_map_topic_;
    std::string occupancy_frame_;
    std::string vector_map_frame_;
    int map_width_;
    int map_height_;
    double resolution_;
    
    //  Add subscribers here
    ros::Subscriber laser_scan_sub_;
    ros::Subscriber vector_map_sub_;  

    //  Add Publishers here
    ros::Publisher occupancy_map_pub_;
    
    //  Add Timer Events here
    ros::Timer timer;

    //  Add private member variables here
    std_msgs::Int8MultiArray occupancy_array;
    int* array = NULL;    

    // Add private member function declarations here.
    void createOccupancyGrid(const ros::TimerEvent&);
    void laserscanToGrid();
    void curbsToGrid();

    //  Add Helper functions here
    geometry_msgs::PoseStamped createNewWaypoint(double current_x, double current_y, std::string frame);
    bool insideBoundaries(float x, float y);

    /* Add shared pointers here*/
    std::shared_ptr <sensor_msgs::LaserScan> current_laserscan_;
    std::shared_ptr <visualization_msgs::MarkerArray> current_vector_map_;


    
    /*Add ROS callbacks here */
    void laserscanCallback(const sensor_msgs::LaserScan::ConstPtr& msg);
    void vectormapCallback( const visualization_msgs::MarkerArray::ConstPtr& msg);

    
    //  Add TransformListener and transform functions here
    tf::TransformListener listener_;
    geometry_msgs::Pose lookupTF(geometry_msgs::PoseStamped initial_pt, std::string destination, std::string original);
};
