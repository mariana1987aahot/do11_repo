#!/usr/bin/env python
#THIS SCRIPT IS TO GIVE GOAL POSE


import rospy
from geometry_msgs.msg import PoseStamped
from tf.transformations import quaternion_from_euler
from autoware_config_msgs.msg import ConfigWaypointFollower

rospy.init_node('goal_pos')
pub1 = rospy.Publisher('move_base_simple/goal', PoseStamped, queue_size = 10)
pub2 = rospy.Publisher('config/waypoint_follower', ConfigWaypointFollower, queue_size = 10)

rospy.sleep(1)
checkpoint = PoseStamped()
checkpoint.header.frame_id = 'world'

checkpoint.pose.position.x = 6.63938903809
checkpoint.pose.position.y = 23.7333488464
checkpoint.pose.position.z = 0.0

checkpoint.pose.orientation.x = 0.0
checkpoint.pose.orientation.y = 0.0
checkpoint.pose.orientation.z = 0.918340174619
checkpoint.pose.orientation.w = -0.395792020739




pure_pursuit_param = ConfigWaypointFollower()

pure_pursuit_param.header.frame_id = ''
pure_pursuit_param.param_flag = 0
pure_pursuit_param.velocity = 2.0
pure_pursuit_param.lookahead_distance = 4.0
pure_pursuit_param.lookahead_ratio = 2.0
pure_pursuit_param.displacement_threshold = 0.0
pure_pursuit_param.relative_angle_threshold = 0.0



pub1.publish(checkpoint)

pub2.publish(pure_pursuit_param)
