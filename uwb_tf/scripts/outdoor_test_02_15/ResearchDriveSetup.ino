#include <Wire.h>
#include "Pozyx.h"
#include "Pozyx_definitions.h"


////////////////////////////////////////////////
////////// APPLICATION VARIABLES ///////////////
////////////////////////////////////////////////
uint8_t  calibration_status = 0;
uint8_t  calib_status;
uint16_t network_id = 0;

uint8_t  num_anchors = 15; // the number of anchors to use for the constellation

// urban systems outdoors - dec 26, 2018
uint16_t   anchors[15] = {0x6a26, 0x6a2d, 0x6a30, 0x6a31, 0x6a38, 0x6a39, 0x6a3b, 0x6a40, 0x6a41, 0x6a4a, 0x6a33, 0x6a20, 0x6a43, 0x6a6d, 0x6a44}; // the network id of the anchors: change these to the network ids of your anchors.
int32_t  anchors_x[15] = {66810,66703,45096,66925,67236,66719,39674,66851,-148215,38630,69714,55518,55043,63608,65649}; // anchor x-coordinates in mm
int32_t  anchors_y[15] = {-23160,25000,-97204,-58698,977,-114712,-48892,-82832,-148215,35527,-196911,-196850,-172626,100955,50845}; // anchor y-coordinates in mm
int32_t  anchors_z[15] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; // anchor z-coordinates in mm (not critical for ground vehicles)
//int32_t  anchors_z[4] = {   4280,  4280,   4280,   4280}; // anchor z-coordinates in mm (not critical for ground vehicles)
//uint8_t  algorithm = POZYX_POS_ALG_UWB_ONLY;   // positioning algorithm. Try POZYX_POS_ALG_TRACKING for fast moving objects vs POZYX_POS_ALG_UWB_ONLY
uint8_t  algorithm = POZYX_POS_ALG_TRACKING;   // positioning algorithm. Try POZYX_POS_ALG_TRACKING for fast moving objects vs POZYX_POS_ALG_UWB_ONLY
uint8_t  dimension = POZYX_2_5D;                 // positioning dimension (2D is more accurate for ground vehicles)
int32_t  height = 400;                         // height of the mobile device, only required in 2.5D or 3D positioning
uint16_t remote_id = NULL;                     // no remote devices
/////////////////////////////////////////////////

void setup(){
  Serial.begin(115200);
  delay(1000);
  if(Pozyx.begin() == POZYX_FAILURE){
    Serial.println(F("ERROR: Unable to connect to UWB shield"));
    Serial.println(F("Reset required"));
    delay(100);
    abort();
  }
  Pozyx.clearDevices(NULL); // clear previous devices in device list
  setAnchors();
  Pozyx.setPositionAlgorithm(algorithm, dimension, remote_id);
  // jd - filter strength is the second parameter (values=0..15) "FILTER_TYPE_MOVINGAVERAGE, 15" was used for the video shoot
  Pozyx.setPositionFilter(FILTER_TYPE_MOVINGAVERAGE, 15);  
  //Pozyx.setPositionFilter(FILTER_TYPE_MOVINGAVERAGE, 15); 
  //Pozyx.setPositionFilter(FILTER_TYPE_MOVINGMEDIAN, 15); 
  //Pozyx.setPositionFilter(FILTER_TYPE_FIR, 15);  
  //Pozyx.setPositionFilter(FILTER_TYPE_NONE, 0);  
  delay(2000);
}

void loop(){
  coordinates_t position;
  euler_angles_t hdg;
  int status = Pozyx.doPositioning(&position, dimension, height);
  Pozyx.getEulerAngles_deg(&hdg,NULL);
  if (status == POZYX_SUCCESS){
    printCoords_Hdg(position, hdg);
  }
}

// print coordinates and heading (direction the beacon is pointing relative to anchor constellation)
void printCoords_Hdg(coordinates_t coor, euler_angles_t heading){
  char temp_string[12];
  euler_angles_t temp_angle = heading;
  sprintf(temp_string, "%9d", coor.x);
  Serial.print(temp_string); 
  Serial.print(",");
  sprintf(temp_string, "%9d", coor.y);
  Serial.print(temp_string);  
  Serial.print(",");
   sprintf(temp_string, "%9d", coor.z);  
  Serial.print(temp_string); 
  Serial.print(",");
  sprintf(temp_string, "%9d", (int) (heading.heading));
  Serial.println(temp_string);
  //Serial.print("LED indicated error: ");
  //Serial.println(POZYX_ERRORCODE); 
}

// function to programmatically set anchor coordinates
void setAnchors(){
  for(int i = 0; i < num_anchors; i++){
    device_coordinates_t anchor;
    anchor.network_id = anchors[i];
    anchor.flag = 0x1;
    anchor.pos.x = anchors_x[i];
    anchor.pos.y = anchors_y[i];
    anchor.pos.z = anchors_z[i];
    Pozyx.addDevice(anchor, NULL);
  }
  if (num_anchors > 4){
    Pozyx.setSelectionOfAnchors(POZYX_ANCHOR_SEL_AUTO, num_anchors, NULL);
  }
}
