#!/usr/bin/env python
###################THIS IS GNSS TRANSFORM IF NEEDED. NOT USED RIGHT NOW#############################

import tf
import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseStamped
import sys


class tf_transfer:
    def __init__(self):
        self.msg = PoseStamped()
        self.gps_sub=rospy.Subscriber('kalman_pose',PoseStamped,self.gps_callback)
        #self.location=PoseStamped()

    def gps_callback(self,gps_data):
        rospy.loginfo("Receiving Information")# Test the info inflow
        self.msg=gps_data 
        self.loc_x=self.msg.pose.position.x
        self.loc_y=self.msg.pose.position.y
        self.loc_z=self.msg.pose.position.z

        self.angle_x=self.msg.pose.orientation.x
        self.angle_y=self.msg.pose.orientation.y
        self.angle_z=self.msg.pose.orientation.z
        self.angle_w=self.msg.pose.orientation.w
    #handle_uwb_tf(self.loc_x, self.loc_y, self.loc_z, self.angle_x, self.angle_y, self.angle_z, self.angle_w)
    #handle_uwb_tf(self)
        br = tf.TransformBroadcaster()
        br.sendTransform((self.loc_x, self.loc_y, self.loc_z),(self.angle_x, self.angle_y, self.angle_z, self.angle_w),rospy.Time.now(),"base_link","map") #child, parent
        rospy.loginfo("Publishing Information")# Test the info outflow

if __name__ == "__main__":
    rospy.init_node('gps_tf_broadcaster')
    C = tf_transfer()  
    r = rospy.Rate(40)
    while not rospy.is_shutdown():
        # r.sleep()
        rospy.spin()
