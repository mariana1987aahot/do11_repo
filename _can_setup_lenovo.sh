#!/bin/bash

#this shell file launches #all lidars #all cameras with rectification

#cmd1="sudo modprobe usbcore usbfs_memory_mb=1500"

cmd1="sudo ip link set can0 type can bitrate 500000"
cmd2="sudo ip link set up can0"
cmd3="sleep 3"
cmd4="~/ardarshir_ws/devel/lib/sm2/sm2_node "
cmd5="rosrun twist_gate_do11 twist_gate_do11"
cmd6="conda activate env3"
cmd7="cd ~/do11_workspace/src/do11_repo/can/src/scripts"
cmd8="python /home/ardashir/do11_workspace/src/do11_repo/can/src/scripts/ros_can_conversion.py"
cmd10="source "/home/ardashir/env3/bin/activate""
cmd11="which python"
cmd12="roscore"
cmd13="roslaunch joy ps4joy.launch"


#works, sets up can and runs ros_can_conversion.py
gnome-terminal --tab --command="bash -c '$cmd1; $cmd3; $cmd2; $cmd3; $cmd10; $cmd11; $cmd8; $SHELL '"\
	       --tab --command="bash -c '$cmd12; $SHELL '"\
	       --tab --command="bash -c '$cmd4; $SHELL '"\
	       --tab --command="bash -c '$cmd5; $SHELL '"\
	       --tab --command="bash -c '$cmd3; $cmd13; $SHELL '"\

