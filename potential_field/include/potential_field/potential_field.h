#include <iostream>
#include <vector>
#include <cmath>
#include <ros/ros.h>
#include <string>
#include <tuple>
#include <math.h>
#include <iomanip>
#include <nav_msgs/OccupancyGrid.h>
#include <std_msgs/Int8.h> 
#include <std_msgs/Int32.h> 
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Bool.h>
#include <autoware_msgs/LaneArray.h>
#include <autoware_msgs/Waypoint.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseArray.h>
#include <tf/transform_listener.h>
#include "tf2_msgs/TFMessage.h"
#include "tf2_ros/transform_listener.h"
#include <visualization_msgs/MarkerArray.h>

struct Node {
  //  grid index of flowfield node
  int index_x{0};
  int index_y{0};
  // Height value just in terms of wavefront
  int wave{0};
  //  Height value in flowfield (repulsive potential)
  double d{0.0};
  //  Actual Position in local frame, in our case Velodyne frame 
  double pos_x{0.0};
  double pos_y{0.0};
};

class PotentialField{
  public:
    PotentialField();
    ~PotentialField();

  private:
    //  Add node handle here
    ros::NodeHandle nh_, pnh_;;
    
    //  Add ROS Parameters here
    std::string local_frame_;
    std::string orig_frame_;
    double k_repulsive_;
    double robot_radius_;
    double k_attractive_;
    
    //  Add subscribers here
    ros::Subscriber occupancy_grid_sub_;
    ros::Subscriber waypoint_sub_;  
    ros::Subscriber closest_waypoint_index_sub_;
    ros::Subscriber curb_sub_;
    
    //  Add Publishers here
    ros::Publisher pot_field_replan_pub_;
    ros::Publisher feasible_path_pub_;
    ros::Publisher pot_field_path_pub_;
    
    // Add Visualization Publishers here
    ros::Publisher vis_pub_flowfield_;
    ros::Publisher obstacle_pub_;
    ros::Publisher inflation_pub_;
    ros::Publisher vis_pub_start_;
    ros::Publisher vis_pub_goal_;
    ros::Publisher vis_path_pub_;
    
    //  Add Timer Events here
    ros::Timer timer;

    
    //  Creating tuples and vectors for flowfield/obstacles/inflations/goal/start etc.
    //  Flowfield contains both index of the grid and the positions in velodyne frame
    std::vector<Node> flowfield {};
    std::vector<Node> obstacles {};
    std::vector<Node> inflations {};
    std::vector<Node> path {};
    Node start_node;
    Node goal_node;
    
    
//    ros::Subscriber cluster_map_sub_;
//    ros::Subscriber waypoint_sub_;  

//    ros::Subscriber curb_sub_;
    
    // Add publishers here
//    ros::Publisher publisher;
//    ros::Publisher marker_pub_;
//    ros::Publisher obstacle_pub_;
//    ros::Publisher vis_pub_start_;
//    ros::Publisher vis_pub_goal_;
//    ros::Publisher inflation_pub_;
//    ros::Publisher vis_pub_flowfield_;
//    ros::Publisher pot_field_replan_pub_;
//    ros::Publisher pot_field_path_pub_;
//    ros::Publisher feasible_path_pub_;
    
    

    // Add private member variables here
    int nMapWidth{};
    int nMapHeight{};
    float res{}; //resolution of occupancy grid and related to your motion model and flow
    std::vector<std::vector<float>> motion {};
    std::vector<std::vector<float>> motion2 {};
    

//    std::vector<std::tuple<int,int,double>> path_;

    // Add private member function declarations here. This is flow of creating the potential field
    void createFlowField(const ros::TimerEvent&);    
    void createInflationLayer();
    void findGoalNode();
    void createWaveFront();
    void repulsivePotential(Node &current_node);
    void attractivePotential(Node &current_node);
    void planner();
    void createWaypoints();
    
    // Add Helper functions here
    double fitToResolution(float);
    Node createNewNode(int index_x, int index_y, double d);
    geometry_msgs::PoseStamped createNewWaypoint(double current_x, double current_y, std::string frame);
    double getDistance(Node node1, Node node2);
    bool insideBoundaries(int x, int y, int safety);
    std_msgs::Bool controllerFlag(int x, int y);
//    void getMotionModel();
//    void planner();

//    void repulsivePotential(std::tuple<int, int, double> &current_node);
//    double getDistance(const std::tuple<int, int, double> &node1, 
//                       const std::tuple<int, int, double> &node2);
//    double getDistance(const std::tuple<int, int, double> &node1, 
//                       const std::tuple<int, int> &node2);
                       
//    void attractivePotential(std::tuple<int, int, double> &current_node);

    
    /*shared pointers*/
    std::shared_ptr <nav_msgs::OccupancyGrid> current_occupancy_;
    std::shared_ptr <autoware_msgs::LaneArray> current_lanes_;
    std::shared_ptr <std_msgs::Int32> current_closest_index_;
    std::shared_ptr <visualization_msgs::MarkerArray> current_curbs_;
//    std::shared_ptr <nav_msgs::OccupancyGrid> current_cluster_;


//    std::shared_ptr <visualization_msgs::MarkerArray> current_curbs_;
    
    /*functions relating to ROS*/
//    void createInflationLayer();

//    void createWaypoints();
//    void addCurbsAsObstacles();
    
//    geometry_msgs::Pose lookupTF(geometry_msgs::PoseStamped initial_pt);
//    geometry_msgs::Pose lookupTFReverse(geometry_msgs::PoseStamped initial_pt);
//    void visualization();
    
//    std::tuple<int, int> fitToResolution(std::tuple<double,double>);
    
    
    /*ROS callbacks*/
    void occupanyCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg);
    void waypointCallback( const autoware_msgs::LaneArray::ConstPtr& msg);
    void closestWaypointIndexCallback(const std_msgs::Int32::ConstPtr& msg);
    void curbCallback(const visualization_msgs::MarkerArray::ConstPtr& msg);
//    void clusterCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg);

//    void curbCallback(const visualization_msgs::MarkerArray::ConstPtr& msg);
    
    //  TransformListener for transforms of waypoints and goal position
    tf::TransformListener listener_;
    geometry_msgs::Pose lookupTF(geometry_msgs::PoseStamped initial_pt, std::string destination, std::string original);
    
    // Vizualization Functions
    void viz();
    void vizFlowField();
    void vizObstacles();
    void vizInflation();
    void vizStartAndGoal();
    void vizPath();

};
