
#include "potential_field/potential_field.h"

void PotentialField::viz(){
//  Obstacle Viz
    vizObstacles();
    vizInflation();


//  %Start and goal points
    vizStartAndGoal();
    
//    VisualizeFlowField
    vizFlowField();
    
// Final Path
    vizPath();
}


void PotentialField::vizFlowField(){
    visualization_msgs::Marker points {};
    points.header.frame_id = local_frame_;
    points.header.stamp = ros::Time::now();
    points.ns = "FlowField";
    points.action =  visualization_msgs::Marker::ADD;
    points.pose.orientation.w = 1.0;
    points.id = 0;
    points.type = visualization_msgs::Marker::POINTS;
    points.scale.x = 1*res;
    points.scale.y = 1*res;
    // Points are green
    points.scale.x = 1*res;
    points.scale.y = 1*res;
    points.scale.z = 1*res;
    points.color.g = 0.5f;
    points.color.a = 1.0; 
    points.color.r = 1.0;
    points.color.b = 0.0;

    // Create the vertices for the points and lines
    for (auto cell: flowfield)
    {
      geometry_msgs::Point p;
      p.x = cell.pos_x;
      p.y = cell.pos_y;
      p.z = cell.d;
      points.points.push_back(p);    
    }
    vis_pub_flowfield_.publish(points);      
}

void PotentialField::vizObstacles(){
  visualization_msgs::Marker points {};
    points.header.frame_id = local_frame_;
    points.header.stamp = ros::Time::now();
    points.ns = "obstacles";
    points.action =  visualization_msgs::Marker::ADD;
    points.pose.orientation.w = 1.0;
    points.id = 0;
    points.type = visualization_msgs::Marker::POINTS;
    points.scale.x = 1*res;
    points.scale.y = 1*res;
    // Points are green
    points.color.g = 0.5f;
    points.color.a = 1.0; 
	  points.color.r = 1.0;
	  points.color.b = 1.0;

    // Create the vertices for the points and lines
    for (auto obstacle: obstacles)
    {
      geometry_msgs::Point p;
      p.x = obstacle.pos_x;
      p.y = obstacle.pos_y;
      p.z = 1;
      points.points.push_back(p);
    }
    obstacle_pub_.publish(points);
}

void PotentialField::vizInflation(){
  visualization_msgs::Marker points {};
    points.header.frame_id = local_frame_;
    points.header.stamp = ros::Time::now();
    points.ns = "inflation";
    points.action =  visualization_msgs::Marker::ADD;
    points.pose.orientation.w = 1.0;
    points.id = 0;
    points.type = visualization_msgs::Marker::POINTS;
    points.scale.x = 1*res;
    points.scale.y = 1*res;
    // Points are green
    points.color.g = 0.5f;
    points.color.a = 1.0; 
	  points.color.r = 0.0;
	  points.color.b = 1.0;

    // Create the vertices for the points and lines
    for (auto inflation: inflations)
    {
      geometry_msgs::Point p;
      p.x = inflation.pos_x;
      p.y = inflation.pos_y;
      p.z = 1;
      points.points.push_back(p);
    }
    inflation_pub_.publish(points);
}

void PotentialField::vizStartAndGoal()
{
    auto p = [&](int columns, int rows) {return (rows + nMapHeight/2) * nMapWidth + (columns + nMapWidth/2);};
    visualization_msgs::Marker start;
    start.header.frame_id = local_frame_;
    start.header.stamp = ros::Time();
    start.ns = "start_position";
    start.id = 0;
    start.type = visualization_msgs::Marker::SPHERE;
    start.action = visualization_msgs::Marker::ADD;
  //start.action = 2;
    start.pose.position.x = start_node.pos_x;
    start.pose.position.y = start_node.pos_y;
    start.pose.position.z = start_node.d;
    start.pose.orientation.x = 0.0;
    start.pose.orientation.y = 0.0;
    start.pose.orientation.z = 0.0;
    start.pose.orientation.w = 1.0;
    start.scale.x = 3;
    start.scale.y = 3;
    start.scale.z = 3;
    start.color.a = 1.0; // Don't forget to set the alpha!
    start.color.r = 0.0;
    start.color.g = 1.0;
    start.color.b = 0.0;
    vis_pub_start_.publish( start );
    
    visualization_msgs::Marker goal;
    goal.header.frame_id = local_frame_;
    goal.header.stamp = ros::Time();
    goal.ns = "goal_position";
    goal.id = 0;
    goal.type = visualization_msgs::Marker::SPHERE;
    goal.action = visualization_msgs::Marker::ADD;
  //  closest.action = 2;
    goal.pose.position.x = goal_node.pos_x;
    goal.pose.position.y = goal_node.pos_y;
    goal.pose.position.z = goal_node.d;
    goal.pose.orientation.x = 0.0;
    goal.pose.orientation.y = 0.0;
    goal.pose.orientation.z = 0.0;
    goal.pose.orientation.w = 1.0;
    goal.scale.x = 3;
    goal.scale.y = 3;
    goal.scale.z = 3;
    goal.color.a = 1.0; // Don't forget to set the alpha!
    goal.color.r = 1.0;
    goal.color.g = 0.0;
    goal.color.b = 0.0;
    vis_pub_goal_.publish( goal );
}

void PotentialField::vizPath(){
//    std::cout<<"In vizPath"<<std::endl;
    visualization_msgs::Marker points {};
    visualization_msgs::Marker line_strip{};
    visualization_msgs::Marker line_list {};
    points.header.frame_id = line_strip.header.frame_id = line_list.header.frame_id = local_frame_;
    points.header.stamp = line_strip.header.stamp = line_list.header.stamp = ros::Time::now();
    points.ns = line_strip.ns = line_list.ns = "path";
    points.action = line_strip.action = line_list.action = visualization_msgs::Marker::ADD;
    points.pose.orientation.w = line_strip.pose.orientation.w = line_list.pose.orientation.w = 1.0;
    points.id = 0;
    line_strip.id = 1;
    line_list.id = 2;
    points.type = visualization_msgs::Marker::POINTS;
    line_strip.type = visualization_msgs::Marker::LINE_STRIP;
    line_list.type = visualization_msgs::Marker::LINE_LIST;
    points.scale.x = 1*res;
    points.scale.y = 1*res;
    line_strip.scale.x = 0.1;
    line_list.scale.x = 0.1;
    // Points are green
    points.color.g = 1.0f;
    points.color.a = 1.0;
    // Line strip is blue
    line_strip.color.b = 1.0;
    line_strip.color.a = 1.0;



    // Create the vertices for the points and lines
    for (auto node: path)
    {
    
      geometry_msgs::Point p;
      p.x = node.pos_x;
      p.y = node.pos_y;
      p.z = 0;
      points.points.push_back(p);
      line_strip.points.push_back(p);
    }
    vis_path_pub_.publish(points);
    vis_path_pub_.publish(line_strip);
}

double PotentialField::fitToResolution(float float_val)
{
  //snap the floats to nearest res value
  double snapped_val;
  snapped_val = double(int(float_val /res + 0.5)) *res;
  return snapped_val;
}

Node PotentialField::createNewNode(int index_x, int index_y, double d)
{
    Node node; 
    node.index_x = index_x;
    node.index_y = index_y;
    node.d = d;
    node.pos_x = fitToResolution(index_x)*res;
    node.pos_y = fitToResolution(index_y)*res;
    return node;
}

geometry_msgs::PoseStamped PotentialField::createNewWaypoint(double current_x, double current_y, std::string frame)
{
    geometry_msgs::PoseStamped waypoint;
    waypoint.header.frame_id = frame; //"map"
    waypoint.pose.position.x = current_x;
    waypoint.pose.position.y = current_y;
    waypoint.pose.position.z = 0;
    waypoint.pose.orientation.x = 0.0;
    waypoint.pose.orientation.y = 0.0;
    waypoint.pose.orientation.z = 0.0;
    waypoint.pose.orientation.w = 1.0;
    return waypoint;
}

double PotentialField::getDistance(Node node1, Node node2)
{
  double x = node1.index_x - node2.index_x;
  double y = node1.index_y - node2.index_y;
  return double(sqrt(pow(x, 2) + pow(y, 2)));
}


bool PotentialField::insideBoundaries(int x, int y, int safety)
{
  if(x < nMapWidth/2 - safety && y < nMapHeight/2 - safety && x >= -nMapWidth/2 + safety && y >= -nMapHeight/2 + safety)
  {
      return true;
  }else
  {
      return false;
  }
  
}

std_msgs::Bool PotentialField::controllerFlag(int x, int y)
{
  auto p = [&](int columns, int rows) {return (rows + nMapHeight/2) * nMapWidth + (columns + nMapWidth/2);};
  std_msgs::Bool flag_stanley;
  flag_stanley.data = false; //NEEDS TO BE FALSE
  double potential = flowfield[p(x,y)].d;
  if(potential == -1 || potential == -2 || potential >= 55)
  {
    flag_stanley.data = true;
  }
  for( auto move : motion)
  {
    if(insideBoundaries(x+move[0], y+move[1],0))
    {
      potential = flowfield[p(x + move[0],y + move[1])].d;
      if(potential == -1 ||  potential == -2 || potential >= 55)
      {
        flag_stanley.data = true;
      }
    }
  }
  //checking 16 cells around the front axle
  for(auto move : motion2)
  {
    if(insideBoundaries(x+move[0]+3, y+move[1]+3,0))
    {
      potential = flowfield[p(x + move[0]+3,y + move[1]+3)].d;
      if(potential == -1 || potential == -2 || potential >= 55)
      {
        flag_stanley.data = true;
      }
    }
  }
  return flag_stanley;
  
}

//Generic lookup transform function
geometry_msgs::Pose PotentialField::lookupTF(geometry_msgs::PoseStamped initial_pt, std::string destination, std::string original)
{
    geometry_msgs::Pose transformed_pose;  
    tf::Transform dest_to_orig;
    tf::poseMsgToTF(initial_pt.pose, dest_to_orig);
    tf::StampedTransform req_to_dest;
    try
    {
      listener_.lookupTransform(destination, original, ros::Time(0), req_to_dest);
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }
    tf::Transform req_to_orig;
    req_to_orig = req_to_dest * dest_to_orig;
    tf::poseTFToMsg(req_to_orig, transformed_pose);
    
    return transformed_pose;
}
