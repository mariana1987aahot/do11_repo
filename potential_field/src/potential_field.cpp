#include "potential_field/potential_field.h"

PotentialField::PotentialField() : nh_(""), pnh_("~")
{
  // Add rosparamters here
  pnh_.param("local_frame", local_frame_, std::string("/velodyne"));
  pnh_.param("orig_frame", orig_frame_, std::string("/map"));
  pnh_.param("k_repulsive", k_repulsive_, double(5000.0));
  pnh_.param("robot_radius", robot_radius_, double(6.0));
  pnh_.param("k_attractive", k_attractive_, double(0.02));
  
  

  
  //  Add subscribers here
  occupancy_grid_sub_ =  nh_.subscribe("occupancy_map", 1000, &PotentialField::occupanyCallback, this);
  curb_sub_ =  nh_.subscribe("vector_map", 1000, &PotentialField::curbCallback,this);
  waypoint_sub_ = nh_.subscribe("lane_waypoints_array1", 1000, &PotentialField::waypointCallback, this); 
  closest_waypoint_index_sub_ = nh_.subscribe("closest_waypoint_index", 1000, &PotentialField::closestWaypointIndexCallback, this); 
  
  //  Add publishers here
  pot_field_replan_pub_ = nh_.advertise<std_msgs::Bool>("potential_field_replan_state", 10);
  feasible_path_pub_ = nh_.advertise<std_msgs::Bool>("is_path_feasible", 10);
  pot_field_path_pub_ = nh_.advertise<geometry_msgs::PoseArray>("potential_field_replan_path", 10);
  
  // Add Visualization Publishers here
  vis_pub_flowfield_ = nh_.advertise<visualization_msgs::Marker>("flowfield_visualization", 10);
  obstacle_pub_ = nh_.advertise<visualization_msgs::Marker>("obstacle_visualization", 10);
  inflation_pub_ = nh_.advertise<visualization_msgs::Marker>("inflation_visualization", 10);
  vis_pub_start_ = nh_.advertise<visualization_msgs::Marker>("start_visualization", 10);
  vis_pub_goal_ = nh_.advertise<visualization_msgs::Marker>("goal_visualization", 10);
  vis_path_pub_ = nh_.advertise<visualization_msgs::Marker>("path_visualization", 10);
  
  //  Add TimerEvent
  timer = nh_.createTimer(ros::Duration(0.05), &PotentialField::createFlowField, this);
  
  // Motion Model for checking  8 cells and 16 cells 
  motion = {{1,0},{0,1},{-1,0},{0,-1},{-1,-1},{-1,1},{1,-1},{1,1}};
  motion2 = {{1,0},{0,1},{-1,0},{0,-1},{-1,-1},{-1,1},{1,-1},{1,1},{2,0},{0,2},{-2,0},{0,-2},{-2,-2},{-2,2},{2,-2},{2,2},{2,1},{1,2},{-2,1},{1,-2}};
};

PotentialField::~PotentialField(){};

void PotentialField::createFlowField(const ros::TimerEvent&)
{
  //  Lamda function to find what element you are looking at in the array in terms of (x,y)
  auto p = [&](int columns, int rows) {return (rows + nMapHeight/2) * nMapWidth + (columns + nMapWidth/2);};
  start_node = createNewNode(3/res,0,0);
  if(current_occupancy_ != nullptr)
  {
    nMapWidth = current_occupancy_->info.width;
    nMapHeight = current_occupancy_->info.height;
    res = current_occupancy_->info.resolution;
    
    //  Nulling flowfield and obstacles tuples
    flowfield.clear();
    obstacles.clear();
    int count{0}; 
    //  Populating flow field with 0s and -1s based on occupancy map
    for (int rows = -nMapHeight/2; rows < nMapHeight/2; rows++)
    {
      for (int columns = -nMapWidth/2; columns < nMapWidth/2; columns++)
      {
        Node node = createNewNode(columns, rows, 0);        
        if (current_occupancy_->data[p(columns,rows)] != 0)
        {
          node.d = -1;
          flowfield.push_back(node);
          obstacles.push_back(node);
        }
        else
        {
          flowfield.push_back(node);
        }
          count++;
      }
    }
//    std::cout<<"count = "<<count<<std::endl;
  }

  createInflationLayer();
  //THIS WILL GO AFTER SETTING ALL OBSTACLES AND FLOWFIELDS
  //  Set Goal Waypoint upon lanes input
  if(flowfield.size() != 0)
  {
    if(current_lanes_!=nullptr)
    {
      findGoalNode();
    }else
    {
      goal_node = createNewNode(3/res,0,1);
    }
    flowfield[p(goal_node.index_x,goal_node.index_y)].d = 1;
  }
  createWaveFront();
  planner();
  createWaypoints();
  
  viz();
}

void PotentialField::createInflationLayer()
{
  inflations.clear();
  auto p = [&](int columns, int rows) {return (rows + nMapHeight/2) * nMapWidth + (columns + nMapWidth/2);};
  
  for (auto obstacle: obstacles){
    for (auto move: motion){
      int x = obstacle.index_x+move[0];
      int y = obstacle.index_y+move[1];
      if(insideBoundaries(x,y,0))
      {
        if(flowfield[p(x,y)].d != -1 && flowfield[p(x,y)].d != -2 )
        {  
            flowfield[p(x,y)].d = -2;
            inflations.push_back(createNewNode(x,y,0));
        }
      }
    }   
  }
//  std::cout<<"Inflation Layer Obstacle size = "<< inflations.size() << std::endl;
}

void PotentialField::findGoalNode()
{
  // Need to have obstacle, inflation layer and repulsive info here
  bool end {true};
  //  Creating flags for stanley or potential field takeover
  std_msgs::Bool flag_stanley;
  std_msgs::Bool final_flag_stanley;
  final_flag_stanley.data = false;
  //  From Stanley, closest waypoint index
  auto finalWaypoint = current_closest_index_->data;
  while(end)
  {
    auto current_x = current_lanes_->lanes[0].waypoints[finalWaypoint].pose.pose.position.x;
    auto current_y = current_lanes_->lanes[0].waypoints[finalWaypoint].pose.pose.position.y;
    geometry_msgs::PoseStamped waypoint = createNewWaypoint(current_x, current_y, orig_frame_);
    geometry_msgs::Pose transformed_pose;  
    transformed_pose = lookupTF(waypoint, local_frame_, orig_frame_);
    auto x = fitToResolution(transformed_pose.position.x)/res;
    auto y = fitToResolution(transformed_pose.position.y)/res;
    if(insideBoundaries(x,y,1))
    {
      goal_node.index_x = x;
      goal_node.index_y = y;
      goal_node.pos_x = x*res;
      goal_node.pos_y = y*res;
      goal_node.d = 1.0;
      int x = int(goal_node.index_x);
      int y = int(goal_node.index_y);
      flag_stanley = controllerFlag(transformed_pose.position.x/res,transformed_pose.position.y/res);
      if(flag_stanley.data == true)
      {
        final_flag_stanley.data = true;
      }
      finalWaypoint++;
      if(finalWaypoint == current_lanes_->lanes[0].waypoints.size())
      {
        break;
      }
    }else{
      end = false;    
    }
  }
  pot_field_replan_pub_.publish(final_flag_stanley);
}


//void PotentialField::findGoalNode()
//{
//  // Need to have obstacle, inflation layer and repulsive info here
//  bool end {true};
//  //  Creating flags for stanley or potential field takeover
//  std_msgs::Bool flag_stanley;
//  
//  //  From Stanley, closest waypoint index
//  auto finalWaypoint = current_lanes_->lanes[0].waypoints.size()-1;
//  while(end)
//  {
//    auto current_x = current_lanes_->lanes[0].waypoints[finalWaypoint].pose.pose.position.x;
//    auto current_y = current_lanes_->lanes[0].waypoints[finalWaypoint].pose.pose.position.y;
//    geometry_msgs::PoseStamped waypoint = createNewWaypoint(current_x, current_y, orig_frame_);
//    geometry_msgs::Pose transformed_pose;  
//    transformed_pose = lookupTF(waypoint, local_frame_, orig_frame_);
//    auto x = fitToResolution(transformed_pose.position.x)/res;
//    auto y = fitToResolution(transformed_pose.position.y)/res;
//    if(insideBoundaries(x,y,1))
//    {
//      if(finalWaypoint == current_lanes_->lanes[0].waypoints.size()-1)
//      {
//        end = false;
//      }else
//      {
//        finalWaypoint++;
//      }
//      goal_node.index_x = x;
//      goal_node.index_y = y;
//      goal_node.pos_x = x*res;
//      goal_node.pos_y = y*res;
//      goal_node.d = 1.0;
//      int x = int(goal_node.index_x);
//      int y = int(goal_node.index_y);
//      flag_stanley = controllerFlag(x,y);

//    }
//    else
//    {
//      if(finalWaypoint == current_lanes_->lanes[0].waypoints.size()-1)
//      {
//        finalWaypoint = current_closest_index_->data;
//      }else
//      {
//        end = false;
//      }
//    }
//  }
//  pot_field_replan_pub_.publish(flag_stanley);
//  
//}

void PotentialField::createWaveFront(){
  auto p = [&](int columns, int rows) {return (rows + nMapHeight/2) * nMapWidth + (columns + nMapWidth/2);};
  std::vector<Node> nodes;
  
  // Add the first discovered node - the target location, with a distance of 1
  nodes.push_back(goal_node);
  while(!nodes.empty())
  {
    std::vector<Node> new_nodes;
    for(auto &n : nodes)
    {
        int x = n.index_x;
        int y = n.index_y;
        int d = n.d;
        
        // Check North
        if ((y + 1) < nMapHeight/2 && flowfield[p(x,y+1)].d == 0)
        {
          new_nodes.push_back(createNewNode(x,y+1,d+1));
//          flowfield[p(x,y+1)].d = d+1;
          flowfield[p(x,y+1)].wave = d+1;
          attractivePotential(flowfield[p(x,y+1)]);
          repulsivePotential(flowfield[p(x,y+1)]);
        }
        // Check South
        if ((y - 1) >= -nMapHeight/2 && flowfield[p(x,y-1)].d == 0){
          new_nodes.push_back(createNewNode(x,y-1,d+1));
//          flowfield[p(x,y-1)].d = d+1;
          flowfield[p(x,y-1)].wave = d+1;
          attractivePotential(flowfield[p(x,y-1)]);
          repulsivePotential(flowfield[p(x,y-1)]);
        }
        // Check East
        if ((x + 1) < nMapWidth/2 && flowfield[p(x+1,y)].d  == 0){
          new_nodes.push_back(createNewNode(x+1,y,d+1));
//          flowfield[p(x+1,y)].d = d+1;
          flowfield[p(x+1,y)].wave = d+1;
          attractivePotential(flowfield[p(x+1,y)]);
          repulsivePotential(flowfield[p(x+1,y)]);
        }
        // Check West
        if ((x - 1) >= -nMapWidth/2 && flowfield[p(x-1,y)].d == 0){
          new_nodes.push_back(createNewNode(x-1,y,d+1));
//          flowfield[p(x-1,y)].d = d+1;
          flowfield[p(x-1,y)].wave = d+1;
          attractivePotential(flowfield[p(x-1,y)]);
          repulsivePotential(flowfield[p(x-1,y)]);
        }
        // Check North West 
        if ((x - 1) >= -nMapWidth/2 && (y + 1) < nMapHeight/2 && flowfield[p(x-1,y+1)].d == 0){
          new_nodes.push_back(createNewNode(x-1,y+1,d+1));
//          flowfield[p(x-1,y+1)].d = d+1;
          flowfield[p(x-1,y+1)].wave = d+1;
          attractivePotential(flowfield[p(x-1,y+1)]);
          repulsivePotential(flowfield[p(x-1,y+1)]);
        }
        // Check  South West
        if ((x - 1) >= -nMapWidth/2 && (y - 1) >= -nMapHeight/2 && flowfield[p(x-1,y-1)].d == 0){
          new_nodes.push_back(createNewNode(x-1,y-1,d+1));
//          flowfield[p(x-1,y-1)].d = d+1;
          flowfield[p(x-1,y-1)].wave = d+1;
          attractivePotential(flowfield[p(x-1,y-1)]);
          repulsivePotential(flowfield[p(x-1,y-1)]);
        }
        // Check North East
        if ((y + 1) < nMapHeight/2 && (x + 1) < nMapWidth/2 && flowfield[p(x+1,y+1)].d == 0){
          new_nodes.push_back(createNewNode(x+1,y+1,d+1));
//          flowfield[p(x+1,y+1)].d = d+1;
          flowfield[p(x+1,y+1)].wave = d+1;
          attractivePotential(flowfield[p(x+1,y+1)]);
          repulsivePotential(flowfield[p(x+1,y+1)]);
        }
        // Check South East
        if ((x + 1) < nMapWidth/2 && (y - 1) >= -nMapHeight/2 && flowfield[p(x+1,y-1)].d == 0){
          new_nodes.push_back(createNewNode(x+1,y-1,d+1));
//          flowfield[p(x+1,y-1)].d = d+1;
          flowfield[p(x+1,y-1)].wave = d+1;
          attractivePotential(flowfield[p(x+1,y-1)]);
          repulsivePotential(flowfield[p(x+1,y-1)]);
          //std::cout << "POTENTIAL flowfield[p(x+1,y-1)]  = " << flowfield[p(x+1,y-1)] << std::endl;
        }
    }
    nodes.clear();
    nodes.insert(nodes.begin(), new_nodes.begin(), new_nodes.end());
    new_nodes.clear();
  }
}

void PotentialField::attractivePotential(Node &current_node)
{
 current_node.d += 0.5 * k_attractive_ * pow(getDistance(current_node, goal_node),2);
}

void PotentialField::repulsivePotential(Node &current_node)
{
  //search nearest obstacles
  double distance{};
  double min_distance = std::numeric_limits<double>::infinity();
  for(auto inflation: inflations)
  {
    distance = getDistance(current_node, inflation);
    if (distance < min_distance)
    {
      min_distance = distance;
    }
  }
  
  if (min_distance <= robot_radius_)
  {
    if(min_distance <= 0.1)
    {
      min_distance = 0.1;
    }
    current_node.d += 0.5 * k_repulsive_ * pow((1.0/min_distance - 1.0 / robot_radius_), 2);
    
  }
}

void PotentialField::planner()
{
  auto p = [&](int columns, int rows) {return (rows + nMapHeight/2) * nMapWidth + (columns + nMapWidth/2);};
  double potential{};
  int ix {start_node.index_x}, iy {start_node.index_y}, inx{}, iny{}, count{0}, wave;
  path.clear();
  path.push_back(start_node);
  double distance = getDistance(start_node,goal_node);
  
  while(distance > 3 && count < (nMapHeight*nMapWidth-inflations.size()-obstacles.size()))
  {
    double min_potential = std::numeric_limits<double>::infinity();
    int min_wave = std::numeric_limits<int>::max();
    int minix{-1}, miniy{-1};
    for (auto move: motion)
    {
      inx = ix + move[0];
      iny = iy + move[1];
      if(insideBoundaries(inx,iny,0))
      {
        if(flowfield[p(inx, iny)].d != -1 && flowfield[p(inx, iny)].d != -2)
        {
          potential = flowfield[p(inx, iny)].d;
          wave = flowfield[p(inx, iny)].wave;
        }else
        {
          potential = std::numeric_limits<double>::infinity();
          wave = std::numeric_limits<int>::max();
        }
        //std::cout << "potential = " << potential << " min_potential = " << min_potential << std::endl;
        //std::cout << "wave = " << wave << " min_wave = " << min_wave << std::endl;
        if(wave <= min_wave)
        {
          if(potential <= min_potential)
          {
            //std::cout << "new potential path point" << std::endl;
            min_potential = potential;
            min_wave = wave;
            minix = inx;
            miniy = iny;
          }
        } 
      }
    }
    //std::cout << "new node:  ix = " << ix << " iy = " << iy << std::endl;
    ix = minix;
    iy = miniy;
    Node node = createNewNode(ix,iy,flowfield[p(ix, iy)].d);
    distance = getDistance(node,goal_node);
    path.push_back(node);
    count++;
  }
  
  std_msgs::Bool is_feasible;
  if (distance>5)
  {
    //infeasible path
    is_feasible.data = false;
  }
  else
  {
    is_feasible.data = true;
  }
  feasible_path_pub_.publish(is_feasible);
}

void PotentialField::createWaypoints()
{
  geometry_msgs::PoseArray waypoints{};
  for(auto node:path)
  {
    auto current_x = node.index_x;
    auto current_y = node.index_y;
    geometry_msgs::PoseStamped waypoint = createNewWaypoint(current_x, current_y, local_frame_); 
    geometry_msgs::Pose transformed_pose;
    transformed_pose = lookupTF(waypoint, orig_frame_, local_frame_);
    waypoints.poses.push_back(transformed_pose);    
  }
  pot_field_path_pub_.publish(waypoints);
}


//Add Callbacks here
void PotentialField::occupanyCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
  current_occupancy_ = std::make_shared<nav_msgs::OccupancyGrid>(*msg);
}
void PotentialField::waypointCallback( const autoware_msgs::LaneArray::ConstPtr& msg)
{
  current_lanes_ = std::make_shared<autoware_msgs::LaneArray>(*msg);
}
void PotentialField::closestWaypointIndexCallback(const std_msgs::Int32::ConstPtr& msg)
{
  current_closest_index_ = std::make_shared<std_msgs::Int32>(*msg);
}
void PotentialField::curbCallback(const visualization_msgs::MarkerArray::ConstPtr& msg)
{
  current_curbs_ = std::make_shared<visualization_msgs::MarkerArray>(*msg);
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "potential_field_node");
  PotentialField obj;
  ros::spin();
  return 0;
};
