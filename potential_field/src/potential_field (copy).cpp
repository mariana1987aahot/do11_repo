#include "potential_field/potential_field.h"

PotentialField::PotentialField() : nh_("")
{
//   add subscribers and init functions

//  createFlowFieldROS();
  
//  createWaveFront();
//  planner();
//  printFlowField();
  
  marker_pub_ = nh_.advertise<visualization_msgs::Marker>("path_visualization", 10);
  obstacle_pub_ = nh_.advertise<visualization_msgs::Marker>("obstacle_visualization", 10);
  vis_pub_start_ = nh_.advertise<visualization_msgs::Marker>("start_visualization", 10);
  vis_pub_goal_ = nh_.advertise<visualization_msgs::Marker>("goal_visualization", 10);
  inflation_pub_ = nh_.advertise<visualization_msgs::Marker>("inflation_visualization", 10);
  vis_pub_flowfield_ = nh_.advertise<visualization_msgs::Marker>("flowfield_visualization", 10);
  pot_field_replan_pub_ = nh_.advertise<std_msgs::Bool>("potential_field_replan_state", 10);
  pot_field_path_pub_ = nh_.advertise<geometry_msgs::PoseArray>("potential_field_replan_path", 10);
  
  feasible_path_pub_ = nh_.advertise<std_msgs::Bool>("is_path_feasible", 10);
  
  cluster_map_sub_ =  nh_.subscribe("cluster_map", 1000, &PotentialField::clusterCallback,this);
  occupancy_grid_sub_ =  nh_.subscribe("occupancy_map", 1000, &PotentialField::occupanyCallback, this);
  
  waypoint_sub_ = nh_.subscribe("lane_waypoints_array1", 1000, &PotentialField::waypointCallback, this); 
  index_sub_ = nh_.subscribe("closest_waypoint_index", 1000, &PotentialField::closestWaypointIndexCallback, this); 
  
//for curb detection
  curb_sub_ =  nh_.subscribe("vector_map", 1000, &PotentialField::curbCallback,this);

};

PotentialField::~PotentialField(){};

void PotentialField::createFlowFieldROS(){
  std::ofstream myfile ("flow_viz.txt");
  nMapWidth = current_occupancy_->info.width;
  nMapHeight = current_occupancy_->info.height;
  res = current_occupancy_->info.resolution;
  
//  nMapWidth = 36;
//  nMapHeight = 15;
  // auto p = [&](int x, int y) {return x * nMapWidth + y;};
  auto p = [&](int columns, int rows) {return (rows + nMapHeight/2) * nMapWidth + (columns + nMapWidth/2);};
//  getMotionModel();
  flowfield.clear();
  obstacles.clear(); 
  for (int rows = -nMapHeight/2; rows < nMapHeight/2; rows++)
  {
    for (int columns = -nMapWidth/2; columns < nMapWidth/2; columns++)
    {
      // (-100, -100) = data(0)  // (100,100) = data(39999) // (-99,-100) = data(1)
      std::tuple<int, int, double> node{columns,rows,0};

      if (current_occupancy_->data[p(columns,rows)] != 0)
      {
//        std::cout<<"Yes its a obstacle = " << p(x,y) <<std::endl;
        std::get<2>(node) = -1;
        flowfield.push_back(node);
        obstacles.push_back(std::make_tuple(columns,rows));
      }
      else
      {
        flowfield.push_back(node);
      }
      
      if (myfile.is_open()){
        myfile << std::get<0>(node)<<" "<<std::get<1>(node)<<" "<< std::get<2>(node)<<" ";
        myfile << "\n";
      } 
      
    }
  }
  //if we have current laaanes
  if(current_lanes_!=nullptr)
  {
    findGoalWaypointIndex();
  }
  //  iiiterate through
  //  find last wp in occ-grid_map    
  //  set as goal
//  std::cout <<"goalx = "<< std::get<0>(goal_node) <<" goaly = "<< std::get<1>(goal_node) << std::endl;
  std::get<2>(flowfield[p(std::get<0>(goal_node), std::get<1>(goal_node))]) = 1;

//  std::cout<<"Flowfield size = "<< flowfield.size() << std::endl;
//  std::cout<<"Obstacle size = "<< obstacles.size() << std::endl;
  myfile.close();
  addCurbsAsObstacles();
  createInflationLayer();
  createWaveFront();
  planner();
  createWaypoints();
  viz();
}

void PotentialField::addCurbsAsObstacles()
{
  for(auto curb: current_curbs_->markers)
  {
    for(auto point: curb.points)
    {
      auto current_x = point.x;
      auto current_y = point.y;
      geometry_msgs::PoseStamped waypoint;
      waypoint.header.frame_id = "map";
      waypoint.pose.position.x = current_x;
      waypoint.pose.position.y = current_y;
      waypoint.pose.position.z = 0;
      waypoint.pose.orientation.x = 0.0;
      waypoint.pose.orientation.y = 0.0;
      waypoint.pose.orientation.z = 0.0;
      waypoint.pose.orientation.w = 1.0;  
      geometry_msgs::Pose transformed_pose;
      //we now have transformed pose in velodyne frames for the curb point
      transformed_pose = lookupTF(waypoint);
      
      auto x = transformed_pose.position.x;
      auto y = transformed_pose.position.y;
      if(x < (nMapWidth/2)-1 && y < (nMapHeight/2)-1 && x >= (-nMapWidth/2)+1 && y >= (-nMapHeight/2)+1)
      {
        obstacles.push_back(std::make_tuple(x,y));
      }
    }
  }
}

std::tuple<int, int> PotentialField::fitToResolution(std::tuple<double,double> float_vals){
  //snap the floats to nearest res value
  snapped_x = float(int(std::get<0>(float_vals) /res + 0.5)) *res;
  snapped_y = float(int(std::get<0>(float_vals) /res + 0.5)) *res;
}

void PotentialField::createWaypoints(){

  geometry_msgs::PoseArray waypoints{};
  // Loop through path_
  for(auto node:path_){
  // Transform wps to map frame
    auto current_x = std::get<0>(node);
    auto current_y = std::get<1>(node);
//    std::cout <<"Velodyne: "<<"waypoint x = "<< current_x <<" waypoint y = "<< current_y << std::endl;
    geometry_msgs::PoseStamped waypoint;
    waypoint.header.frame_id = "velodyne";
    waypoint.pose.position.x = current_x;
    waypoint.pose.position.y = current_y;
    waypoint.pose.position.z = 0;
    waypoint.pose.orientation.x = 0.0;
    waypoint.pose.orientation.y = 0.0;
    waypoint.pose.orientation.z = 0.0;
    waypoint.pose.orientation.w = 1.0;
    
    geometry_msgs::Pose transformed_pose;
    transformed_pose = lookupTFReverse(waypoint);
    waypoints.poses.push_back(transformed_pose);
    auto x = transformed_pose.position.x;
    auto y = transformed_pose.position.y;
//    std::cout <<"Map:      "<<"waypoint x = "<< x <<" waypoint y =  "<< y << std::endl;
//    std::cout <<"------------------------------------" << std::endl; 
  }
  // Generate theta
  // Publish the pose array
  pot_field_path_pub_.publish(waypoints);
}

void PotentialField::findGoalWaypointIndex(){
  auto p = [&](int columns, int rows) {return (rows + nMapHeight/2) * nMapWidth + (columns + nMapWidth/2);};
  bool end {true};
  std_msgs::Bool flag_stanley;
  flag_stanley.data = false;
  getMotionModel();
  auto finalWaypoint = current_closest_index_->data;
//  std::cout <<"------------------------------------" << std::endl;
//  std::cout<<"current index = "<<finalWaypoint<<std::endl;
  
  while(end)
  {
    auto current_x = current_lanes_->lanes[0].waypoints[finalWaypoint].pose.pose.position.x;
    auto current_y = current_lanes_->lanes[0].waypoints[finalWaypoint].pose.pose.position.y;
    geometry_msgs::PoseStamped waypoint;
    waypoint.header.frame_id = "map";
    waypoint.pose.position.x = current_x;
    waypoint.pose.position.y = current_y;
    waypoint.pose.position.z = 0;
    waypoint.pose.orientation.x = 0.0;
    waypoint.pose.orientation.y = 0.0;
    waypoint.pose.orientation.z = 0.0;
    waypoint.pose.orientation.w = 1.0;
    
    std::cout <<"waypoint x = "<< current_x <<" waypoint y = "<< current_y << std::endl;
    
    geometry_msgs::Pose transformed_pose;
    transformed_pose = lookupTF(waypoint);
    
    auto x = transformed_pose.position.x;
    auto y = transformed_pose.position.y;
    std::cout <<"transformed x = "<< x <<" transformed y = "<< y << std::endl;
//    std::cout <<"------------------------------------" << std::endl; 
    if(x < (nMapWidth*res/2)-1 && y < (nMapHeight*res/2)-1 && x >= (-nMapWidth*res/2)+1 && y >= (-nMapHeight*res/2)+1){
      std::cout<<"valid point"<<std::endl;
      finalWaypoint++;
      //funtion to take float x, y then snap to nearest 0.5, then return int after *res
      
      std::get<0>(goal_node) = int(x);//should be nearest .5 value
      std::get<1>(goal_node) = int(y);
      std::get<2>(goal_node) = int(1);
      
      if(std::get<2>(flowfield[p(int(x),int(y))]) == -1 || 
         std::get<2>(flowfield[p(int(x),int(y))]) == -2 ||
         std::get<2>(flowfield[p(int(x),int(y))]) >= 55 ){
            flag_stanley.data = true;
         }
      
      for( auto move : motion)
      {
        if(std::get<2>(flowfield[p(int(x + move[0]),int(y + move[1]))]) == -1 || 
           std::get<2>(flowfield[p(int(x + move[0]),int(y + move[1]))]) == -2 ||
           std::get<2>(flowfield[p(int(x + move[0]),int(y + move[1]))]) >= 55)
           {
             flag_stanley.data = true;
           }
      }
      //checking 16 cells around the front axle
      for(auto move : motion2)
      {
       if(std::get<2>(flowfield[p(int(3 + move[0]),int(0 + move[1]))]) == -1 || 
          std::get<2>(flowfield[p(int(3 + move[0]),int(0 + move[1]))]) == -2 ||
          std::get<2>(flowfield[p(int(3 + move[0]),int(0 + move[1]))]) >= 55)
           {
             flag_stanley.data = true;
           }      
      }
      
    }
    else{
      end = false;
    }
  }
  pot_field_replan_pub_.publish(flag_stanley);
  
}

geometry_msgs::Pose PotentialField::lookupTF(geometry_msgs::PoseStamped initial_pt)
{
//    std::cout <<"insideTF x = "<< initial_pt.pose.position.x <<" insideTF y = "<< initial_pt.pose.position.y << std::endl;
    
    geometry_msgs::Pose transformed_pose;  
    tf::Transform map_to_velodyne;
    tf::poseMsgToTF(initial_pt.pose, map_to_velodyne);
    tf::StampedTransform req_to_map;
    try
    {
      listener_.lookupTransform("/velodyne", "/map", ros::Time(0), req_to_map);
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }
    tf::Transform req_to_velodyne;
    req_to_velodyne = req_to_map * map_to_velodyne;
    tf::poseTFToMsg(req_to_velodyne, transformed_pose);
    
    return transformed_pose;
}

//TF lookup for velodyne to map frame
geometry_msgs::Pose PotentialField::lookupTFReverse(geometry_msgs::PoseStamped initial_pt)
{
//    std::cout <<"insideTF x = "<< initial_pt.pose.position.x <<" insideTF y = "<< initial_pt.pose.position.y << std::endl;
    
    geometry_msgs::Pose transformed_pose;  
    tf::Transform velodyne_to_map;
    tf::poseMsgToTF(initial_pt.pose, velodyne_to_map);
    tf::StampedTransform req_to_velodyne;
    try
    {
      listener_.lookupTransform("/map", "/velodyne", ros::Time(0), req_to_velodyne);
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }
    tf::Transform req_to_map;
    req_to_map = req_to_velodyne * velodyne_to_map;
    tf::poseTFToMsg(req_to_map, transformed_pose);

    return transformed_pose;
}


void PotentialField::createInflationLayer(){
  //adding inflation layer to obstacles
  getMotionModel();
  inflations.clear();
  auto p = [&](int columns, int rows) {return (rows + nMapHeight/2) * nMapWidth + (columns + nMapWidth/2);};
//  auto p = [&](int columns, int rows) { return x * nMapWidth + y;};
  for (auto obstacle: obstacles){
    for (auto move: motion){
      int x = std::get<0>(obstacle)+move[0];
      int y = std::get<1>(obstacle)+move[1];
      if(y < nMapHeight/2 && x < nMapWidth && y >= -nMapHeight/2 && x >= -nMapWidth/2){
        if(std::get<2>(flowfield[p(x,y)]) != -1 &&
           std::get<2>(flowfield[p(x,y)]) != -2 )
        {  
            std::get<2>(flowfield[p(x,y)]) = -2;
//            board[x][y] = PotentialField::State::kInflation;
            inflations.push_back(std::make_tuple(x,y));
        }
      }
    }   
  }
  std::cout<<"Inflation Layer Obstacle size = "<< inflations.size() << std::endl;
}

void PotentialField::clusterCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
  current_cluster_ = std::make_shared<nav_msgs::OccupancyGrid>(*msg);
  
//  std::cout<<"current_cluster_ data point = "<< msg->data[4] <<std::endl;
//  createFlowFieldROS();
  
}

void PotentialField::occupanyCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
  current_occupancy_ = std::make_shared<nav_msgs::OccupancyGrid>(*msg);
  
  createFlowFieldROS();
}

void PotentialField::waypointCallback( const autoware_msgs::LaneArray::ConstPtr& msg)
{
  current_lanes_ = std::make_shared<autoware_msgs::LaneArray>(*msg);
}

void PotentialField::closestWaypointIndexCallback(const std_msgs::Int32::ConstPtr& msg)
{
  current_closest_index_ = std::make_shared<std_msgs::Int32>(*msg);
}

void PotentialField::curbCallback(const visualization_msgs::MarkerArray::ConstPtr& msg)
{
  current_curbs_ = std::make_shared<visualization_msgs::MarkerArray>(*msg);
  std::cout << "another one - dj Khaled" << std::endl;
 }

int main(int argc, char **argv)
{
  ros::init(argc, argv, "potential_field_node");
  PotentialField obj;
  ros::spin();
  return 0;
};

void PotentialField::createWaveFront(){
  
  auto p = [&](int columns, int rows) {return (rows + nMapHeight/2) * nMapWidth + (columns + nMapWidth/2);};
  std::vector<std::tuple<int, int, double>> nodes;

		// Add the first discovered node - the target location, with a distance of 1
	nodes.push_back(goal_node);
    while(!nodes.empty())
		{
		  std::vector<std::tuple<int, int, double>> new_nodes;
		  for(auto &n : nodes)
		  {
		    int x = std::get<0>(n);
		    int y = std::get<1>(n);
		    int d = std::get<2>(n);
		    
		    
		    // Check North
				  if ((y + 1) < nMapHeight/2 && std::get<2>(flowfield[p(x, y+1)]) == 0){
					  new_nodes.push_back(std::make_tuple( x, y+1, d + 1));
					  std::get<2>(flowfield[p(x, y+1)])= d+1;
					  repulsivePotential(flowfield[p(x, y+1)]);
          }
				  // Check South
				  if ((y - 1) >= -nMapHeight/2 && std::get<2>(flowfield[p(x, y-1)]) == 0){
					  new_nodes.push_back(std::make_tuple( x, y-1, d + 1 ));
					  std::get<2>(flowfield[p(x, y-1)])= d+1;
					  repulsivePotential(flowfield[p(x, y-1)]);
          }
				  // Check East
				  if ((x + 1) < nMapWidth/2 && std::get<2>(flowfield[p(x+1, y)]) == 0){
					  new_nodes.push_back(std::make_tuple( x+1, y, d + 1 ));
					  std::get<2>(flowfield[p(x+1, y)])= d+1;
					  repulsivePotential(flowfield[p(x+1, y)]);
          }
				  // Check West
				  if ((x - 1) >= -nMapWidth/2 && std::get<2>(flowfield[p(x-1, y)]) == 0){
					  new_nodes.push_back(std::make_tuple(x-1, y, d + 1 ));
					  std::get<2>(flowfield[p(x-1, y)])= d+1;
					  repulsivePotential(flowfield[p(x-1, y)]);
          }
          
          
          // Check North West 
          if ((x - 1) >= -nMapWidth/2 && (y + 1) < nMapHeight/2 && std::get<2>(flowfield[p(x-1, y+1)]) == 0){
					  new_nodes.push_back(std::make_tuple( x-1, y+1, d + 1));
					  std::get<2>(flowfield[p(x-1, y+1)])= d+1;
					  repulsivePotential(flowfield[p(x-1, y+1)]);
          }
				  // Check  South West
				  if ((x - 1) >= -nMapWidth/2 && (y - 1) >= -nMapHeight/2 && std::get<2>(flowfield[p(x-1, y-1)]) == 0){
					  new_nodes.push_back(std::make_tuple( x-1, y-1, d + 1 ));
					  std::get<2>(flowfield[p(x-1, y-1)])= d+1;
					  repulsivePotential(flowfield[p(x-1, y-1)]);
          }
				  // Check North East
				  if ((y + 1) < nMapHeight/2 && (x + 1) < nMapWidth/2 && std::get<2>(flowfield[p(x+1, y+1)]) == 0){
					  new_nodes.push_back(std::make_tuple( x+1, y+1, d + 1 ));
					  std::get<2>(flowfield[p(x+1, y+1)])= d+1;
					  repulsivePotential(flowfield[p(x+1, y+1)]);
          }
				  // Check South East
				  if ((x + 1) < nMapWidth/2 && (y - 1) >= -nMapHeight/2 && std::get<2>(flowfield[p(x+1, y-1)]) == 0){
					  new_nodes.push_back(std::make_tuple(x+1, y-1, d + 1 ));
					  std::get<2>(flowfield[p(x+1, y-1)])= d+1;
					  repulsivePotential(flowfield[p(x+1, y-1)]);
          }		   		  
		  }
          nodes.clear();
	        nodes.insert(nodes.begin(), new_nodes.begin(), new_nodes.end());
	        new_nodes.clear();              
		}
}


void PotentialField::getMotionModel(){

    motion = {{1,0},{0,1},{-1,0},{0,-1},{-1,-1},{-1,1},{1,-1},{1,1}};
//    motion1 = {{1,0},{1,-1},{1,1}};
    motion2 = {{1,0},{0,1},{-1,0},{0,-1},{-1,-1},{-1,1},{1,-1},{1,1},{2,0},{0,2},{-2,0},{0,-2},{-2,-2},{-2,2},{2,-2},{2,2},{2,1},{1,2},{-2,1},{1,-2}};
}

void PotentialField::vizPath(){
//    std::cout<<"In vizPath"<<std::endl;
    visualization_msgs::Marker points {};
    visualization_msgs::Marker line_strip{};
    visualization_msgs::Marker line_list {};
    points.header.frame_id = line_strip.header.frame_id = line_list.header.frame_id = "velodyne";
    points.header.stamp = line_strip.header.stamp = line_list.header.stamp = ros::Time::now();
    points.ns = line_strip.ns = line_list.ns = "path";
    points.action = line_strip.action = line_list.action = visualization_msgs::Marker::ADD;
    points.pose.orientation.w = line_strip.pose.orientation.w = line_list.pose.orientation.w = 1.0;
    points.id = 0;
    line_strip.id = 1;
    line_list.id = 2;
    points.type = visualization_msgs::Marker::POINTS;
    line_strip.type = visualization_msgs::Marker::LINE_STRIP;
    line_list.type = visualization_msgs::Marker::LINE_LIST;
    points.scale.x = 1*res;
    points.scale.y = 1*res;
    line_strip.scale.x = 0.1;
    line_list.scale.x = 0.1;
    // Points are green
    points.color.g = 1.0f;
    points.color.a = 1.0;
    // Line strip is blue
    line_strip.color.b = 1.0;
    line_strip.color.a = 1.0;



    // Create the vertices for the points and lines
    for (auto node: path_)
    {
      geometry_msgs::Point p;
      p.x = double(std::get<0>(node));
      p.y = double(std::get<1>(node));
      p.z = double(std::get<2>(node));
      points.points.push_back(p);
      line_strip.points.push_back(p);
    }
    marker_pub_.publish(points);
    marker_pub_.publish(line_strip);
}

void PotentialField::vizObstacles(){
  visualization_msgs::Marker points {};
    points.header.frame_id = "/velodyne";
    points.header.stamp = ros::Time::now();
    points.ns = "obstacles";
    points.action =  visualization_msgs::Marker::ADD;
    points.pose.orientation.w = 1.0;
    points.id = 0;
    points.type = visualization_msgs::Marker::POINTS;
    points.scale.x = 1;
    points.scale.y = 1;
    // Points are green
    points.color.g = 0.5f;
    points.color.a = 1.0; 
	  points.color.r = 1.0;
	  points.color.b = 1.0;

    // Create the vertices for the points and lines
    for (auto obstacle: obstacles)
    {
      geometry_msgs::Point p;
      p.x = double(std::get<0>(obstacle));
      p.y = double(std::get<1>(obstacle));
      p.z = 1;
      points.points.push_back(p);
    }
    obstacle_pub_.publish(points);
}

void PotentialField::vizInflation(){
  visualization_msgs::Marker points {};
    points.header.frame_id = "/velodyne";
    points.header.stamp = ros::Time::now();
    points.ns = "inflation";
    points.action =  visualization_msgs::Marker::ADD;
    points.pose.orientation.w = 1.0;
    points.id = 0;
    points.type = visualization_msgs::Marker::POINTS;
    points.scale.x = 1;
    points.scale.y = 1;
    // Points are green
    points.color.g = 0.5f;
    points.color.a = 1.0; 
	  points.color.r = 0.0;
	  points.color.b = 1.0;

    // Create the vertices for the points and lines
    for (auto inflation: inflations)
    {
      geometry_msgs::Point p;
      p.x = double(std::get<0>(inflation));
      p.y = double(std::get<1>(inflation));
      p.z = 1;
      points.points.push_back(p);
    }
    inflation_pub_.publish(points);
}

void PotentialField::vizStartAndGoal()
{
    auto p = [&](int columns, int rows) {return (rows + nMapHeight/2) * nMapWidth + (columns + nMapWidth/2);};
    visualization_msgs::Marker start;
    start.header.frame_id = "/velodyne";
    start.header.stamp = ros::Time();
    start.ns = "start_position";
    start.id = 0;
    start.type = visualization_msgs::Marker::SPHERE;
    start.action = visualization_msgs::Marker::ADD;
  //start.action = 2;
    start.pose.position.x = std::get<0>(start_node);
    start.pose.position.y = std::get<1>(start_node);
    start.pose.position.z = std::get<2>(flowfield[p(std::get<0>(start_node),std::get<1>(start_node))]);
    start.pose.orientation.x = 0.0;
    start.pose.orientation.y = 0.0;
    start.pose.orientation.z = 0.0;
    start.pose.orientation.w = 1.0;
    start.scale.x = 3;
    start.scale.y = 3;
    start.scale.z = 3;
    start.color.a = 1.0; // Don't forget to set the alpha!
    start.color.r = 0.0;
    start.color.g = 1.0;
    start.color.b = 0.0;
    vis_pub_start_.publish( start );
    
    visualization_msgs::Marker goal;
    goal.header.frame_id = "/velodyne";
    goal.header.stamp = ros::Time();
    goal.ns = "goal_position";
    goal.id = 0;
    goal.type = visualization_msgs::Marker::SPHERE;
    goal.action = visualization_msgs::Marker::ADD;
  //  closest.action = 2;
    goal.pose.position.x = std::get<0>(goal_node);
    goal.pose.position.y = std::get<1>(goal_node);
    goal.pose.position.z = std::get<2>(goal_node);
    goal.pose.orientation.x = 0.0;
    goal.pose.orientation.y = 0.0;
    goal.pose.orientation.z = 0.0;
    goal.pose.orientation.w = 1.0;
    goal.scale.x = 3;
    goal.scale.y = 3;
    goal.scale.z = 3;
    goal.color.a = 1.0; // Don't forget to set the alpha!
    goal.color.r = 1.0;
    goal.color.g = 0.0;
    goal.color.b = 0.0;
    vis_pub_goal_.publish( goal );
}

void PotentialField::vizFlowField(){
    visualization_msgs::Marker points {};
    points.header.frame_id = "/velodyne";
    points.header.stamp = ros::Time::now();
    points.ns = "FlowField";
    points.action =  visualization_msgs::Marker::ADD;
    points.pose.orientation.w = 1.0;
    points.id = 0;
    points.type = visualization_msgs::Marker::POINTS;
    points.scale.x = 1;
    points.scale.y = 1;
    // Points are green
    points.scale.x = 1;
    points.scale.y = 1;
    points.scale.z = 1;
    points.color.g = 0.5f;
    points.color.a = 1.0; 
    points.color.r = 1.0;
    points.color.b = 0.0;

    // Create the vertices for the points and lines
    for (auto cell: flowfield)
    {
      geometry_msgs::Point p;
      p.x = double(std::get<0>(cell));
      p.y = double(std::get<1>(cell));
      p.z = double(std::get<2>(cell));
      points.points.push_back(p);    
    }
    vis_pub_flowfield_.publish(points);      
}
    


void PotentialField::viz(){
//  Obstacle Viz
    vizObstacles();
    vizInflation();


//  %Start and goal points
    vizStartAndGoal();
    
//    VisualizeFlowField
    vizFlowField();
    
// Final Path
    vizPath();
}

void PotentialField::planner(){
  
  auto p = [&](int columns, int rows) {return (rows + nMapHeight/2) * nMapWidth + (columns + nMapWidth/2);};
  double distance = getDistance(start_node,goal_node);
  
  double potential{};
  int ix {std::get<0>(start_node)};
  int iy {std::get<1>(start_node)};
  int inx {};
  int iny {};
  path_.clear();
  getMotionModel(); //add heading
  int count{0};
  while(distance > 3 && count < (nMapHeight*nMapWidth-inflations.size()-obstacles.size())){
    double min_potential = std::numeric_limits<double>::infinity();
    int minix{-1};
    int miniy{-1};
    for (auto move: motion){
      inx = ix + move[0];
      iny = iy + move[1];
      if(inx < nMapWidth/2 && iny < nMapHeight/2 && inx >= -nMapWidth/2 && iny >= -nMapHeight/2){
        if(std::get<2>(flowfield[p(inx, iny)]) != -1 && std::get<2>(flowfield[p(inx, iny)]) != -2){
          potential = std::get<2>(flowfield[p(inx,iny)]);
        }
        else{
          potential = std::numeric_limits<double>::infinity();
        }
        if(potential < min_potential)
        {
          min_potential = potential;
          minix = inx;
          miniy = iny;
        } 
      }
    }
    ix = minix;
    iy = miniy;
    std::tuple<int,int,double> node {ix,iy,std::get<2>(flowfield[p(ix,iy)])};
    distance = getDistance(node,goal_node);
    path_.push_back(node);
//    std::cout<<"Distance ="<<distance<<std::endl;
//    std::cout<<"Path size ="<<path_.size()<<std::endl;
//    std::cout<<"Inside While"<<std::endl;
    count++;
  }
  std_msgs::Bool is_feasible;
  if (distance>5)
  {
    //infeasible path
    is_feasible.data = false;
  }
  else
  {
    is_feasible.data = true;
  }
  feasible_path_pub_.publish(is_feasible);

}

void PotentialField::repulsivePotential(std::tuple<int, int, double> &current_node){
  //search nearest obstacles
  double distance{};
  std::tuple<int, int> closest_obst{};
  double min_distance = std::numeric_limits<double>::infinity();
  for(auto inflation: inflations){
    distance = getDistance(current_node, inflation);
    if (distance < min_distance){
      min_distance = distance;
      closest_obst = inflation;
//      std::cout << "NEW MIN_DISTANCE "<< "distance = " << distance << std::endl;
    }
  }
  
  if (min_distance <= robot_radius){
    if(min_distance <= 0.1){
      min_distance = 0.1;
    }
//    std::get<2>(current_node) += 1;
//  std::cout << "min distance = " << min_distance<< std::endl;
  std::get<2>(current_node) += 0.5 * k_rep * pow((1.0/min_distance - 1.0 /    PotentialField::robot_radius), 2);
//  std::cout << "at end repsulive = " << std::get<2>(current_node) << std::endl;
  }
}


double PotentialField::getDistance(const std::tuple<int, int, double> &node1, 
                                   const std::tuple<int, int, double> &node2){
  double x = std::get<0>(node1) - std::get<0>(node2);
  double y = std::get<1>(node1) - std::get<1>(node2);
  return double(sqrt(pow(x, 2) + pow(y, 2)));
}

double PotentialField::getDistance(const std::tuple<int, int, double> &node1, 
                                   const std::tuple<int, int> &node2){
  double x = std::get<0>(node1) - std::get<0>(node2);
  double y = std::get<1>(node1) - std::get<1>(node2);
  return double(sqrt(pow(x, 2) + pow(y, 2)));
}

void PotentialField::attractivePotential(std::tuple<int, int, double> &current_node){
  std::get<2>(current_node) = 0.5 * k_att * pow(getDistance(current_node, goal_node),2);
}

void PotentialField::printFlowField(){
  std::ofstream myfile ("flow_viz.txt");
  auto p = [&](int x, int y) { return x * nMapWidth + y;  };
  std::cout <<"-------------------------------------------------------------------------"<< std::endl;
  for (int x = 0; x < nMapHeight; x++)
    {
      for (int y = 0; y < nMapWidth; y++)
      {
        // Set border or obstacles
        std::cout<<std::setw(5)<<std::setfill(' ')<<std::setprecision(5)<<std::get<2>(flowfield[p(x,y)])<<" ";
        if (myfile.is_open()){
          myfile << std::get<0>(flowfield[p(x,y)])<<" "<<std::get<1>(flowfield[p(x,y)])<<" "<< std::get<2>(flowfield[p(x,y)])<<" ";
          myfile << "\n";
        }
      }
      std::cout<<"\n";
    }

}
