#ifndef MAZDA_H_
#define MAZDA_H_


#include <vector>
#include <string>
#include <fstream>


#include <iostream>
#include <chrono>

#include <ros/ros.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include <autoware_msgs/NDTStat.h>
#include <tf2/utils.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Joy.h>

class Mazda {
public:
  /**
  * Constructor.
  */
  Mazda();

  /**
  * Destructor.
  */
  virtual ~Mazda();

private:
  ros::Timer timer1;
  
  ros::NodeHandle nh_;
  ros::Publisher mazda_pub_;
  ros::Subscriber joy_sub_;

  float steer_{0};
  float velocity_{0};
  
    /* for model prediction */
  std::shared_ptr <sensor_msgs::Joy> current_joy_;
  

  
    /** Maz calldabacks**/
  void joyCallback(const sensor_msgs::Joy::ConstPtr& msg);  
  void timerCallback(const ros::TimerEvent&);
};

#endif /* MAZDA_H_ */
